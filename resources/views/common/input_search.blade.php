
<div>
    <input type="text" name="q" value="{{ Request::get('q') }}" class="form-control  pull-{{ trans('crudbooster.right') }}"
	   placeholder="{{trans('crudbooster.filter_search')}}"/>
    <!--{!! CRUDBooster::getUrlParameters(['q']) !!}-->
    <div class="input-group-btn">
	@if(Request::get('q'))
	<?php
	$parameters = Request::all();

	unset($parameters['q']);
	$build_query = urldecode(http_build_query($parameters));
	$build_query = ($build_query) ? "?" . $build_query : "";
	$build_query = (Request::all()) ? $build_query : "";
	?>
	@endif
    </div>
</div>