<?php
//echo "<pre>"; print_r($validation);die();
//echo "col_width_label: " . $col_width_label;
//echo "min: " . $min;
?>

<div class='form-group {{$header_group_class}} {{ ($errors->first($name))?"has-error":"" }}' id='form-group-{{$name}}' style="{{@$form['style']}}">
    <label class='control-label {{$col_width_label?:'col-sm-2'}}'>{{$form['label']}}
        @if($required)
	<span class='text-danger' title='{!! trans('crudbooster.this_field_is_required') !!}'>*</span>
        @endif
    </label>

    <div class="{{$col_width?:'col-sm-10'}}">
        <input type='number' step="{{($form['step'])?:'1'}}" title="{{$form['label']}}"
	       
	<?php
	// vinhth add code
	if (isset($form['min'])) {
	    echo 'min=' . $form['min'];
	}
	?>
	       
	<?php
	// vinhth add code
	if (isset($form['max'])) {
	    echo 'max=' . $form['max'];
	}
	?>
               {{$required}} {{$readonly}} {!!$placeholder!!} {{$disabled}} {{$validation['min']?"min=".$validation['min']:""}} {{$validation['max']?"max=".$validation['max']:""}} class='form-control'
               name="{{$name}}" id="{{$name}}" value='{{$value}}'/>
        <div class="text-danger">{!! $errors->first($name)?"<i class='fa fa-info-circle'></i> ".$errors->first($name):"" !!}</div>
        <p class='help-block'>{{ @$form['help'] }}</p>
    </div>
</div>