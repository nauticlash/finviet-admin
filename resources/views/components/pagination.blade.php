<div class="col-md-12 no-padding">
  <span class="pull-left">{{ __("crudbooster.filter_rows_total") }} : {{ $from }} {{ __("crudbooster.filter_rows_to") }} {{ $to }} {{ __("crudbooster.filter_rows_of") }} {{ $total }}</span>
  <span class="pull-right">
    @if($pageParam > 1)
      <a class="btn" href="?page={!! $pageParam - 1 !!}"> &lt; </a>
    @endif
    @foreach ($html_show as $page)
        <!--  Use three dots when current page is greater than 4.  -->
        @if ($pageParam > 4 && $page === 2)
            <a class="btn page-item disabled"><span class="page-link">...</span></a>
        @endif

        <!--  Show active page else show the first and last two pages from current page.  -->
        @if ($page == $pageParam)
            <a class="btn btn-primary page-item active"><span class="page-link">{{ $page }}</span></a>
        @elseif ($page === $pageParam + 1 || $page === $pageParam + 2 || $page === $pageParam - 1 || $page === $pageParam - 2 || $page === count($html_show) || $page === 1)
            <a class="btn page-link" href="?page={{ $page }}">{{ $page }}</a>
        @endif

        <!--  Use three dots when current page is away from end.  -->
        @if ($pageParam < count($html_show) - 3 && $page === count($html_show) - 1)
            <a class="btn page-item disabled"><span class="page-link">...</span></a>
        @endif
    @endforeach
    @if($pageParam < count($html_show))
      <a class="btn" href="?page={!! $pageParam + 1 !!}"> &gt; </a>
    @endif
  </span>
</div>