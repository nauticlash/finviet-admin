@extends('crudbooster::admin_template')
@section('content')
<div class="container" style="width: 100% !important;">
  <ul class="nav nav-tabs">
    @foreach($listTab as $tab)
    <li class="{{$tab['tabActive'] ? 'active' : '' }}" class="active">
      <a data-toggle="tab" href="#tab-{{$tab['tabHref']}}" onclick="handleApiOnClick(`{{$tab['apiUrl']}}` , `{{$tab['tabHref']}}`)">{{$tab['tabName']}}</a>
    </li>
    @endforeach
  </ul>

  <div class="tab-content">
  <p style="padding-top: 10px;"></p>
    @foreach($listTab as $tab)
    <div id="tab-{{$tab['tabHref']}}" class="{{$tab['tabActive'] ? 'tab-pane fade in active' : 'tab-pane fade' }}">
      {!! $tab['tabActive'] ? $tab['defaultViewActive'] : $tab['tabView'] !!}
    </div>
    @endforeach
  </div>
</div>
@endsection

<script>
  function handleApiOnClick(pathName, tabId) {
    window.history.pushState('','',window.location.href.split('?')[0]);
    var APP_URL = {!! json_encode(url('/')) !!}
    $.ajax({
      url: APP_URL +  pathName,
      type: 'GET',
      dataType: 'json',
      data: {
        "_token": '{{csrf_token()}}'
      }
    }).done(function(res) {
      $("#tab-" + tabId).html(res.view);
    });
  }
</script>