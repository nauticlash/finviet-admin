<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ECO Delivery</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/d632000363.js"></script>
</head>
<style>
    .wrapper {
        width: 100vw;
        overflow: hidden;
        margin: 0 auto;
        padding: 0;
        text-align: center;
        position: relative;
    }
    .wrapper > img {
        padding: 0;
        margin: 0 auto;
        max-width: 150vw;
    }
    .buttons {
        position: absolute;
        text-align: left;
        height: 100%;
        width: 100%;
    }
    .hotline {
        position: absolute;
        display: inline-block;
        max-height: 6vh;
        top: 21%;
        left: 14%
    }
    .hotline img {
        max-height: 6vh;
        max-width: 20vw;
    }
    .logo {
        position: relative;
        top: 30%;
        left: 14%;
    }
    .logo > img {
        display: none;
        padding-bottom: 3%;
    }
    .logo a img {
        max-width: 15%;
    }

    @media only screen and (min-device-width: 931px), only screen and (orientation: landscape), only screen and (min-width: 931px) {
        .wrapper > img {
            max-width: 100%;
            max-height: 100vh;
        }

        .wrapper .hotline {
            left: 10%
        }

        .logo {
            left: 60%;
        }

        .logo > img {
            display: inline-block;
            max-width: 33%;
            max-height: 25%;
        }
    }
</style>
<body>
    <div class="wrapper">
        <div class="buttons">
            <a href="tel:0344270760" class="hotline"><img src="landing/hotline.png" alt=""></a>
            <div class="logo">
                <img src="landing/eco.png" alt="">
                <br/>
                <a href="https://apps.apple.com/us/app/eco/id1473093015?ls=1" style="padding-right: 1%;padding-left: 0.5%">
                    <img src="landing/AppStore.png" alt="">
                </a>
                <a href="https://play.google.com/store/apps/details?id=vn.com.finviet.delivery&hl=vi">
                    <img src="landing/GooglePlay.png" alt="">
                </a>
            </div>
        </div>
        <img src="landing/landing_page.png" alt="">
    </div>
<script>
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;
    if (/Android/i.test(userAgent)) {
        location.replace('https://play.google.com/store/apps/details?id=vn.com.finviet.delivery&hl=vi');
//        location.replace('http://finviet.7tech.ai/landing/ECO_Delivery.apk');
    }

    // iOS detection from: http://stackoverflow.com/a/9039885/177710
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
        location.replace('https://apps.apple.com/us/app/eco/id1473093015?ls=1');
    }
</script>
</body>
</html>