<div class="modal fade" id="send_notification" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fa fa-times"></i></span>
                </button>
                <h4 class="modal-title"><?=trans("crudbooster.send_notification")?></h4>
            </div>
            <div class="modal-body">
                <textarea placeholder="<?=trans("crudbooster.message_content")?>" class="form-control"
                          id="message" name="message" rows="5">
                </textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary"
                        data-dismiss="modal">{{trans("crudbooster.button_close")}}</button>
                <button onclick="send_message();" type="button"
                        class="btn btn-primary">{{trans("crudbooster.button_submit")}}</button>
            </div>
        </div>
    </div>
</div>
<form method='get' action='{{Request::url()}}'>
    <div class="col-md-12 col-sm-12 no-padding">
        <div class="col-md-3 col-sm-12">
            <select class="form-control" id="select_online_status" name="select_online_status">
                <option value="">{{ trans('crudbooster.filter_by_online_status') }}</option>
                @if(isset($array_online_status))
                    @foreach($array_online_status as $key => $status)
                        <option
                            value="{{$key}}" <?php echo (isset($select_online_status) && $select_online_status == $key) ? 'selected' : ''; ?> >{{$status}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="col-md-3 col-sm-12">
            <!--<select class="form-control" id="select_isfree" name="select_isfree">
                <option value="">{{ trans('crudbooster.filter_by_onorder_free') }}</option>
                @if(isset($array_is_verify))
                    @foreach($array_isfree as $key => $isfree)
                        <option
                            value="{{$key}}" <?php echo (isset($select_isfree) && $select_isfree == $key) ? 'selected' : ''; ?> >{{$isfree}}</option>
                    @endforeach
                @endif
            </select>-->
        </div>
        <div class="col-md-3 col-sm-12">
            <select class="form-control" id="select_is_verify" name="select_is_verify">
                <option value="">{{ trans('crudbooster.filter_by_authentication') }}</option>
                @if(isset($array_is_verify))
                    @foreach($array_is_verify as $key => $is_verify)
                        <option
                            value="{{$key}}" <?php echo (isset($select_is_verify) && $select_is_verify == $key) ? 'selected' : ''; ?> >{{ trans("crudbooster." . $is_verify) }}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="col-md-3 col-sm-12">
            <select class="select_is_contactno form-control" name="select_is_contactno" id="select_is_contactno">
                <option value="">{{trans("crudbooster.filter_by_number_phone")}}</option>
                @if(isset($select_is_contactno))
                    <option value="{{$select_is_contactno}}" selected>{{$select_is_contactno}}</option>
                @endif
            </select>
        </div>
        <div class="col-md-3 col-sm-12">
            <div class="input-group">
                <span class="input-group-addon"><a href="javascript:void(0)"><i class="fa fa-calendar"></i></a></span>
                <input type="hidden"/>
                <input type="text" readonly
                       <?php echo 'title= "' . trans('crudbooster.field_analysis_star_day') . '"'?> class="form-control"
                       name="select_start_date"
                       id="select_start_date" <?php echo (isset($select_start_date)) ? 'value = "' . $select_start_date . '"' : ''; ?> <?php echo 'placeholder= "' . trans('crudbooster.field_analysis_star_day') . '"'?>>
            </div>
        </div>
        <div class="col-md-3 col-sm-12">
            <div class="input-group">
                <span class="input-group-addon"><a href="javascript:void(0)"><i class="fa fa-calendar"></i></a></span>
                <input type="hidden"/>
                <input type="text" readonly class="form-control" name="select_end_date" id="select_end_date"
                <?php echo 'title= "' . trans('crudbooster.field_analysis_end_day') . '"'?>
                    <?php echo (isset($select_end_date)) ? 'value = "' . $select_end_date . '"' : ''; ?> <?php echo 'placeholder= "' . trans('crudbooster.field_analysis_end_day') . '"'?>>
            </div>
        </div>
        <div class="col-md-3 col-sm-12">
            @include("common.input_search")
        </div>
        <div class="col-md-3 col-sm-12 pull-right">
            <button type='submit'
                    class="btn btn-primary" <?php echo 'title= "' . trans('crudbooster.filter_search') . '"'?> >
                <i class="fa fa-search"></i></button>
            <?php
            if (!empty(Request::all())) {
            ?>
            <a href="<?php echo Config::get('constants.BASE_URL_ADMIN'); ?>/driver" class="btn btn-warning"
            <?php echo 'title="' . trans('crudbooster.button_reset') . '"' ?>>
                <i class="fa fa-refresh" aria-hidden="true"></i>
            </a>
            <?php
            }
            ?>
            <button type="button" class="btn btn-primary" data-toggle="modal" style="margin-left: 10px"
                    data-target="#send_notification">
                <i class="fa fa-bell"></i> {{trans("crudbooster.send_notification")}}
            </button>
        </div>
    </div>
    <div style="clear: both;"></div>
</form>
@push('bottom')
    <script>
        function send_message() {
            event.preventDefault();
            var message = $('#message').val();
            var params = {
                message: message
            };
            if (message == '') {
                <?php echo " alert (' " . trans('crudbooster.message_content') . " '); "?>
                    return false;
            }

            var r =<?php echo "confirm('" . trans('crudbooster.confirm_message') . "');" ?> ;
            if (r === true) {
                js_post_data('<?=Config::get('constants.BASE_URL_ADMIN') . '/driver/send-message-to-drivers' ?>', params);
            }
        }

        $(document).ready(function () {
            $("#select_start_date").daterangepicker({
                minDate: '01-01-2018',
                singleDatePicker: true,
                showDropdowns: true,
                autoApply: false,
                autoUpdateInput: false,
                timePicker: false,
                locale: {
                    format: 'DD-MM-YYYY'
                }
            }).on('apply.daterangepicker', function (ev, picker) {
                $('#select_start_date').val(picker.startDate.format(picker.locale.format));
            });
            $("#select_end_date").daterangepicker({
                minDate: '01-01-2018',
                singleDatePicker: true,
                showDropdowns: true,
                autoApply: false,
                autoUpdateInput: false,
                timePicker: false,
                locale: {
                    format: 'DD-MM-YYYY'
                }
            }).on('apply.daterangepicker', function (ev, picker) {
                $('#select_end_date').val(picker.startDate.format(picker.locale.format));
            });
            $('.select_is_contactno').select2({
                ajax: {
                    url: '<?php echo Config::get('constants.BASE_URL_ADMIN') . '/driver/contact-no' ?>',
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.contactno,
                                    id: item.contactno
                                }
                            })
                        };
                    },
                    cache: true
                }
            });
        });
    </script>
@endpush
