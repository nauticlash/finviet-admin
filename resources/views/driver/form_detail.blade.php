<div class="row">
  <div class=" col-md-6">
  </div>

  <div class="col-md-6">
    @if($allowSendNotification)
    <button onclick="openModal('notificationModal' , @json($row->id))" style="margin-bottom:5px;" type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#sendNotificationModal<?= $row->id ?>">
      <?= trans("crudbooster.send_notification") ?>
    </button>
    @endif
    @if($allowSendResetPassword)
    <button onclick="openModal('resetPaswordModal', @json($row->id))" style="margin-bottom:5px;" type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#sendResetPaswordModal<?= $row->id ?>">
      <?= trans("crudbooster.send_resetPassword") ?>
    </button>
    @endif
  </div>
  <div class="_generalModal modal fade" id="" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="_generalModalTitle modal-title" id=""></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="_generalModalBody modal-body" style="display: none;">
          <div class="">
            <textarea class="form-control" rows="5" id="message" name="message" placeholder="<?= trans("crudbooster.message_content") ?>
                                              "></textarea>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= trans("crudbooster.button_close") ?></button>
          <button onclick="handleAction(@json($row->id))" type="button" class="btn btn-primary"><?= trans("crudbooster.button_submit") ?></button>
        </div>
      </div>
    </div>
  </div>
  <div style="clear: both;"></div>
</div>
<div class="row">
  <div class="col-md-4">
    <div class="box box-primary">
      <div class="box-header with-border">
        <div class="box-title">
          <?= trans("crudbooster.personal_information") ?>
        </div>
      </div>
      <div class="box-body box-profile">
        <div class="row">
          <div class="col-md-12">
            <?php if (!empty($row->profilepic)) { ?>
              <img class="profile-user-img img-responsive img-circle" src="<?php echo Config::get('constants.BASE_URL') . '/images/profile/' . $row->profilepic; ?>" alt="User profile picture">
            <?php } else { ?>
              <img class="profile-user-img img-responsive img-circle" src="<?php echo Config::get('constants.BASE_URL') . '/assets/images/noimage.png'; ?>" alt="User profile picture">
            <?php } ?>
            <h3 class="profile-username text-center"><?= $row->firstname ?></h3>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <label for="exampleInputEmail1"><?= trans("crudbooster.field_phone_number") ?></label>
            <br><?= $row->contactno ?>
          </div>
          <div class="col-md-6">
            <label for="exampleInputEmail1">Email </label>
            <br><span style="word-break: break-word"><?= $row->email ?></span>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <label for="exampleInputEmail1"><?= trans("crudbooster.field_address") ?></label>
            <br><?= $row->address . ', ' . $row->state ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <label for="exampleInputEmail1"><?= trans("rider.online_status") ?></label>
            <br><?= ucfirst($row->online_status) ?>
          </div>
          <div class="col-md-6">
            <label for="exampleInputEmail1"><?= trans("rider.last_update_gps") ?></label>
            <?php if ($row->last_location_updated) : ?>
              <br><?= get_formatted_date($row->last_location_updated) ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-8">
    <div class="box box-primary">
      <div class="box-header with-border">
        <div class="box-title">
          <?= trans("crudbooster.current_location") ?>
        </div>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-md-12">
            <div id="map" style="width: 100%; height: 340px;"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header with-border">
        <div class="box-title">
          <?= trans("crudbooster.detail") ?> &emsp;
          <span style="color: green;"> {{__('crudbooster.order_success_num')}} : {{$row->total_success}}</span> &emsp;
          <span style="color: red;"> {{__('crudbooster.order_fail_num')}} : {{$row->total_fail}}</span>
        </div>
      </div>
      <div class="panel-body">
        <table id="table-order_master" class="table table-striped">
          <thead>
            <tr>
              <th><?= trans("order.order_id") ?></th>
              <th><?= trans("order.created_at") ?></th>
              <th><?= trans("crudbooster.field_pickup_address") ?></th>
              <th><?= trans("crudbooster.field_delivery_address") ?></th>
              <th><?= trans("crudbooster.field_delivery_time") ?></th>
              <th><?= trans("crudbooster.field_status") ?></th>
              <th>Time Line</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if ($order_master) {
              foreach ($order_master as $order) :
                $orderStatus = get_order_status($order);
            ?>
                <tr>
                  <td class="orderid">
                    <a href="<?= Config::get('constants.BASE_URL_ADMIN') . '/order/detail/' . $order->id ?>">
                      <span class="td-label"><?= $order->merchant_order_id ?></span>
                    </a>
                  </td>
                  <td class="created_at">
                    <span class="td-label"><?= $order->created_at ?></span>
                  </td>
                  <td class="pickup_address">
                    <span class="td-label"><?= $order->pickup_address ?></span>
                  </td>
                  <td class="delivery_address">
                    <span class="td-label"><?= $order->delivery_address ?></span>
                  </td>
                  <td class="delivery_time">
                    <span class="td-label"><?= $order->delivery_time ?></span>
                  </td>
                  <td class="status">
                    <span class="td-label"><?= trans('order.status_' . $orderStatus) ?></span>
                  </td>
                  <td style="text-align: center;">
                    <a class="btn btn-sm" style="background-color: #91b41a" href="<?= Config::get('constants.BASE_URL_ADMIN') . '/order/detail/' . $order->id ?>">
                      <i class="fa fa-history" style="color: white"></i>
                    </a>
                  </td>
                </tr>

            <?php
              endforeach;
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@include('components.pagination')

@push('bottom')

<script>
  $(function() {

    var height_li = 0;
    $(".personal-info li").each(function() {
      if (($(this).height()) > height_li) {
        height_li = $(this).height();
      }
    });
    $(".personal-info li").each(function() {
      $(this).css("height", height_li);
    });
    var height_div = 0;
    $(".personal-info .div-value").each(function() {
      if (($(this).height()) > height_div) {
        height_div = $(this).height();
      }
    });
    $(".personal-info .div-value").each(function() {
      $(this).css("height", height_div);
    });
    // $("#table-order_master").DataTable({
    //     "paging": true,
    //     "lengthChange": false,
    //     "searching": false,
    //     "ordering": true,
    //     "info": true,
    //     "autoWidth": false,
    //     "order": [[0, "desc"]]
    // });
  });
  var actionName = '';
  function openModal(modalType, id) {
    switch (modalType) {
      case 'notificationModal':
        $("._generalModal").attr('id', 'sendNotificationModal' + id).attr('aria-labelledby','sendNotificationLabel');
        $("._generalModalTitle").attr('id','sendNotificationLabel').html('<?= trans("crudbooster.send_notification") ?>');
        $('._generalModalBody').show();
        actionName = 'send_message';
        break;
      case 'resetPaswordModal':
        $("._generalModal").attr('id', 'sendResetPaswordModal' + id).attr('aria-labelledby','sendResetPasswordLabel');
        $("._generalModalTitle").attr('id','sendResetPasswordLabel').html('<?= trans("crudbooster.send_resetPassword") ?>');
        $('._generalModalBody').hide();
        actionName = 'reset_password';
        break;
    }
  }

  function handleAction(id){
    switch (actionName) {
      case 'send_message':
        send_message(id)
        break;
        case 'reset_password':
        reset_password(id)
        break;
        default:
        alert("Invalid action");
        break;
    }
  }

  function send_message(riderid) {
    event.preventDefault();
    var message = $('#message').val();
    var params = {
      riderid: riderid,
      message: message
    };
    if (message == '') {
      <?php echo " alert (' " . trans('crudbooster.message_content') . " '); " ?>
      return false;
    }

    var r = <?php echo "confirm('" . trans('crudbooster.confirm_message') . "');" ?>;
    if (r === true) {
      js_post_data('<?= Config::get('constants.BASE_URL_ADMIN') . '/driver/send-message-to-driver' ?>', params);
    }
  }

  function reset_password(riderid){
    event.preventDefault();
    var params = {
      riderid: riderid
    };
    var r = <?php echo "confirm('" . trans('crudbooster.confirm_send_password') . "');" ?>;
    if (r === true) {
      js_post_data('<?= Config::get('constants.BASE_URL_ADMIN') . '/driver/send-password-to-driver' ?>', params);
    }
  }

  let map;
  let position;

  function initMap() {
    position = new google.maps.LatLng(<?= $row->lat ?>, <?= $row->lng ?>);
    map = new google.maps.Map(document.getElementById('map'), {
      center: position,
      zoom: 15
    });
    new google.maps.Marker({
      position: position,
      map: map,
    });
  }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=<?= env('GOOGLE_API_KEY') ?>&callback=initMap" async defer></script>
@endpush