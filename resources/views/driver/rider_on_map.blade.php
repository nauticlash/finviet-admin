@extends('crudbooster::admin_template')

@section('content')
    <div class="row">
        <form method="get" action="" id="filter_form">
            <div class="col-md-3">
                <label for=""><?=trans('crudbooster.total_rider')?>: </label> <?=count($riders)?>
            </div>
            <div class="col-md-3 pull-right">
                <label for="#time_range"><?=trans('rider.last_update_gps')?></label>
                <select name="time_range" id="time_range" class="select2 form-control">
                    <option value="-30 minutes" <?=$timeRange == '-30 minutes' ? 'selected':''?>><?=trans('crudbooster.thirty_minutes_ago')?></option>
                    <option value="-1 hours" <?=$timeRange == '-1 hours' ? 'selected':''?>><?=trans('crudbooster.one_hour_ago')?></option>
                    <option value="-1 days" <?=$timeRange == '-1 days' ? 'selected':''?>><?=trans('crudbooster.one_day_ago')?></option>
                    <option value="-7 days" <?=$timeRange == '-7 days' ? 'selected':''?>><?=trans('crudbooster.seven_days_ago')?></option>
                </select>
            </div>
        </form>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <!-- form start -->
                <div id="map" style="width: 100%; height: 700px;"></div>
            </div>
        </div>
    </div>
@endsection

@push('bottom')
    <script>
        $(document).ready(function () {
            $('#time_range').select2().on('select2:select', function (e) {
                $('#filter_form').submit();
            });
        });
        let map;
        let infoWindow;

        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: -34.397, lng: 150.644},
                zoom: 8
            });
            let bounds = new google.maps.LatLngBounds();

            <?php
                if (!empty($riders)):
                foreach ($riders as $rider):
                ?>
            let marker_<?=$rider->id?> = new google.maps.Marker({
                position: new google.maps.LatLng(<?=$rider->lat?>, <?=$rider->lng?>),
                map: map,
            });
            marker_<?=$rider->id?>.addListener('click', function () {
                if (infoWindow) {
                    infoWindow.close();
                }
                infoWindow = new google.maps.InfoWindow({
                    content: '<div class="box-body box-profile">' +
                        '<a href="<?=Config::get('constants.BASE_URL_ADMIN') . '/driver/detail/' . $rider->id?>">' +
                        '<img src="<?= Config::get('constants.BASE_URL') . !empty($rider->profilepic) ? '/images/profile/' . $rider->profilepic : '/assets/images/noimage.png'; ?>" class="profile-user-img img-responsive img-circle">' +
                        '<h3 class="profile-username text-center"><?=$rider->firstname?> - <?=$rider->contactno?></h3></a></div>'
                });
                infoWindow.open(map, marker_<?=$rider->id?>);
            });
            bounds.extend(marker_<?=$rider->id?>.position);
            <?php
            endforeach;
            endif;
            ?>
            map.fitBounds(bounds);
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?=env('GOOGLE_API_KEY')?>&callback=initMap" async
            defer></script>
@endpush
