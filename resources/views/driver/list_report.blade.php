<div class="col-md-12 col-sm-12 no-padding">
  <div class="col-md-3 col-sm-6">
    <div class="input-group">
      <span class="input-group-addon"><a href="javascript:void(0)"><i class="fa fa-calendar"></i></a></span>
      <input type="hidden" />
      <input type="text" readonly class="form-control" name="select_start_date" id="select_start_date" title="{{__('crudbooster.field_analysis_star_day')}}" 
      placeholder="{{__('crudbooster.field_analysis_star_day')}}" value="{{$_GET['select_start_date'] ?? ''}}">
    </div>
  </div>
  <div class="col-md-3 col-sm-6">
    <div class="input-group">
      <span class="input-group-addon"><a href="javascript:void(0)"><i class="fa fa-calendar"></i></a></span>
      <input type="hidden" />
      <input type="text" readonly class="form-control" name="select_end_date" id="select_end_date" title="{{__('crudbooster.field_analysis_end_day')}}" 
      placeholder="{{__('crudbooster.field_analysis_end_day')}}" value="{{$_GET['select_end_date'] ?? ''}}">
    </div>
  </div>
  <div class="col-md-6 col-sm-12">
    <div class="input-group">
      <input type="text" name="q" value="{{$_GET['filter'] ?? ''}}" class="form-control" placeholder="Tìm theo số điện thoại hoặc tên tài xế">
      <a id="search-href" class="btn btn-primary input-group-addon" onclick="searchDriver()"><i class="fa fa-search"></i> Search </a>
      <a id="search-href" class="btn btn-primary input-group-addon" onclick="exportExcel()"><i class="fa fa-file-excel-o"></i> Export Excel </a>
    </div>
  </div>

</div>
<script>
  $().ready(function() {
    var startEqualEnd = new Date();
    startEqualEnd.setHours(23, 59, 59, 999);
    $("#select_start_date").daterangepicker({
      minDate: ' 01-01-2018',
      maxDate: startEqualEnd,
      singleDatePicker: true,
      showDropdowns: true,
      autoApply: false,
      autoUpdateInput: false,
      timePicker: false,
      locale: {
        format: 'DD-MM-YYYY'
      }
    }).on('apply.daterangepicker', function(ev, picker) {
      $('#select_start_date').val(picker.startDate.format(picker.locale.format));
    });
    $("#select_end_date").daterangepicker({
      minDate: '01-01-2018',
      maxDate: startEqualEnd,
      singleDatePicker: true,
      showDropdowns: true,
      autoApply: false,
      autoUpdateInput: false,
      timePicker: false,
      locale: {
        format: 'DD-MM-YYYY'
      }
    }).on('apply.daterangepicker', function(ev, picker) {
      $('#select_end_date').val(picker.startDate.format(picker.locale.format));
    });
  });

  function searchDriver() {
    let searchText = $('input[name="q" ]').val();
    let selectStart = $('#select_start_date').val();
    let selectEnd = $('#select_end_date').val();
    window.location.href = window.location.href.split('?')[0] + '?filter=' + searchText + '&select_start_date=' + selectStart + '&select_end_date=' + selectEnd;
  }

  function exportExcel() {
    let searchText = $('input[name="q" ]').val();
    let selectStart = $('#select_start_date').val();
    let selectEnd = $('#select_end_date').val();
    window.location.href = window.location.href.split('?')[0] + '?filter=' + searchText + '&select_start_date=' + selectStart + '&select_end_date=' + selectEnd + '&exportExcel=true';
  }
</script> <br>
<div class="panel-body">
  <table id="table-order_master" class="table table-striped">
    <thead>
      <tr>
        @foreach($driver_header as $head)
        <th>{{$head}}</th>
        @endforeach
      </tr>
    </thead>
    <tbody>
      @if($driver_infos)
      @foreach($driver_infos as $driver)
      <tr>
        @foreach($driver as $key => $val)
        @if($key !== 'id')
        @if($key === 'driver_name')
        <td id="{{$key}}">
          <a id="td-{{$key}}" href="{{$driver->id ? '/admin/driver/detail/'.$driver->id : '/admin/driver'}}">{{$val}}</a>
        </td>
        @else
        <td id="{{$key}}">
          <span id="td-{{$key}}">{{ in_array($key ,$field_format) ? number_format($val , 0) : $val}}</span>
        </td>
        @endif
        @endif
        @endforeach
      </tr>
      @endforeach
      @endif
    </tbody>
  </table>
</div>
<br>

@include('components.pagination')