<div class="box box-primary" style="padding: 0px 15px 0px 15px">
    <div class="box-header with-border">

    </div>
    <div class="box-body box-profile">
	<div class="row">
	    <div class="col-md-12">
		<div class="box-body">
		    <h3><?php echo trans("crudbooster.field_order_arlet") ?></h3>
		    <div class="row">
			<div class="col-sm-6">
			    <div class="form-group ">
				<label for="order_pending_time"><?php echo trans("crudbooster.field_order_arlet_minute") ?></label>
				<input type="text" class="form-control" id="order_pending_time" name="order_pending_time" value="<?php echo ($data->order_pending_time) ? $data->order_pending_time : '' ?>" >
			    </div>
			</div>
		    </div>
		    <h3><?php echo trans("crudbooster.field_no_driver_yet") ?></h3>
		    <div class="row">
			<div class="col-sm-6">
			    <div class="form-group ">
				<label for="rider_pickup_velocity"><?php echo trans("crudbooster.field_speed_average_pickup") ?></label>
				<input type="text" class="form-control" id="rider_pickup_velocity" name="rider_pickup_velocity" value="<?php echo ($data->rider_pickup_velocity) ? $data->rider_pickup_velocity : '' ?>" >
			    </div>
			</div>
		    </div>
		    <h3><?php echo trans("crudbooster.field_orders_not_delivered_driver") ?></h3>
		    <div class="row">
			<div class="col-sm-6">
			    <div class="form-group ">
				<label for="rider_dropoff_velocity"><?php echo trans("crudbooster.field_speed_average_delivery") ?></label>
				<input type="text" class="form-control" id="rider_dropoff_velocity" name="rider_dropoff_velocity" value="<?php echo ($data->rider_dropoff_velocity) ? $data->rider_dropoff_velocity : '' ?>" >
			    </div>
			</div>
		    </div>
		    <div class="row">
			<div class="col-sm-6">
			    <div class="form-group ">
				<label for="alert_email_list"><?php echo trans("crudbooster.field_email_arlet") ?></label>
				<textarea rows="5" class="form-control" id="alert_email_list" name="alert_email_list"><?php echo ($data->alert_email_list) ? $data->alert_email_list : '' ?></textarea>
			    </div>
			</div>
		    </div>

		</div>

	    </div>
	</div>
    </div>
</div>