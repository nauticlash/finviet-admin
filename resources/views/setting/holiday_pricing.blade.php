<div class="box box-primary" style="padding: 0px 15px 0px 15px;border-top: inherit !important;">
  <div class="box-body box-profile">
    <div class="row">
      <div class="col-md-12">
        <div class="box-body">
          <h3><?php echo trans("crudbooster.field_min_km_re_alert") ?></h3>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group ">
                <label for="min_km_re"><?php echo trans("crudbooster.field_min_km_re_input") ?></label>
                <input type="number" class="form-control" id="min_km_re" onkeypress="return event.charCode >= 48" min="0" name="min_km_re" value="<?php echo ($data->min_km_re) ? $data->min_km_re : '' ?>">
              </div>
            </div>
          </div>
          <h3><?php echo trans("crudbooster.field_min_km_price_re_alert") ?></h3>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group ">
                <label for="min_km_price_re"><?php echo trans("crudbooster.field_min_km_price_re_input") ?></label>
                <input type="number" class="form-control" id="min_km_price_re" onkeypress="return event.charCode >= 48" min="0" name="min_km_price_re" value="<?php echo ($data->min_km_price_re) ? $data->min_km_price_re : '' ?>">
              </div>
            </div>
          </div>
          <h3><?php echo trans("crudbooster.field_min_up_price_re_alert") ?></h3>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group ">
                <label for="min_up_price_re"><?php echo trans("crudbooster.field_min_up_price_re_input") ?></label>
                <input type="number" class="form-control" id="min_up_price_re" onkeypress="return event.charCode >= 48" min="0" name="min_up_price_re" value="<?php echo ($data->min_up_price_re) ? $data->min_up_price_re : '' ?>">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="box-footer">
    <a class="btn btn-success pull-right" onclick="saveHolidayPrice()"> {{__("crudbooster.save_btn")}} <i class="fa fa-save"></i></a>
  </div>
</div>

<script>
  function saveHolidayPrice() {
    let min_km_re = $('#min_km_re').val()
    let min_km_price_re = $('#min_km_price_re').val()
    let min_up_price_re = $('#min_up_price_re').val()
    if (min_km_re < 0)
      return swal({
        title: 'Thông báo',
        text: "{{__('crudbooster.warn_min_km')}}",
        type: "error",
        showCancelButton: false,
        closeOnConfirm: true,
      });
    if (min_km_price_re < 0)
      return swal({
        title: 'Thông báo',
        text: "{{__('crudbooster.warn_min_km_price_re')}}",
        type: "error",
        showCancelButton: false,
        closeOnConfirm: true,
      });
    if (min_up_price_re < 0)
      return swal({
        title: 'Thông báo',
        text: "{{__('crudbooster.warn_min_up_price_re')}}",
        type: "error",
        showCancelButton: false,
        closeOnConfirm: true,
      });

    $.ajax({
      method: "POST",
      url: "<?php echo Config::get('constants.BASE_URL_ADMIN') . '/setting/holiday-pricing' ?>",
      data: {
        min_km_re,
        min_km_price_re,
        min_up_price_re
      },
      dataType: "json"
    }).done(function(res) {
      if (res.status == 200) {
        swal({
          title: '{{__("crudbooster.alert_success")}}',
          text: res.msg,
          type: "success",
          showCancelButton: false,
          closeOnConfirm: true,
        });
      } else {
        swal({
          title: '{{__("crudbooster.alert_danger")}}',
          text: res.msg,
          type: "danger",
          showCancelButton: false,
          closeOnConfirm: true,
        });
      }
    });
  }
</script>