@extends('crudbooster::admin_template')
@section('content')
    <?php
    //echo "<pre>"; print_r(CRUDBooster::getCurrentModule());
    //die();
    ?>
    <div>

        <div>


            <div class="panel-body">
                <?php
                $action = (@$row) ? CRUDBooster::mainpath("edit-save/$row->id") : CRUDBooster::mainpath("add-save");
                $return_url = ($return_url) ?: g('return_url');
                ?>
                <form class='form-horizontal' method='post' id="my-form"
                      enctype="multipart/form-data" action='{{$action}}'>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type='hidden' name='return_url' value='{{ @$return_url }}'/>
                    <input type='hidden' name='ref_mainpath' value='{{ CRUDBooster::mainpath() }}'/>
                    <input type='hidden' name='ref_parameter' value='{{urldecode(http_build_query(@$_GET))}}'/>
                    <input type='hidden' name='updated_at' id='updated_at' value='{{ $updated_at }}'/>
                    @if($hide_form)
                        <input type="hidden" name="hide_form" value='{!! serialize($hide_form) !!}'>
                    @endif

                    <div class="box-body" id="parent-form-area">
                        @if($command == 'detail')
                            @if(isset($view_detail) && !empty($view_detail))
                                @include($view_detail)
                            @else
                                @include("default.form_detail")
                            @endif
                        @elseif($command == 'add')
                            @if(isset($view_add) && !empty($view_add))
                                @include($view_add)
                            @else
                                @include("default.form_add")
                            @endif
                        @else
                            @if(isset($view_edit) && !empty($view_edit))
                                @include($view_edit)
                            @else
                                @include("default.form_edit")
                            @endif
                        @endif

                    </div><!-- /.box-body -->
                    <div class="box-footer" style="background: #F5F5F5">

                        <div class="form-group">
                            <label class="control-label col-sm-2"></label>
                            <div class="col-sm-10">
                              @if($button_cancel && CRUDBooster::getCurrentMethod() != 'getDetail')
                                @if(g('return_url'))
                                  <a href='{{g("return_url")}}' class='btn btn-default'><i
                                    class='fa fa-chevron-circle-left'></i> {{trans("crudbooster.button_back")}}</a>
                                @else
                                  <a href='{{CRUDBooster::mainpath("?".http_build_query(@$_GET)) }}' class='btn btn-default'><i
                                    class='fa fa-chevron-circle-left'></i> {{trans("crudbooster.button_back")}}</a>
                                @endif
                              @endif
                              @if(CRUDBooster::isCreate() || CRUDBooster::isUpdate())
                                @if(CRUDBooster::isCreate() && $button_addmore==TRUE && $command == 'add')
                                  <input type="button" onclick="confirmSubmit()" name="button"  value='{{trans("crudbooster.button_save_more")}}' class='btn btn-success'>
                                @endif

                                @if($button_save && $command != 'detail')
                                  <input type="button" onclick="confirmSubmit()" name="button"  value='{{trans("crudbooster.button_save")}}' class='btn btn-success'>
                                @endif
                              @endif
                            </div>
                        </div>


                    </div><!-- /.box-footer-->
                   
                </form>

            </div>
        </div>
    </div><!--END AUTO MARGIN-->
    <script>
      function confirmSubmit() {
        event.preventDefault();
        swal({
            title: "Do you want to continue.",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Ok",
            cancelButtonText: "Cancel",
            closeOnConfirm: true,
            closeOnCancel: true,
            allowOutsideClick: true
          },
          function(isConfirm) {
            if (isConfirm) {
              $("#my-form").submit();
            } else {

            }
          });
      }
    </script>
@endsection
