<?php
//Loading Assets
$asset_already = [];
foreach ($forms as $form) {
    $type = @$form['type'] ?: 'text';

    if (in_array($type, $asset_already)) {
	continue;
    }
    ?>

    @if(file_exists(resource_path('views/vendor/crudbooster/type_components/'.$type.'/asset.blade.php')))
    @include('vendor.crudbooster.type_components.'.$type.'.asset')
    @elseif(file_exists(base_path('/vendor/crocodicstudio/crudbooster/src/views/default/type_components/'.$type.'/asset.blade.php')))
    @include('crudbooster::default.type_components.'.$type.'.asset')
    @endif

    <?php
    $asset_already[] = $type;
} //end forms
?>

@push('head')
<style type="text/css">
    #table-detail tr td:first-child {
	/*font-weight: bold;*/
	width: 25%;
    }

    .class-hr{
	margin-top: 5px;
	margin-bottom: 5px;
    }

    @media screen and (max-width: 320px){
	.about-me .personal-info li {
	    width: 100%;
	}
    }

    @media only screen and (min-width: 1200px) {
	.about-me .personal-info li {
	    width: 33%;
	}
    }


    .about-me {
    }
    .about-me p {
	line-height: 24px;
	letter-spacing: 0px;
    }
    .about-me .personal-info {
	display: inline-block;
	width: 100%;
	border: 1px solid #ddd;
	padding: 30px;
	list-style-type: none;
    }

    .about-me .personal-info li span.left {
	width: 40%;
	float: left;
	text-transform: uppercase;
	font-weight: bold;
    }
    .about-me .personal-info li span.right {

    }
    .about-me .personal-info li p {
	margin: 0px;
	line-height: 30px;
    }
    .about-me .personal-info li a {
	color: #346abb;
    }

    .grid-3-div {

    }
</style>
<script>
    $(function () {

	var height_li = 0;
	$(".personal-info li").each(function () {
	    if (($(this).height()) > height_li) {
		height_li = $(this).height();
	    }
	});
	$(".personal-info li").each(function () {
	    $(this).css("height", height_li);
	});

	var height_div = 0;
	$(".personal-info .div-value").each(function () {
	    if (($(this).height()) > height_div) {
		height_div = $(this).height();
	    }
	});
	$(".personal-info .div-value").each(function () {
	    $(this).css("height", height_div);
	});


    });
</script>
@endpush
<div class='table-responsive'>

    <section class="about-me padding-top-10">
	<ul class="personal-info col-md-12">
	<!--<table id='table-detail' class='table table-striped'>-->

	    <?php
	    $number = 1;
	    foreach ($forms as $index => $form):

		$name = $form['name'];
		@$join = $form['join'];
		@$value = (isset($form['value'])) ? $form['value'] : '';
		@$value = (isset($row->{$name})) ? $row->{$name} : $value;
		@$showInDetail = (isset($form['showInDetail'])) ? $form['showInDetail'] : true;

		if ($showInDetail == false) {
		    continue;
		}

		if (isset($form['callback_php'])) {
		    @eval("\$value = " . $form['callback_php'] . ";");
		}

		if (isset($form['callback'])) {
		    $value = call_user_func($form['callback'], $row);
		}

		if (isset($form['default_value'])) {
		    @$value = $form['default_value'];
		}

		if ($join && @$row) {
		    $join_arr = explode(',', $join);
		    array_walk($join_arr, 'trim');
		    $join_table = $join_arr[0];
		    $join_title = $join_arr[1];
		    $join_table_pk = CB::pk($join_table);
		    $join_fk = CB::getForeignKey($table, $join_table);
		    $join_query_{$join_table} = DB::table($join_table)->select($join_title)->where($join_table_pk, $row->{$join_fk})->first();
		    $value = @$join_query_{$join_table}->{$join_title};
		}

		$type = @$form['type'] ?: 'text';
		$required = (@$form['required']) ? "required" : "";
		$readonly = (@$form['readonly']) ? "readonly" : "";
		$disabled = (@$form['disabled']) ? "disabled" : "";
		$jquery = @$form['jquery'];
		$placeholder = (@$form['placeholder']) ? "placeholder='" . $form['placeholder'] . "'" : "";

		$user_location = resource_path('views/vendor/crudbooster/type_components/' . $type . '/component_detail.blade.php');
		$file_location = base_path('vendor/crocodicstudio/crudbooster/src/views/default/type_components/' . $type . '/component_detail.blade.php');
		?>

    	    @if(file_exists($user_location))
		<?php $containTR = (substr(trim(file_get_contents($user_location)), 0, 4) == '<tr>') ? true : false; ?>

    	    @if($containTR)
    	    @if($type != 'child' && $type != 'child_logs')
    	    @include('vendor.crudbooster.type_components.'.$type.'.component_detail')
    	    @endif
    	    @else
    	    @if($type != 'child' && $type != 'child_logs')



    	    <li class="col-md-12">
    		<label>{{$form['label']}}</label>
    		<div class="div-value">@include('vendor.crudbooster.type_components.'.$type.'.component_detail')</div>
    		<hr class="class-hr">
    	    </li>



		<?php ?>

    	    @endif
    	    @endif
    	    @elseif(file_exists($file_location))
		<?php $containTR = (substr(trim(file_get_contents($file_location)), 0, 4) == '<tr>') ? true : false; ?>
    	    @if($containTR)
    	    @if($type != 'child' && $type != 'child_logs')
    	    @include('crudbooster::default.type_components.'.$type.'.component_detail')
    	    @endif
    	    @else
    	    @if($type != 'child' && $type != 'child_logs')
    	    <tr>
    		<td>{{$form['label']}}</td>
    		<td>@include('crudbooster::default.type_components.'.$type.'.component_detail')</td>
    	    </tr>
    	    @endif
    	    @endif
    	    @else
    	    <!-- <tr><td colspan='2'>NO COMPONENT {{$type}}</td></tr> -->
    	    @endif

		<?php $number++;
	    endforeach;
	    ?>

	    <!--</table>-->
	</ul>
	<div class="clearfix"></div>
    </section>

    <?php
    foreach ($forms as $index => $form) {
	$type = @$form['type'] ?: 'text';
	?>
        @if($type == 'child' || $type == 'child_logs')
        @include('vendor.crudbooster.type_components.'.$type.'.component_detail')
        @endif
	<?php
    }
    ?>

</div>

