@extends('crudbooster::admin_template')

@section('content')

    <?php
    $list = array();
    $start_time = strtotime($where_start_date);
    $end_time = strtotime($where_end_date);
    $j = 1;
    for ($i = $start_time; $i <= $end_time; $i += 86400) {
        $list[$j] = date('d-m-Y', $i);
        $j++;
    }
    $query = "SELECT DATE_FORMAT(o.orderdate, '%d-%m-%Y') as grouporderdate, DATE_FORMAT(o.orderdate, '%d-%m-%Y') as orderdate, SUM(CASE WHEN o.`status` = 'Pending' THEN 1  ELSE 0 END) as pendingorder, SUM(CASE WHEN o.`status` = 'Complete' THEN 1  ELSE 0 END) as completeorder, SUM(CASE WHEN o.`status` = 'In Progress' THEN 1  ELSE 0 END) as ongoingorder, SUM(CASE WHEN o.`status` = 'Canceled' THEN 1  ELSE 0 END) as cancelorder FROM order_master as o WHERE o.is_deleted = 0 GROUP BY grouporderdate";
    $orderarray = DB::select($query);
    $montharray = $list;
    ?>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <div class="box">
        <div class="box-header">
            <form method='get' action=''>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="col-md-3 col-sm-12">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <a href="javascript:void(0)">
                                        <i class="fa fa-calendar"></i>
                                    </a>
                                </span>
                                <input type="hidden"/>
                                <input type="text" readonly name="select_start_date" id="select_start_date"
                                       class="form-control" value="<?=$select_start_date?>"
                                       placeholder="<?=trans('report.start_day')?>">
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-12">
                            <div class="input-group">
                                <span tabindex="1" class="input-group-addon"><a href="javascript:void(0)"><i
                                                class="fa fa-calendar"></i></a></span>
                                <input type="hidden" autofocus="true"/>
                                <input type="text" readonly
                                       <?php echo 'title= "' . trans('report.end_day') . '"'?> class="form-control"
                                       name="select_end_date" id="select_end_date"
                                       <?php echo (isset($select_end_date)) ? 'value = "' . $select_end_date . '"' : ''; ?> placeholder="Xem đến ngày">
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-12">
                            <div class="input-group">
                                <button type='submit' class="btn btn-default" title="Tìm kiếm"><i
                                            class="fa fa-search"></i></button>
                            </div>

                        </div>

                    </div>
                </div>
            </form>
        </div>
        <div class="box-body table-responsive">
            <div class="row">
                <div class="col-md-12">
                    <div id="bar_chart"></div>
                </div>
            </div>
        </div>

    </div>

@endsection
@push('bottom')

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script>
    $(document).ready(function () {
        $("#select_start_date").daterangepicker({
            minDate: '01-01-2018',
            singleDatePicker: true,
            showDropdowns: true,
            timePicker: false,
            timePicker12Hour: false,
            timePickerIncrement: 5,
            timePickerSeconds: false,
            autoApply: true,
            locale: {
                format: 'DD-MM-YYYY'
            }
        });
        $("#select_end_date").daterangepicker({
            minDate: '01-01-2018',
            singleDatePicker: true,
            showDropdowns: true,
            timePicker: false,
            timePicker12Hour: false,
            timePickerIncrement: 5,
            timePickerSeconds: false,
            autoApply: true,
            locale: {
                format: 'DD-MM-YYYY'
            }
        });

        google.charts.load('current', {'packages': ['bar']});
        google.charts.setOnLoadCallback(drawChart);
    });


    function drawChart() {

        let data = new google.visualization.DataTable();
        data.addColumn('string',<?php echo "'" . trans('report.column_time') . "'";?>);
        data.addColumn('number',<?php echo "'" . trans('report.order_status_new') . "'";?> );
        data.addColumn('number',<?php echo "'" . trans('report.order_status_delivering') . "'";?>);
        data.addColumn('number',<?php echo "'" . trans('report.order_status_delivered') . "'";?>);
        data.addColumn('number',<?php echo "'" . trans('report.order_status_canceled') . "'";?>);
        <?php
        if (!empty($montharray)) {
            $flag = 0;
            foreach ($montharray as $key => $value) {
                foreach ($orderarray as $row) {
                    if ($value == $row->orderdate) {
                        $flag = 1;
        ?>
        data.addRow([<?php echo "'" . $value . "'"; ?>, <?php echo $row->pendingorder; ?>, <?php echo $row->ongoingorder; ?>,<?php echo $row->completeorder; ?>,<?php echo $row->cancelorder; ?>]);
        <?php
                        break;
                    } else {
                        $flag = 0;
                    }
                }
                if ($flag == 0) {
        ?>
        data.addRow([<?php echo "'" . $value . "'"; ?>, 0, 0, 0, 0]);
            <?php
            }
            }
            }
            ?>
        let options = {
                chart: {
                    title: '<?=trans('report.orders_report_by_status')?>'
                },
//	        width: 960,
                height: 600,
                axes: {
                    x: {
                        0: {side: 'bottom'}
                    }
                }
            };
        let chart = new google.charts.Bar(document.getElementById('bar_chart'));
        chart.draw(data, options);
    }
</script>

@endpush