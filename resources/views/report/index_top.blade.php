
<form method='get' style="display:inline;" action='{{Request::url()}}'>
    <div class="col-md-12 col-sm-12">

	<div class="col-md-3 col-sm-12">
	    <select class="form-control" id="select_merchants_id" name="select_merchants_id">
		<option value=""><?php echo trans('crudbooster.field_select_the_partner_branch');?></option>
		@if(isset($merchants))
		    @foreach($merchants as $merchant)
			<option value="{{$merchant->user_id}}" <?php echo ( isset($select_merchants_id) && $select_merchants_id == $merchant->user_id) ? 'selected' : ''; ?> >{{$merchant->name}}</option>
		    @endforeach
		@endif
	    </select>
	</div>

<!--	<div class="col-md-3 col-sm-12">
	    <select class="form-control" id="select_userid" name="select_userid">
		<option value="">Chọn từ ngày</option>
		@if(isset($order_master_user))
		    @foreach($order_master_user as $user)
			<option value="{{$user->id}}" <?php echo ( isset($select_userid) && $select_userid == $user->id) ? 'selected' : ''; ?> >{{$user->firstname}} {{$user->lastname}}</option>
		    @endforeach
		@endif
	    </select>
	</div>

	<div class="col-md-3 col-sm-12">
	    <select class="form-control" id="select_status" name="select_status">
		<option value="">Chọn đến ngày</option>
		@if(isset($array_status))
		    @foreach($array_status as $key => $status)
			<option value="{{$key}}" <?php echo ( isset($select_status) && $select_status == $key) ? 'selected' : ''; ?> >{{$status}}</option>
		    @endforeach
		@endif
	    </select>
	</div>-->

<!--	<div class="col-md-3 col-sm-12">
	    <div class="input-group">
		<div class="input-group-addon">
		    <i class="fa fa-calendar"></i>
		</div>
		<input id="date_range" type="text" class="form-control pull-right" placeholder="Chọn khoảng ngày " value="<?php echo $date_range ?>">
		<input type="hidden" name="date_range" value="<?php echo $date_range ?>">
	    </div> /.input group 
	</div>-->

	<div class="col-md-3 col-sm-12">
	    <div class="input-group">  			
		<span tabindex="1" class="input-group-addon"><a href="javascript:void(0)" ><i class="fa fa-calendar"></i></a></span>
		<input type="hidden" autofocus="true" />
		<input type="text" readonly <?php echo 'title= "'.trans('crudbooster.field_analysis_star_day').'"'?> class="form-control" name="select_start_date" id="select_start_date" <?php echo (isset($select_start_date)) ? 'value = "' . $select_start_date . '"' : ''; ?> <?php 'placeholder="'.trans("crudbooster.field_analysis_star_day").'"' ?>>					
	    </div>
	</div>

	<div class="col-md-3 col-sm-12">
	    <div class="input-group">  			
		<span tabindex="1" class="input-group-addon"><a href="javascript:void(0)" ><i class="fa fa-calendar"></i></a></span>
		<input type="hidden" autofocus="true" />
		<input type="text" readonly <?php echo 'title= "'.trans('crudbooster.field_analysis_end_day').'"'?> class="form-control" name="select_end_date" id="select_end_date" <?php echo (isset($select_end_date)) ? 'value = "' . $select_end_date . '"' : ''; ?> placeholder="Xem đến ngày">					
	    </div>
	</div>

	<div class="col-md-3 col-sm-12">
	    @include("common.input_search")
	</div>

    </div>
</form>

<script>
//    $(document).ready(function(){
//	$('#date_range').daterangepicker({
//            locale: {
//                format: 'DD-MM-YYYY'
//            }
//        }, function(start, end) {
//            $('input[name="date_range"]').val(start.format('DD-MM-YYYY') + ' - ' + end.format('DD-MM-YYYY'));
//        });
//    });
    
    
    $(function () {
//	if ($("#select_start_date").length > 0) {
//	    $("#select_start_date").datetimepicker({
//		format: "yyyymm",
//		startView: "months",
//		minViewMode: "months",
//		bound:false,
//		autoOpen: false,
//		isDisabled: true,
//		hide:true
//	    }).trigger('blur');
//	}
	
	if ($("#select_start_date").length > 0) {
	    $("#select_start_date").daterangepicker({
		minDate: '01-01-2018',
		singleDatePicker: true,
		showDropdowns: true,
		timePicker: false,
		timePicker12Hour: false,
		timePickerIncrement: 5,
		timePickerSeconds: false,
		autoApply: true,
		format: 'DD-MM-YYYY'
	    });
	}
	
	if ($("#select_end_date").length > 0) {
	    $("#select_end_date").daterangepicker({
		minDate: '01-01-2018',
		singleDatePicker: true,
		showDropdowns: true,
		timePicker: false,
		timePicker12Hour: false,
		timePickerIncrement: 5,
		timePickerSeconds: false,
		autoApply: true,
		format: 'DD-MM-YYYY'
	    });
	}
    }); 
</script>

