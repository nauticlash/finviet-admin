<?php
//Loading Assets
$asset_already = [];
foreach ($forms as $form) {
  $type = @$form['type'] ?: 'text';

  if (in_array($type, $asset_already)) {
    continue;
  }
?>

  @if(file_exists(resource_path('views/vendor/crudbooster/type_components/'.$type.'/asset.blade.php')))
  @include('vendor.crudbooster.type_components.'.$type.'.asset')
  @elseif(file_exists(base_path('/vendor/crocodicstudio/crudbooster/src/views/default/type_components/'.$type.'/asset.blade.php')))
  @include('crudbooster::default.type_components.'.$type.'.asset')
  @endif

<?php
  $asset_already[] = $type;
} //end forms
?>

@push('head')
<style type="text/css">
  #table-detail tr td:first-child {
    /*font-weight: bold;*/
    width: 25%;
  }

  .class-hr {
    margin-top: 5px;
    margin-bottom: 5px;
  }

  @media screen and (max-width: 320px) {
    .about-me .personal-info li {
      width: 100%;
    }
  }

  @media only screen and (min-width: 1200px) {
    .about-me .personal-info li {
      width: 33%;
    }
  }


  .about-me {}

  .about-me p {
    line-height: 24px;
    letter-spacing: 0px;
  }

  .about-me .personal-info {
    display: inline-block;
    width: 100%;
    border: 1px solid #ddd;
    padding: 30px;
    list-style-type: none;
  }

  .about-me .personal-info li span.left {
    width: 40%;
    float: left;
    text-transform: uppercase;
    font-weight: bold;
  }

  .about-me .personal-info li span.right {}

  .about-me .personal-info li p {
    margin: 0px;
    line-height: 30px;
  }

  .about-me .personal-info li a {
    color: #346abb;
  }

  .grid-3-div {}
</style>
<script>
  $(function() {

    var height_li = 0;
    $(".personal-info li").each(function() {
      if (($(this).height()) > height_li) {
        height_li = $(this).height();
      }
    });
    $(".personal-info li").each(function() {
      $(this).css("height", height_li);
    });

    var height_div = 0;
    $(".personal-info .div-value").each(function() {
      if (($(this).height()) > height_div) {
        height_div = $(this).height();
      }
    });
    $(".personal-info .div-value").each(function() {
      $(this).css("height", height_div);
    });


  });
</script>
@endpush
<div class='table-responsive'>

  <div class="panel panel-default">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-offset-2 col-md-8">
          <!-- DIRECT CHAT WARNING -->
          <div class="box box-primary box-solid direct-chat direct-chat-primary">
            <div class="box-header">
              <h3 class="box-title"><?php echo trans("crudbooster.field_chat_history") ?></h3>
            </div><!-- /.box-header -->
            <div class="box-body">

              <!-- Conversations are loaded here -->
              <div class="direct-chat-messages" style="height: 530px" id="message_list">
                <!-- Message. Default to the left -->

                <?php
                if ($data) {
                  //				    echo "<pre>"; print_r($data);die();

                  foreach ($data as $row) {
                    //					echo "<pre>"; print_r($row);die();

                    $flag = 0;
                    $msgdate = $row->create_date;
                    //                                    $msg_date = date_create($row->create_date);
                    //                                    $msgdate = date_format($msg_date, "d M Y H:i A");

                    if ($row->toId == 1 && !$row->fromId != 1) {
                      $userimage = "";
                      if ($row->profilepic != "") {
                        $userimage = Config::get('constants.BASE_URL') . '/' . $row->profilepic;
                      } else {
                        $userimage = Config::get('constants.BASE_URL') . '/assets/images/noimage.png';
                      }
                      $flag = 1;
                ?>
                      <!-- Rider :: Message to the LEFT -->
                      <div class="direct-chat-msg">
                        <div class="direct-chat-info clearfix">
                          <span class="direct-chat-name pull-left"><?php echo ($row->firstname) ? $row->firstname . ' ' . $row->lastname  : '' ?></span>
                          <span class="direct-chat-timestamp pull-right"><?php echo $msgdate ?></span>
                        </div><!-- /.direct-chat-info -->
                        <img class="direct-chat-img" src="<?php echo $userimage ?>" alt="message user image"><!-- /.direct-chat-img -->
                        <div class="direct-chat-text col-md-6">
                          <?php echo ($row->message) ? $row->message : '' ?>
                        </div><!-- /.direct-chat-text -->
                      </div><!-- /.direct-chat-msg -->
                    <?php
                    }
                    if ($row->fromId == 1 && $row->toId != 1) {
                    ?>
                      <!--Admin :: Message to the right-->
                      <div class="direct-chat-msg right">
                        <div class="direct-chat-info clearfix ">

                          <span class="direct-chat-timestamp pull-left "><?php echo $msgdate ?></span>
                        </div>
                        <div class="direct-chat-text col-md-6 pull-right">
                          <?php echo ($row->message) ? $row->message : '' ?>
                        </div>
                      </div>
                <?php
                    }
                  }
                } else {
                  echo '<label style="margin-top: 25%;margin-left: 45%;">No conversation<label>';
                }
                ?>
              </div>
              <!--/.direct-chat-messages-->
            </div><!-- /.box-body -->
          </div>
          <!--/.direct-chat -->
          @if($allowSendNotification)
          <div class="box-chat">
            <div class="form-group">
              <div class="col-sm-10 no-padding">
                <input type="text" class="form-control" id="chat_message">
              </div>
              <div class="col-sm-2">
                <span class="btn btn-primary" id="send_message"><?php echo trans("crudbooster.field_send") ?></span>
              </div>
            </div>
          </div>
          @endif
        </div><!-- /.col -->
      </div>
    </section>
  </div>

  <script>
    $(document).ready(function() {

      var APP_URL = {
        !!json_encode(url('/')) !!
      };

      $("#send_message").on('click', function() {
        let message = $('#chat_message').val().trim();
        if (message !== '') {
          $.ajax({
            type: "POST",
            url: APP_URL + '/admin/message/send-message',
            data: {
              toId: <?php echo $toId; ?>,
              message: message
            },
            success: function(response) {
              //                        let data = JSON.parse(response);
              if (response.status === 1) {
                $('#chat_message').val('');
                //                            reloadData(true);
                location.reload();
              } else {
                alert('Đã có lỗi xảy ra. Vui lòng thử lại!');
              }
            }
          });
        } else {
          alert('Vui lòng nhập nội dung');
        }
      });

    });
  </script>

  <?php
  foreach ($forms as $index => $form) {
    $type = @$form['type'] ?: 'text';
  ?>
    @if($type == 'child' || $type == 'child_logs')
    @include('vendor.crudbooster.type_components.'.$type.'.component_detail')
    @endif
  <?php
  }
  ?>

</div>