@extends('crudbooster::admin_template')
@section('content')

<div class="modal fade" id="exampleModal<?= $object->id ?>" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel"><?= trans("crudbooster.assign_button") ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group" style="padding-left: 20px">
          <label for="sel1"><?= trans("crudbooster.assign_driver_for_order") ?></label>
          <select class="form-control" id="select_rider_id" name="select_rider_id" style="width: 75%">
            <option value=""><?= trans('crudbooster.field_select_rider') ?></option>
          </select>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= trans('crudbooster.button_close') ?></button>
        <button onclick="assign_rider_for_order(<?= $object->id ?>);" type="button" class="btn btn-primary"><?= trans('crudbooster.assign') ?></button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="cancel_order" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel"><?= trans("order.button_cancel") ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group" style="padding-left: 20px">
          <select name="refind" id="refind" class="form-control">
            <option value="1"><?= trans('order.re_find_riders') ?></option>
            <option value="0"><?= trans('order.return_to_partner') ?></option>
          </select>
        </div>
        <div class="form-group" style="padding-left: 20px">
          <label for="sel1"><?= trans("crudbooster.cancel_reason") ?></label>
          <textarea class="form-control" name="cancel_reason" id="cancel_reason" cols="30" rows="10"></textarea>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= trans('crudbooster.button_close') ?></button>
        <button onclick="cancel_order();" type="button" class="btn btn-danger"><?= trans('order.button_cancel') ?></button>
      </div>
    </div>
  </div>
</div>
<div class="">
  <div class="col-md-6">

  </div>

  @if($status)
  <div class="col-md-6">
    <button style="margin-bottom:5px;margin-left: 10px" type="button" class="btn btn-danger btn-lg pull-right" data-toggle="modal" data-target="#cancel_order">
      <?= trans("order.button_cancel") ?>
    </button>
    <button style="margin-bottom:5px;" type="button" class="btn btn-primary btn-lg pull-right" data-toggle="modal" data-target="#exampleModal<?= $object->id ?>">
      <?= trans("crudbooster.assign_button") ?>
    </button>
  </div>
  @endif
</div>


<section class="content" id="order_details">
  <div class="row">
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <div class="row">
            <div class="col-md-8">
              <h2 class="box-title"><?= trans('order.info') ?></h2>
            </div>
            <div class="col-md-4">
              <h2 class="box-title"><?= trans('rider.info') ?></h2>
            </div>
          </div>
        </div><!-- /.box-header -->
        <!-- form start -->
        <div class="row">
          <div class="col-md-4">
            <div class="box-body box-profile">
              <div class="form-group">
                <label for="exampleInputEmail1"><?= trans("crudbooster.field_pickup_address") ?></label>
                <div><?= $object->pickup_address . '-' . $object->merchant_contact .
                        '-' . $object->sender_name ?></div>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1"><?= trans("crudbooster.field_delivery_address") ?></label>
                <div><?= $object->delivery_address . '-' . $object->customer_contact .
                        '-' . $object->recipient_name ?></div>
              </div>
              <div class="form-group">
                <label><?= trans("order.distance") ?></label>
                <div><?= $object->no_of_km . ' km' ?></div>
              </div>
              <div class="form-group">
                <label><?= trans("crudbooster.field_description") ?></label>
                <div><?= $object->description ?></div>
              </div>
            </div><!-- /.box-body -->
          </div>

          <div class="col-md-4">
            <div class="box-body box-profile">
              <div class="form-group">
                <div class="row" style="font-weight: bold;">
                  <span style="text-align: left;" class="col-md-4">{{__("crudbooster.order_items")}}</span> 
                  <span style="text-align: left;" class="col-md-4">{{__("crudbooster.order_price")}}</span>
                  <span style="text-align: left;" class="col-md-4">{{__("crudbooster.order_quantity")}}</span>

                </div>
                <ul id="order_items">
                  @if($object->order_items)
                    @if(count(json_decode($object->order_items)) > 0)
                      @foreach(json_decode($object->order_items) as $item)
                      <li class="row">
                        <span style="text-align: left;" class="col-md-4">{{$item->name}}</span>
                        <span style="text-align: left;" class="col-md-4">{{number_format($item->amount, 0)}}</span>
                        <span style="text-align: left;" class="col-md-4">{{number_format($item->quantity, 0)}}</span>
                      </li>
                      @endforeach
                    @endif
                  @endif
                </ul>
              </div>
              <div class="form-group">
                <div class="row" style="font-weight: bold;">
                  <span style="text-align: left;" class="col-md-4">{{__("crudbooster.order_prepaid")}}</span>  <!-- order_prepaid chính là pickup_amount -->
                  <span style="text-align: left;" class="col-md-4">{{__("crudbooster.order_total")}}</span> 
                  <span style="text-align: left;" class="col-md-4">{{__("crudbooster.shipping_fee")}}</span> 
                </div>
                <div class="row">
                  <span style="text-align: left;" class="col-md-4">{{number_format($object->pickup_amount, 0)}}</span>
                  <span style="text-align: left;" class="col-md-4">{{number_format($object->order_total, 0)}}</span>
                  <span style="text-align: left;" class="col-md-4">{{number_format($object->user_cash_payment, 0)}}</span>
                </div>
              </div>

            </div><!-- /.box-body -->
          </div>
          <div class="col-md-4">
            @if(!empty($rider))
            <div class="box-body box-profile">
              <a href="<?= Config::get('constants.BASE_URL_ADMIN') . '/driver/detail/' . $rider->id ?>">
                @if(!empty($rider->profilepic))
                <img src="<?php echo Config::get('constants.BASE_URL') . '/images/profile/' . $rider->profilepic; ?>" class="profile-user-img img-responsive img-circle">
                @else
                <img src="<?php echo Config::get('constants.BASE_URL') . '/assets/images/noimage.png'; ?>" class="profile-user-img img-responsive img-circle">
                @endif
                <h3 class="profile-username text-center"><?= $rider->firstname ?>
                  - <?= $rider->contactno ?></h3>
              </a>
            </div><!-- /.box-body -->
            @endif
          </div>
        </div>
      </div>
    </div>
    <!--/.col (left) -->
  </div> <!-- /.row -->
  <div class="row">
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h2 class="box-title"><?= trans('order.progress') ?></h2>
        </div><!-- /.box-header -->
        <div class="box-body box-profile">
          <ul class="milestone">
            <li class="row completed">
              <div class="status col-md-3">
                <div class="tags">
                  <span class="tag">
                    <span><?= trans('order.create_order') ?></span>
                  </span>
                  <div style="clear: both;"></div>
                </div>
              </div>
              <div class="block col-md-4">
                <div class="block_content">
                  <h2 class="title">
                    <span><?= trans('order.create_order_success') ?></span>
                  </h2>
                  <div class="byline">
                    <span><?= get_formatted_date($object->created_at) ?></span>
                  </div>
                  <p class="excerpt">

                  </p>
                </div>
              </div>
            </li>
            <?php
            $allocateAlert = get_order_alert($object);
            $allocateStatus = trim('allocating_' . $allocateAlert, '_');
            $allocateMessage = 'riders_receive_order';
            $allocateTime = '';
            $startOrderAlert = 'disabled';
            $startOrderStatus = 'starting';
            $startOrderMessage = 'rider_starting_order';
            $startOrderTime = '';
            $pickUpAlert = 'disabled';
            $pickUpStatus = 'picking_up';
            $pickUpMessage = 'rider_picking_up_order';
            $pickUpTime = '';
            $dropOffAlert = 'disabled';
            $dropOffStatus = 'delivering';
            $dropOffMessage = 'rider_delivering_order';
            $dropOffTime = '';
            $completeAlert = 'disabled';
            $completeStatus = 'completing';
            $completeMessage = 'rider_completing_order';
            $completeTime = '';
            $cancelMessage = '';
            if ($object->status == 'Canceled') {
              if (!empty($object->admincancel)) {
                $cancelMessage = 'canceled_by_admin';
              } else {
                if ($object->cancelby == $object->userid) {
                  $cancelMessage = 'canceled_by_partner';
                } else if ($object->cancelby == $object->rider_id && !empty($object->rider_id)) {
                  $cancelMessage = 'canceled_by_rider';
                }
              }
              $allocateAlert = $allocateStatus = 'canceled';
              $allocateMessage = $cancelMessage;
              $allocateTime = get_formatted_date($object->cancel_time);
            }
            $cancelReason = trans('crudbooster.cancel_reason') . ': ' . $object->cancel_reason;
            if (!empty($object->rider_id)) {
              $allocateAlert = 'completed';
              $allocateStatus = 'accepted';
              $allocateMessage = 'rider_accepted_order';
              $allocateTime = get_formatted_date($object->rider_accept_time);
              $startOrderAlert = get_order_alert($object);
              $startOrderStatus = trim($startOrderStatus . '_' . $startOrderAlert, '_');
              if ($object->status == 'Canceled') {
                $startOrderAlert = $startOrderStatus = 'canceled';
                $startOrderMessage = $cancelMessage;
                $startOrderTime = get_formatted_date($object->cancel_time);
              }
              if ($object->is_pickup > 0) {
                $startOrderAlert = 'completed';
                $startOrderStatus = 'started';
                $startOrderMessage = 'rider_started_order';
                $startOrderTime = '';
                $pickUpAlert = get_order_alert($object);
                $pickUpStatus = trim($pickUpStatus . '_' . $pickUpAlert, '_');
                if ($object->status == 'Canceled') {
                  $pickUpAlert = $pickUpStatus = 'canceled';
                  $pickUpMessage = $cancelMessage;
                  $pickUpTime = get_formatted_date($object->cancel_time);
                }
                if ($object->is_pickup > 1) {
                  $pickUpAlert = 'completed';
                  $pickUpStatus = 'picked_up';
                  $pickUpMessage = 'rider_picked_up_order';
                  $pickUpTime = get_formatted_date($object->pickup_time);
                  $dropOffAlert = get_order_alert($object);
                  $dropOffStatus = trim($dropOffStatus . '_' . $dropOffAlert, '_');
                  if ($object->status == 'Canceled') {
                    $dropOffAlert = $dropOffStatus = 'canceled';
                    $dropOffMessage = $cancelMessage;
                    $dropOffTime = get_formatted_date($object->cancel_time);
                  }
                  if ($object->is_dropoff > 0) {
                    if (!empty($object->is_return)) {
                      $dropOffAlert = 'warning';
                      $dropOffStatus = 'returning';
                      $dropOffMessage = 'rider_returning_order';
                    } else {
                      $dropOffAlert = 'completed';
                      $dropOffStatus = 'delivered';
                      $dropOffMessage = 'rider_delivered_order';
                    }
                    $dropOffTime = get_formatted_date($object->dropoff_time);
                    $completeAlert = get_order_alert($object);
                    $completeStatus = trim($completeStatus . '_' . $completeAlert, '_');
                    if ($object->status == 'Canceled') {
                      $completeAlert = $completeStatus = 'canceled';
                      $completeMessage = $cancelMessage;
                      $completeTime = get_formatted_date($object->cancel_time);
                    }
                    if ($object->status == 'Complete') {
                      $completeAlert = 'completed';
                      $completeStatus = 'completed';
                      $completeMessage = 'rider_completed_order';
                      $completeTime = get_formatted_date($object->delivery_time);
                    }
                  }
                }
              }
            }
            ?>
            <li class="row <?= $allocateAlert ?>">
              <div class="status col-md-3">
                <div class="tags">
                  <span class="tag">
                    <span><?= trans('order.status_' . $allocateStatus) ?></span>
                  </span>
                  <div style="clear: both;"></div>
                </div>
              </div>
              <div class="block col-md-4">
                <div class="block_content">
                  <h2 class="title">
                    <span><?= trans('order.' . $allocateMessage) ?></span>
                  </h2>
                  <div class="byline">
                    <span><?= $allocateTime ?></span>
                  </div>
                  <p class="excerpt">
                    <?php if ($allocateAlert == 'canceled') {
                      echo $cancelReason;
                    } else {
                      if (!empty($rider)) : ?>
                        <a href="<?= Config::get('constants.BASE_URL_ADMIN') . '/driver/detail/' . $rider->id ?>">
                          <?php if (!empty($rider->profilepic)) { ?>
                            <img src="<?php echo Config::get('constants.BASE_URL') . '/images/profile/' . $rider->profilepic; ?>" class="profile-img img-responsive img-circle">
                          <?php } else { ?>
                            <img src="<?php echo Config::get('constants.BASE_URL') . '/assets/images/noimage.png'; ?>" class="profile-img img-responsive img-circle">
                          <?php } ?>
                          <span><?= $rider->firstname ?> - <?= $rider->contactno ?></span>
                        </a>
                        <?php
                      else :
                        foreach ($riders as $receiver) :
                        ?>
                          <a href="<?= Config::get('constants.BASE_URL_ADMIN') . '/driver/detail/' . $receiver->id ?>">
                            <span><?= $receiver->firstname ?> - <?= $receiver->contactno ?></span>
                          </a>
                          <br />
                    <?php
                        endforeach;
                      endif;
                    }
                    ?>
                  </p>
                </div>
              </div>
            </li>
            <li class="row <?= $startOrderAlert ?>">
              <div class="status col-md-3">
                <div class="tags">
                  <span class="tag">
                    <span><?= trans('order.status_' . $startOrderStatus) ?></span>
                  </span>
                  <div style="clear: both;"></div>
                </div>
              </div>
              <div class="block col-md-4">
                <div class="block_content">
                  <h2 class="title">
                    <span><?= trans('order.' . $startOrderMessage) ?></span>
                  </h2>
                  <div class="byline">
                    <span><?= $startOrderTime ?></span>
                  </div>
                  <p class="excerpt">
                    <?php if ($startOrderAlert == 'canceled') {
                      echo $cancelReason;
                    } ?>
                  </p>
                </div>
              </div>
              <div class="pull-right col-md-4 right-buttons">
                <?php if ($object->status == 'In Progress' && $object->is_pickup == 0) : ?>
                  <a href="<?= Config::get('constants.BASE_URL_ADMIN') . '/order/start-order/' . $object->id . '/1' ?>">
                    <span class="btn btn-primary"><?= trans('order.rider_started_order') ?></span>
                  </a>
                <?php endif; ?>
              </div>
            </li>
            <li class="row <?= $pickUpAlert ?>">
              <div class="status col-md-3">
                <div class="tags">
                  <span class="tag">
                    <span><?= trans('order.status_' . $pickUpStatus) ?></span>
                  </span>
                  <div style="clear: both;"></div>
                </div>
              </div>
              <div class="block col-md-4">
                <div class="block_content">
                  <h2 class="title">
                    <span><?= trans('order.' . $pickUpMessage) ?></span>
                  </h2>
                  <div class="byline">
                    <span><?= $pickUpTime ?></span>
                  </div>
                  <p class="excerpt">
                    <?php if ($pickUpAlert == 'canceled') {
                      echo $cancelReason;
                    } ?>
                  </p>
                </div>
              </div>
              <div class="pull-right col-md-4 right-buttons">
                <?php if ($object->status == 'In Progress' && $object->is_pickup == 1) : ?>
                  <a href="<?= Config::get('constants.BASE_URL_ADMIN') . '/order/pick-up/' . $object->id . '/1' ?>">
                    <span class="btn btn-primary"><?= trans('order.rider_picked_up_order') ?></span>
                  </a>
                <?php endif; ?>
              </div>
            </li>
            <li class="row <?= $dropOffAlert ?>">
              <div class="status col-md-3">
                <div class="tags">
                  <span class="tag">
                    <span><?= trans('order.status_' . $dropOffStatus) ?></span>
                  </span>
                  <div style="clear: both;"></div>
                </div>
              </div>
              <div class="block col-md-4">
                <div class="block_content">
                  <h2 class="title">
                    <span><?= trans('order.' . $dropOffMessage) ?></span>
                  </h2>
                  <div class="byline">
                    <span><?= $dropOffTime ?></span>
                  </div>
                  <p class="excerpt">
                    <?php if ($dropOffAlert == 'canceled') {
                      echo $cancelReason;
                    } ?>
                  </p>
                </div>
              </div>
              <div class="pull-right col-md-4 right-buttons">
                <?php if ($object->status == 'In Progress' && $object->is_pickup == 3 && $object->is_dropoff == 0) : ?>
                  <a href="<?= Config::get('constants.BASE_URL_ADMIN') . '/order/drop-off/' . $object->id . '/0' ?>">
                    <span class="btn btn-primary"><?= trans('order.order_delivered') ?></span>
                  </a>
                  <a href="<?= Config::get('constants.BASE_URL_ADMIN') . '/order/drop-off/' . $object->id . '/1' ?>">
                    <span class="btn btn-default"><?= trans('order.order_returning') ?></span>
                  </a>
                <?php endif; ?>
              </div>
            </li>
            <li class="row <?= $completeAlert ?>">
              <div class="status col-md-3">
                <div class="tags">
                  <span class="tag">
                    <span><?= trans('order.status_' . $completeStatus) ?></span>
                  </span>
                  <div style="clear: both;"></div>
                </div>
              </div>
              <div class="block col-md-4">
                <div class="block_content">
                  <h2 class="title">
                    <span><?= trans('order.' . $completeMessage) ?></span>
                  </h2>
                  <div class="byline">
                    <span><?= $completeTime ?></span>
                  </div>
                  <p class="excerpt">
                    <?php if ($object->signature_image) : ?>
                      <img src="<?php echo Config::get('constants.BASE_URL') . '/images/profile/' . $object->signature_image; ?>" width="150">
                    <?php endif; ?>
                  </p>
                </div>
              </div>
              <div class="pull-right col-md-4 right-buttons">
                <?php if ($object->status == 'In Progress' && $object->is_dropoff == 2) : ?>
                  <a href="<?= Config::get('constants.BASE_URL_ADMIN') . '/order/complete-order/' . $object->id ?>">
                    <span class="btn btn-primary"><?= trans('order.complete_order') ?></span>
                  </a>
                <?php endif; ?>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <!--/.col (left) -->
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h2 class="box-title">Bản đồ vị trí</h2>
        </div>
        <div style="width: 100%; height: 500px" id="map"></div>
      </div>
    </div>
  </div>
</section><!-- /.content -->
@endsection

@push('bottom')
<script>
  let map;

  function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
      center: {
        lat: 10.7940666,
        lng: 106.6562605
      },
      zoom: 15
    });
    let bounds = new google.maps.LatLngBounds();
    let pickup_location = new google.maps.LatLng(<?= $object->plat ?>, <?= $object->plng ?>);
    new google.maps.Marker({
      position: pickup_location,
      map: map,
      icon: '/assets/images/ic_location_pickup.png'
    });
    bounds.extend(pickup_location);
    let dropoff_location = new google.maps.LatLng(<?= $object->dllat ?>, <?= $object->dllng ?>);
    new google.maps.Marker({
      position: dropoff_location,
      map: map,
      icon: '/assets/images/ic_location_dropoff.png'
    });
    bounds.extend(dropoff_location);
    <?php if (!empty($rider)) { ?>
      let rider_position = new google.maps.LatLng(<?= $rider->lat ?>, <?= $rider->lng ?>);
      new google.maps.Marker({
        position: rider_position,
        map: map,
      });
      bounds.extend(rider_position);
    <?php } ?>
    map.fitBounds(bounds);
  }

  $(document).ready(function() {
    $('#select_rider_id').select2({
      minimumInputLength: 2,
      ajax: {
        url: '<?php echo Config::get('constants.BASE_URL_ADMIN') . '/driver/free-rider' ?>',
        dataType: 'json',
        delay: 250,
        processResults: function(data) {
          return {
            results: $.map(data, function(item) {
              return {
                text: item.firstname + ' - ' + item.contactno,
                id: item.id
              }
            })
          };
        },
        cache: true
      }
      //        }).on('select2:select', function (e) {
      //            $('#filter_form').submit();
    });
  });

  function cancel_order() {
    $.ajax({
      method: "POST",
      url: "<?php echo Config::get('constants.BASE_URL_ADMIN') . '/order/cancel-order' ?>",
      data: {
        order_id: "<?= $object->id ?>",
        cancel_reason: $('#cancel_reason').val(),
        refind: $('#refind').val()
      }
    }).done(function(msg) {
      let data = msg;
      if (data.success) {
        swal({
          title: "Thành Công",
          text: "Vui lòng load lại trang",
          type: "success",
          showCancelButton: false,
          closeOnConfirm: true,
        });
      } else {
        swal({
          title: "Có lỗi xảy ra",
          text: "Vui lòng load lại trang",
          type: "danger",
          showCancelButton: false,
          closeOnConfirm: true,
        });
      }
    });
  }

  function assign_rider_for_order(orderid) {
    event.preventDefault();
    var select_rider_id = $('#select_rider_id').val();
    var params = {
      orderid: orderid,
      select_rider_id: select_rider_id
    };
    if (select_rider_id == '') {
      alert("<?php echo trans('crudbooster.field_select_rider'); ?>");
      return false;
    }

    var r = confirm("<?php echo trans('crudbooster.confirm_assign_rider'); ?>");
    if (r === true) {
      js_post_data('<?= Config::get('constants.BASE_URL_ADMIN') . '/order/assgin-order-to-rider' ?>', params);
    }
  }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=<?= env('GOOGLE_API_KEY') ?>&callback=initMap" async defer></script>
@endpush