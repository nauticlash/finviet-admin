@extends('crudbooster::admin_template')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h2 class="box-title">Thông tin đơn hàng</h2>
                </div>
                <div class="box-body box-profile">
                    <div class="form-group">
                        <label for="exampleInputEmail1">{{trans("crudbooster.field_pickup_address")}}</label>
                        <div></div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">{{trans("crudbooster.field_delivery_address")}}</label>
                        <div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection