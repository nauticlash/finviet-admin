
<div id="orders_table">
    <form id='form-table' method='post' action='{{CRUDBooster::mainpath("action-selected")}}'>
        <input type='hidden' name='button_name' value=''/>
        <input type='hidden' name='_token' value='{{csrf_token()}}'/>
        <table id='table_dashboard' class="table table-hover">
            <thead>
            <tr class="active">
                <?php if($button_bulk_action):?>
                <th width='3%'><input type='checkbox' id='checkall'/></th>
                <?php endif;?>
                <?php if($show_numbering):?>
                <th width="1%">{{ trans('crudbooster.no') }}</th>
                <?php endif;?>
                <?php
                foreach ($columns as $col) {
                    if ($col['visible'] === FALSE) continue;

                    $sort_column = Request::get('filter_column');
                    $colname = $col['label'];
                    $name = $col['name'];
                    $field = $col['field_with'];
                    $width = ($col['width']) ?: "auto";
                    $style = ($col['style']) ?: "auto";
                    $sort = $col['sortable'];
                    $mainpath = trim(CRUDBooster::mainpath(), '/') . $build_query;
                    // echo "<th width='$width'>";
                    echo "<th width='$width' $style>";
                    if ($sort === false) {
                        echo "<a>$colname</a>";
                    } else {
                        if (isset($sort_column[$field])) {
                            switch ($sort_column[$field]['sorting']) {
                                case 'asc':
                                    $url = CRUDBooster::urlFilterColumn($field, 'sorting', 'desc');
                                    echo "<a href='$url' title='" . trans('crudbooster.field_table_descending') . "'>$colname &nbsp; <i class='fa fa-sort-asc'></i></a>";
                                    break;
                                case 'desc':
                                    $url = CRUDBooster::urlFilterColumn($field, 'sorting', 'asc');
                                    echo "<a href='$url' title='" . trans('crudbooster.field_table_ascending') . "'>$colname &nbsp; <i class='fa fa-sort-desc'></i></a>";
                                    break;
                                default:
                                    $url = CRUDBooster::urlFilterColumn($field, 'sorting', 'asc');
                                    echo "<a href='$url' title='" . trans('crudbooster.field_table_ascending') . "'>$colname &nbsp; <i class='fa fa-sort'></i></a>";
                                    break;
                            }
                        } else {
                            $url = CRUDBooster::urlFilterColumn($field, 'sorting', 'asc');
                            echo "<a href='$url' title='" . trans('crudbooster.field_table_ascending') . "'>$colname &nbsp; <i class='fa fa-sort'></i></a>";
                        }
                    }
                    echo "</th>";
                }
                ?>

                @if($button_table_action)
                    @if(CRUDBooster::isUpdate() || CRUDBooster::isDelete() || CRUDBooster::isRead())
                        <th width='{{$button_action_width?:"auto"}}'
                            style="text-align:right">{{trans("crudbooster.action_label")}}</th>
                    @endif
                @endif
            </tr>
            </thead>
            <tbody>
            <?php
            if (count($result) == 0) {
            ?>
            <tr class='warning'>
                <?php if($button_bulk_action && $show_numbering):?>
                <td colspan='{{count($columns)+3}}' align="center">
                <?php elseif( ($button_bulk_action && !$show_numbering) || (!$button_bulk_action && $show_numbering) ):?>
                <td colspan='{{count($columns)+2}}' align="center">
                <?php else:?>
                <td colspan='{{count($columns)+1}}' align="center">
                    <?php endif;?>

                    <i class='fa fa-search'></i> {{trans("crudbooster.table_data_not_found")}}
                </td>
            </tr>
            <?php
            } else {
            foreach ($result as $row) {
                $orderStatus = get_order_status($row);
                $orderAlert = get_order_alert($row);
                if (!empty($orderAlert)) {
                    $orderStatus .= '_' . $orderAlert;
                } else {
                    $orderAlert = 'default';
                }
            ?>
            <tr class="<?=$orderStatus?>">
                <td style="display:none"><?=$row->id?></td>
                <td><a href="<?=CRUDBooster::mainpath('detail/' . $row->id)?>"><?=$row->merchant_order_id?></a></td>
                <td><?=get_formatted_date($row->orderdate)?></td>
                <!-- <td><?=$row->m_name?></td> -->
                <td>
                    <h4><span class="label label-<?=$orderAlert?>"><?=trans('order.status_' . $orderStatus)?></span></h4>
                </td>
                <td>
                    <a href="<?=Config::get('constants.BASE_URL_ADMIN') . '/driver/detail/' . $row->rider_id?>" target="_blank">
                        <?=$row->fullname?>
                    </a>
                </td>
                <td><?=$row->pickup_address?></td>
                <td><?=$row->delivery_address?></td>
                <td><?=$row->no_of_km . " km"?></td>
                <td><?=get_format_money_usd($row->shipping_amount)?></td>
                <td>
                    <div class="button_action" style="text-align:right">
                        <a class="btn btn-xs btn-primary btn-detail" title="{{trans('crudbooster.action_detail_data')}}"
                           href="<?=CRUDBooster::mainpath('detail/' . $row->id)?>">
                            <i class="fa fa-eye"></i>
                        </a>
                    </div>
                </td>
            </tr>
            <?php
                }
            }
            ?>
            </tbody>


            <tfoot>
            <tr>
                <?php if($button_bulk_action):?>
                <th>&nbsp;</th>
                <?php endif;?>

                <?php if($show_numbering):?>
                <th>&nbsp;</th>
                <?php endif;?>

                <?php
                foreach ($columns as $col) {
                    if ($col['visible'] === FALSE) continue;
                    $colname = $col['label'];
                    $width = ($col['width']) ?: "auto";
                    $style = ($col['style']) ?: "";
                    echo "<th width='$width' $style>$colname</th>";
                }
                ?>

                @if($button_table_action)
                    @if(CRUDBooster::isUpdate() || CRUDBooster::isDelete() || CRUDBooster::isRead())
                        <th> -</th>
                    @endif
                @endif
            </tr>
            </tfoot>
        </table>
    </form><!--END FORM TABLE-->
</div>
<div class="col-md-8 no-padding">{!! urldecode(str_replace("/?","?",$result->appends(Request::all())->render())) !!}</div>
<?php
$from = $result->count() ? ($result->perPage() * $result->currentPage() - $result->perPage() + 1) : 0;
$to = $result->perPage() * $result->currentPage() - $result->perPage() + $result->count();
$total = $result->total();
?>
<div class="col-md-4 no-padding"><span class="pull-right">{{ trans("crudbooster.filter_rows_total") }}
        : {{ $from }} {{ trans("crudbooster.filter_rows_to") }} {{ $to }} {{ trans("crudbooster.filter_rows_of") }} {{ $total }}</span>
</div>
@push('bottom')
<script>
    $(function () {
        $('.btn-filter-data').click(function () {
            $('#filter-data').modal('show');
        })

        $('.btn-export-data').click(function () {
            $('#export-data').modal('show');
        })

        var toggle_advanced_report_boolean = 1;
        $(".toggle_advanced_report").click(function () {

            if (toggle_advanced_report_boolean == 1) {
                $("#advanced_export").slideDown();
                $(this).html("<i class='fa fa-minus-square-o'></i> {{trans('crudbooster.export_dialog_show_advanced')}}");
                toggle_advanced_report_boolean = 0;
            } else {
                $("#advanced_export").slideUp();
                $(this).html("<i class='fa fa-plus-square-o'></i> {{trans('crudbooster.export_dialog_show_advanced')}}");
                toggle_advanced_report_boolean = 1;
            }

        })


        $("#table_dashboard .checkbox").click(function () {
            var is_any_checked = $("#table_dashboard .checkbox:checked").length;
            if (is_any_checked) {
                $(".btn-delete-selected").removeClass("disabled");
            } else {
                $(".btn-delete-selected").addClass("disabled");
            }
        })

        $("#table_dashboard #checkall").click(function () {
            var is_checked = $(this).is(":checked");
            $("#table_dashboard .checkbox").prop("checked", !is_checked).trigger("click");
        })

        $('#btn_advanced_filter').click(function () {
            $('#advanced_filter_modal').modal('show');
        })

        $(".filter-combo").change(function () {
            var n = $(this).val();
            var p = $(this).parents('.row-filter-combo');
            var type_data = $(this).attr('data-type');
            var filter_value = p.find('.filter-value');

            p.find('.between-group').hide();
            p.find('.between-group').find('input').prop('disabled', true);
            filter_value.val('').show().focus();
            switch (n) {
                default:
                    filter_value.removeAttr('placeholder').val('').prop('disabled', true);
                    p.find('.between-group').find('input').prop('disabled', true);
                    break;
                case 'like':
                case 'not like':
                    filter_value.attr('placeholder', '{{trans("crudbooster.filter_eg")}} : {{trans("crudbooster.filter_lorem_ipsum")}}').prop('disabled', false);
                    break;
                case 'asc':
                    filter_value.prop('disabled', true).attr('placeholder', '{{trans("crudbooster.filter_sort_ascending")}}');
                    break;
                case 'desc':
                    filter_value.prop('disabled', true).attr('placeholder', '{{trans("crudbooster.filter_sort_descending")}}');
                    break;
                case '=':
                    filter_value.prop('disabled', false).attr('placeholder', '{{trans("crudbooster.filter_eg")}} : {{trans("crudbooster.filter_lorem_ipsum")}}');
                    break;
                case '>=':
                    filter_value.prop('disabled', false).attr('placeholder', '{{trans("crudbooster.filter_eg")}} : 1000');
                    break;
                case '<=':
                    filter_value.prop('disabled', false).attr('placeholder', '{{trans("crudbooster.filter_eg")}} : 1000');
                    break;
                case '>':
                    filter_value.prop('disabled', false).attr('placeholder', '{{trans("crudbooster.filter_eg")}} : 1000');
                    break;
                case '<':
                    filter_value.prop('disabled', false).attr('placeholder', '{{trans("crudbooster.filter_eg")}} : 1000');
                    break;
                case '!=':
                    filter_value.prop('disabled', false).attr('placeholder', '{{trans("crudbooster.filter_eg")}} : {{trans("crudbooster.filter_lorem_ipsum")}}');
                    break;
                case 'in':
                    filter_value.prop('disabled', false).attr('placeholder', '{{trans("crudbooster.filter_eg")}} : {{trans("crudbooster.filter_lorem_ipsum_dolor_sit")}}');
                    break;
                case 'not in':
                    filter_value.prop('disabled', false).attr('placeholder', '{{trans("crudbooster.filter_eg")}} : {{trans("crudbooster.filter_lorem_ipsum_dolor_sit")}}');
                    break;
                case 'between':
                    filter_value.val('').hide();
                    p.find('.between-group input').prop('disabled', false);
                    p.find('.between-group').show().focus();
                    p.find('.filter-value-between').prop('disabled', false);
                    break;
            }
        })

        /* Remove disabled when reload page and input value is filled */
        $(".filter-value").each(function () {
            var v = $(this).val();
            if (v != '') $(this).prop('disabled', false);
        })

    })
</script>
@endpush
