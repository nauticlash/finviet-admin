<form method="get" action="" id="filter_form">
    <div class="col-md-12 col-sm-12">
        <div class="col-md-3 col-sm-12">
            <div class="input-group">
                <span class="input-group-addon"><a href="javascript:void(0)"><i class="fa fa-calendar"></i></a></span>
                <input type="text" readonly class="form-control" id="select_start_date" name="select_start_date"
                       title="<?=trans('crudbooster.field_analysis_star_day')?>" value="<?=$select_start_date?>"
                       placeholder="<?=trans('crudbooster.field_analysis_star_day')?>"/>
            </div>
        </div>
        <div class="col-md-3 col-sm-12">
            <div class="input-group">
                <span class="input-group-addon"><a href="javascript:void(0)"><i class="fa fa-calendar"></i></a></span>
                <input type="text" readonly class="form-control" id="select_end_date" name="select_end_date"
                       title="<?=trans('crudbooster.field_analysis_end_day')?>" value="<?=$select_end_date?>"
                       placeholder="<?=trans('crudbooster.field_analysis_end_day')?>"/>
            </div>
        </div>

        <!-- <div class="col-md-3 col-sm-12">
            <select class="form-control select_filter" id="select_merchant_id" name="select_merchant_id">
                <option value="">{{ trans('order.filter_by_merchant') }}</option>
                @if(isset($merchants))
                    @foreach($merchants as $merchant)
                        <option value="{{$merchant->id}}" <?php echo (isset($select_merchant_id) && $select_merchant_id == $merchant->id) ? 'selected' : ''; ?> >{{$merchant->name}}</option>
                    @endforeach
                @endif
            </select>
        </div> -->
        <div class="col-md-3 col-sm-12">
            <select class="form-control" id="select_rider_id" name="select_rider_id">
                <option value="">{{ trans('crudbooster.filter_by_rider') }}</option>
                @if(!empty($select_rider_id))
                    <option value="{{$rider->id}}" selected>{{$rider->contactno}} - {{$rider->firstname}}</option>
                @endif
            </select>
        </div>
        <div class="col-md-3 col-sm-12">
            <select class="form-control select_filter" id="select_status" name="select_status">
                <option value="">{{ trans('crudbooster.filter_by_order_status') }}</option>
                @if(isset($array_status))
                    @foreach($array_status as $key => $status)
                        <option value="{{$key}}" <?php echo (isset($select_status) && $select_status == $key) ? 'selected' : ''; ?> >{{trans("order." . $status)}}</option>
                    @endforeach
                @endif
            </select>
        </div>

        <div class="col-md-3 col-sm-12">
            @include("common.input_search")
        </div>

        <div class="col-md-3 col-sm-12 pull-right">
            <button type='submit'
                    class="btn btn-primary" <?php echo 'title= "' . trans('crudbooster.filter_search') . '"'?> >
                <i class="fa fa-search"></i></button>
            <?php
            if (!empty(Request::all())) {
            ?>
            <a href="<?php echo Config::get('constants.BASE_URL_ADMIN'); ?>/order" class="btn btn-warning"
            <?php echo 'title="' . trans('crudbooster.button_reset') . '"' ?>>
                <i class="fa fa-refresh" aria-hidden="true"></i>
            </a>
            <?php
            }
            ?>
        </div>

    </div>

</form>
@push('bottom')
<script>
    $(document).ready(function () {
      var max_start = new Date();
      max_start.setDate(max_start.getDate() - 1);

      var max_end = new Date();
      max_end.setHours(23,59,59,999);

        $("#select_start_date").daterangepicker({
            minDate: '01-01-2018',
            maxDate: max_start,
            singleDatePicker: true,
            showDropdowns: true,
            autoApply: false,
            autoUpdateInput: false,
            timePicker: false,
            locale: {
                format: 'DD-MM-YYYY'
            }
        }).on('apply.daterangepicker', function(ev, picker) {
            $('#select_start_date').val(picker.startDate.format(picker.locale.format));
        });
        $("#select_end_date").daterangepicker({
            minDate: '01-01-2018',
            maxDate : max_end,
            singleDatePicker: true,
            showDropdowns: true,
            autoApply: false,
            autoUpdateInput: false,
            timePicker: false,
            locale: {
                format: 'DD-MM-YYYY'
            }
        }).on('apply.daterangepicker', function(ev, picker) {
            $('#select_end_date').val(picker.startDate.format(picker.locale.format));
        });
        $('#select_rider_id').select2({
            minimumInputLength: 2,
            ajax: {
                url: '<?php echo Config::get('constants.BASE_URL_ADMIN') . '/driver/contact-no' ?>',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.firstname + ' - ' + item.contactno,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        }).on('select2:select', function (e) {
            $('#filter_form').submit();
        });
        $('.select_filter').select2().on('select2:select', function (e) {
            $('#filter_form').submit();
        });
    });
</script>
@endpush

