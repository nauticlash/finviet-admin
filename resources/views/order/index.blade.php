@extends('crudbooster::admin_template')

@section('content')

    @if($index_statistic)
        <div id='box-statistic' class='row'>
            @foreach($index_statistic as $stat)
                <div class="{{ ($stat['width'])?:'col-sm-3' }}">
                    <div class="small-box bg-{{ $stat['color']?:'red' }}">
                        <div class="inner">
                            <h3>{{ $stat['count'] }}</h3>
                            <p>{{ $stat['label'] }}</p>
                        </div>
                        <div class="icon">
                            <i class="{{ $stat['icon'] }}"></i>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endif

    @if(!is_null($pre_index_html) && !empty($pre_index_html))
        {!! $pre_index_html !!}
    @endif


    @if(g('return_url'))
        <p><a href='{{g("return_url")}}'><i class='fa fa-chevron-circle-{{ trans('crudbooster.left') }}'></i>
                &nbsp; {{trans('crudbooster.form_back_to_list',['module'=>urldecode(g('label'))])}}</a></p>
    @endif

    @if($parent_table)
        <div class="box box-default">
            <div class="box-body table-responsive no-padding">
                <table class='table table-bordered'>
                    <tbody>
                    <tr class='active'>
                        <td colspan="2"><strong><i class='fa fa-bars'></i> {{ ucwords(urldecode(g('label'))) }}</strong></td>
                    </tr>
                    @foreach(explode(',',urldecode(g('parent_columns'))) as $c)
                        <tr>
                            <td width="25%"><strong>
                                    @if(urldecode(g('parent_columns_alias')))
                                        {{explode(',',urldecode(g('parent_columns_alias')))[$loop->index]}}
                                    @else
                                        {{  ucwords(str_replace('_',' ',$c)) }}
                                    @endif
                                </strong></td>
                            <td> {{ $parent_table->$c }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif

    <!-- MODAL FOR EXPORT DATA-->
    <div class="modal fade" tabindex="-1" role="dialog" id='export-data'>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" aria-label="Close" type="button" data-dismiss="modal">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title"><i class='fa fa-download'></i> {{trans("crudbooster.export_dialog_title")}}
                    </h4>
                </div>

                <form method='post' target="_blank" action='{{ CRUDBooster::mainpath("export-data?t=".time()) }}'>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    {!! CRUDBooster::getUrlParameters() !!}
                    <div class="modal-body">
                        <div class="form-group">
                            <label>{{trans("crudbooster.export_dialog_filename")}}</label>
                            <input type='text' name='filename' class='form-control' required
                                   value='Report {{ $module_name }} - {{date("d M Y")}}'/>
                            <div class='help-block'>
                                {{trans("crudbooster.export_dialog_help_filename")}}
                            </div>
                        </div>

                        <div class="form-group">
                            <label>{{trans("crudbooster.export_dialog_maxdata")}}</label>
                            <input type='number' name='limit' class='form-control' required value='100' max="100000"
                                   min="1"/>
                            <div class='help-block'>{{trans("crudbooster.export_dialog_help_maxdata")}}</div>
                        </div>

                        <div class='form-group'>
                            <label>{{trans("crudbooster.export_dialog_columns")}}</label><br/>
                            <?php foreach($columns as $col):
                                if ($col['visible'] === false) {
                                    continue;
                                }
                            ?>
                                <div class='checkbox inline'>
                                    <label>
                                        <input type='checkbox' checked name='columns[]' value='{{$col["name"]}}'>
                                        {{$col["label"]}}
                                    </label>
                                </div>
                            <?php endforeach; ?>
                        </div>

                        <div class="form-group">
                            <label>{{trans("crudbooster.export_dialog_format_export")}}</label>
                            <select name='fileformat' class='form-control'>
                                <option value='xlsx'>Microsoft Excel 2007 (xlsx)</option>
                                <option value='pdf'>PDF</option>
                                <option value='csv'>CSV</option>
                                <option value='xls'>Microsoft Excel (xls)</option>
                            </select>
                        </div>

                        <p><a href='javascript:void(0)' class='toggle_advanced_report'><i
                                    class='fa fa-plus-square-o'></i> {{trans("crudbooster.export_dialog_show_advanced")}}
                            </a></p>

                        <div id='advanced_export' style='display: none'>


                            <div class="form-group">
                                <label>{{trans("crudbooster.export_dialog_page_size")}}</label>
                                <select class='form-control' name='page_size'>
                                    <option
                                        <?=($setting->default_paper_size == 'Letter') ? "selected" : ""?> value='Letter'>
                                        Letter
                                    </option>
                                    <option
                                        <?=($setting->default_paper_size == 'Legal') ? "selected" : ""?> value='Legal'>
                                        Legal
                                    </option>
                                    <option
                                        <?=($setting->default_paper_size == 'Ledger') ? "selected" : ""?> value='Ledger'>
                                        Ledger
                                    </option>
                                    <?php for($i = 0;$i <= 8;$i++):
                                    $select = ($setting->default_paper_size == 'A' . $i) ? "selected" : "";
                                    ?>
                                    <option <?=$select?> value='A{{$i}}'>A{{$i}}</option>
                                    <?php endfor;?>

                                    <?php for($i = 0;$i <= 10;$i++):
                                    $select = ($setting->default_paper_size == 'B' . $i) ? "selected" : "";
                                    ?>
                                    <option <?=$select?> value='B{{$i}}'>B{{$i}}</option>
                                    <?php endfor;?>
                                </select>
                                <div class='help-block'><input type='checkbox' name='default_paper_size'
                                                               value='1'/> {{trans("crudbooster.export_dialog_set_default")}}
                                </div>
                            </div>

                            <div class="form-group">
                                <label>{{trans("crudbooster.export_dialog_page_orientation")}}</label>
                                <select class='form-control' name='page_orientation'>
                                    <option value='potrait'>Potrait</option>
                                    <option value='landscape'>Landscape</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer" align="right">
                        <button class="btn btn-default" type="button"
                                data-dismiss="modal">{{trans("crudbooster.button_close")}}</button>
                        <button class="btn btn-primary btn-submit"
                                type="submit">{{trans('crudbooster.button_submit')}}</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>

    <div class="box">
        <div class="box-header">
		    @include("order.index_top")
        </div>
	
        <div class="box-body table-responsive">
	        @include("order.table")
        </div>
    </div>

    @if(!is_null($post_index_html) && !empty($post_index_html))
        {!! $post_index_html !!}
    @endif

@endsection
@push('bottom')
    <script>
        $(document).ready(function () {
            $('.btn-export-data').click(function () {
                $('#export-data').modal('show');
            });
        });
    </script>
@endpush
