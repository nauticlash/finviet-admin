
<?php

return [
    'rider_on_map' => 'Online Rider',
    'filter_by_merchant' => 'Lọc theo nhánh Partner',
    'info' => 'Rider\'s Info',
    'send_notification' => 'Send notification',
    'online_status' => 'Online status',
    'last_update_gps' => 'Last update GPS',
    'registered_on' => "Register Date",
    'verify_otp' => "Verify OTP",
    'driver_name' => 'Driver name',
    'driver_phone' => 'Phone',
    'driver_total_delivery_money' => 'Total Delivery Money',
    'driver_total_km' => 'Total km',
    'driver_total_success_order' => 'Total success orders',
    'driver_total_failed_order' => 'Total failed orders',
];
