<?php

return [
    'start_day' => 'From',
    'end_day' => 'To',
    'column_time' => 'Date',
    'order_status_new' => 'New',
    'order_status_delivering' => 'Delivering',
    'order_status_delivered' => 'Delivered',
    'order_status_canceled' => 'Canceled',
    'orders_report_by_status' => 'Report by Order Status'
];
