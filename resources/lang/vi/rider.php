
<?php

return [
    'rider_on_map' => 'Vị Trí Trực Tuyến',
    'filter_by_merchant' => 'Lọc theo nhánh Partner',
    'info' => 'Thông tin lái xe',
    'send_notification' => 'Gửi thông báo',
    'online_status' => 'Trạng thái trực tuyến',
    'last_update_gps' => 'Cập nhật định vị lần cuối',
    'registered_on' => "Ngày đăng ký",
    'verify_otp' => "Xác thực OTP",
    'driver_name' => 'Tên tài xế',
    'driver_phone' => 'Điện thoại',
    'driver_total_delivery_money' => 'Tổng tiền giao hàng',
    'driver_total_km' => 'Tổng km',
    'driver_total_success_order' => 'Số đơn giao thành công',
    'driver_total_failed_order' => 'Số đơn giao thất bại',

];
