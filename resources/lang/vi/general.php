
<?php

return [
    'fullname' => 'Tên: ',
    'phone' => 'Số ĐT: ',
    'more_than_days' => 'Hơn :value ngày trước',
    'more_than_hours' => 'Hơn :value giờ trước',
    'more_than_minutes' => 'Hơn :value phút trước',
    'more_than_seconds' => 'Hơn :value giây trước',
];
