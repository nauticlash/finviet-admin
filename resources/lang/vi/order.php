<?php

return [
    'partner' => 'Đối tác',
    'filter_by_merchant' => 'Lọc theo đối tác',
    'order_id' => 'Mã ĐH',
    'created_at' => 'Thời gian đặt',
    'status_allocating' => 'Đang tìm lái xe',
    'status_allocating_info' => 'Hơn 1 phút chưa có lái xe nhận',
    'status_allocating_warning' => 'Hơn 2 phút chưa có lái xe nhận',
    'status_allocating_danger' => 'Hơn 3 phút chưa có lái xe nhận',
    'status_accepted' => 'Đã có tài xế',
    'status_accepted_info' => 'Hơn 5 phút lái xe chưa bắt đầu',
    'status_accepted_warning' => 'Hơn 10 phút lái xe chưa bắt đầu',
    'status_accepted_danger' => 'Hơn 15 phút lái xe chưa bắt đầu',
    'status_starting' => 'Chuẩn bị đơn hàng',
    'status_starting_info' => 'Hơn 5 phút lái xe chưa bắt đầu',
    'status_starting_warning' => 'Hơn 10 phút lái xe chưa bắt đầu',
    'status_starting_danger' => 'Hơn 15 phút lái xe chưa bắt đầu',
    'status_started' => 'Bắt đầu đơn hàng',
    'status_picking_up' => 'Đang lấy hàng',
    'status_picked_up' => 'Đã lấy hàng',
    'status_picking_up_info' => 'Hơn 10 phút lái xe chưa lấy hàng',
    'status_picking_up_warning' => 'Hơn 15 phút lái xe chưa lấy hàng',
    'status_picking_up_danger' => 'Hơn 20 phút lái xe chưa lấy hàng',
    'status_delivering' => 'Đang giao hàng',
    'status_delivering_info' => 'Hơn 10 phút lái xe chưa giao hàng',
    'status_delivering_warning' => 'Hơn 15 phút lái xe chưa giao hàng',
    'status_delivering_danger' => 'Hơn 20 phút lái xe chưa giao hàng',
    'status_delivered' => 'Đã giao hàng',
    'status_completing' => 'Kết thúc đơn hàng',
    'status_completed' => 'Hoàn thành',
    'status_canceled' => 'Đã hủy',
    'milestone' => 'Lịch sử đơn hàng',
    'info' => 'Thông tin đơn hàng',
    'progress' => 'Tiến trình đơn hàng',
    'create_order' => 'Tạo đơn hàng',
    'create_order_success' => 'Tạo đơn hàng thành công',
    'riders_receive_order' => 'Danh sách lái xe thấy đơn hàng',
    'rider_accepted_order' => 'Lái xe đã nhận đơn hàng',
    'rider_starting_order' => 'Lái xe đang chuẩn bị',
    'rider_started_order' => 'Lái xe đã bắt đầu',
    'rider_picking_up_order' => 'Lái xe đang lấy hàng',
    'rider_picked_up_order' => 'Lái xe đã lấy hàng',
    'rider_delivering_order' => 'Lái xe đang giao hàng',
    'rider_delivered_order' => 'Lái xe đã giao hàng',
    'rider_completing_order' => 'Chờ lái xe xác nhận hoàn thành đơn hàng',
    'rider_completed_order' => 'Lái xe đã hoàn thành đơn hàng',
    'canceled' => 'Đơn hàng đã bị hủy.',
    'canceled_by_admin' => 'Đơn hàng bị admin hủy.',
    'canceled_by_rider' => 'Đơn hàng bị lái xe hủy.',
    'canceled_by_partner' => 'Đơn hàng bị hủy từ phía đối tác.',
    'button_cancel' => 'Huỷ đơn',
    're_find_riders' => 'Tìm lại tài xế',
    'return_to_partner' => 'Trả về đối tác',
    'order_delivered' => 'Đã giao hàng cho khách.',
    'order_returning' => 'Khách không nhận hàng.',
    'complete_order' => 'Kết thúc đơn hàng',
    'distance' => 'Khoảng cách',
    'shipping_amount' => 'Tiền vận chuyển',
    'status_returning' => 'Không giao được',
    'rider_returning_order' => 'Không giao được cho khách',
];
