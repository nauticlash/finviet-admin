<?php

return [
    'start_day' => 'Từ ngày',
    'end_day' => 'Đến ngày',
    'column_time' => 'Thời Gian',
    'order_status_new' => 'Mới Tạo',
    'order_status_delivering' => 'Đang Giao',
    'order_status_delivered' => 'Đã Giao',
    'order_status_canceled' => 'Đã Hủy',
    'orders_report_by_status' => 'Thống kê theo trạng thái đơn hàng'
];
