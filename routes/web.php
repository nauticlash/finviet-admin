<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

//Route::get('language/{lang}', 'MyCBController@language')->where('lang', '[A-Za-z_-]+');
Route::get('language/{lang}', function ($locale) {
  Session::put('locale', $locale);
  return redirect()->back();
});

Route::get('/', function () {
  return view('welcome');
});
Route::get('/delivery', function () {
  return view('delivery');
});


// Customized crudbooster's route with your controller
Route::group(['prefix' => 'admin'], function ($routes) {
  $routes->group(['prefix' => 'privileges'], function ($route) {
    $route->get('add', 'MyPrivilegesController@getAdd');
    $route->post('add', 'MyPrivilegesController@postAdd');
    $route->get('edit/{id}', 'MyPrivilegesController@getEdit');
    $route->post('edit/{id}', 'MyPrivilegesController@postEdit');
    $route->get('delete/{id}', 'MyPrivilegesController@getDelete');
  });
});
