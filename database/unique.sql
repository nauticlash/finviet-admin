ALTER TABLE `fvexpress`.`cms_privileges_roles` 
ADD UNIQUE INDEX `unique_moduls_and_roles`(`id_cms_privileges`, `id_cms_moduls`);

ALTER TABLE `fvexpress`.`cms_menus_privileges` 
ADD UNIQUE INDEX `unique_menu_and_roles`(`id_cms_menus`, `id_cms_privileges`);


