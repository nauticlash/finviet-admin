<?php

namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use CRUDBooster;
use CB;

class AdminReportController extends MyCBController {

    public function cbInit() {

	# START CONFIGURATION DO NOT REMOVE THIS LINE
	$this->title_field = "sender_name";
	$this->limit = "20";
	$this->orderby = "id,desc";
	$this->global_privilege = false;
	$this->button_table_action = FALSE;
	$this->button_bulk_action = FALSE;
	$this->button_action_style = "button_icon";
	$this->button_add = false;
	$this->button_edit = false;
	$this->button_delete = false;
	$this->button_detail = false;
	$this->button_show = false;
	$this->button_filter = false;
	$this->button_import = false;
	$this->button_export = true;
	$this->table = "order_master";

	// vinhth add code
	$this->view_index_top = "report.index_top";

	# END CONFIGURATION DO NOT REMOVE THIS LINE
	# START COLUMNS DO NOT REMOVE THIS LINE
	# trans("crudbooster.field_created_at")
	$this->col = [];
	$this->col[] = ["label" =>trans("crudbooster.field_orderid"), "name" => "merchant_order_id"];
	$this->col[] = ["label" =>trans("crudbooster.field_order_creat_date"), "name" => "orderdate"];
	$this->col[] = [
	    "label" =>trans("crudbooster.field_firstname"),
	    "name" => "(SELECT CONCAT( um.firstname ,' ',um.lastname )
	    FROM user_master um where um.userid = order_master.rider_id) as fullname"
	];

	$this->col[] = [
	    "label" =>trans("crudbooster.field_phone_number") ,
	    "name" => "(SELECT um.contactno
	    FROM user_master um where um.userid = order_master.rider_id) as ridercontactno"
	];

	$this->col[] = ["label" =>trans("crudbooster.field_pickup_address"), "name" => "pickup_address"];
	$this->col[] = ["label" =>trans("crudbooster.field_delivery_address"), "name" => "delivery_address"];
	$this->col[] = ["label" =>trans("crudbooster.field_status"), "name" => "status"];
	$this->col[] = ["label" =>trans("crudbooster.field_money"), "name" => "order_total", "callback_php" => 'get_format_money_usd($row->order_total);'];
	$this->col[] = ["label" =>trans("crudbooster.field_money_rider"), "name" => "total_amount", "callback_php" => 'get_format_money_usd($row->total_amount);'];
	$this->col[] = ["label" =>trans("crudbooster.field_order_money"), "name" => "total_amount", "callback_php" => 'order_total_sum_total_amount($row->order_total, $row->total_amount);'];

	# END COLUMNS DO NOT REMOVE THIS LINE
	# START FORM DO NOT REMOVE THIS LINE
	$this->form   = [];
	$this->form[] = ['label' => 'Orderid', 'name' => 'orderid', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Userid', 'name' => 'userid', 'type' => 'number', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Rider Id', 'name' => 'rider_id', 'type' => 'select2', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10', 'datatable' => 'rider,id'];
	$this->form[] = ['label' => 'Ordertype', 'name' => 'ordertype', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'User Cash Payment', 'name' => 'user_cash_payment', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Other Amount', 'name' => 'other_amount', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Cash Payment', 'name' => 'cash_payment', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Billamount', 'name' => 'billamount', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Change Received', 'name' => 'change_received', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Waiting Charge', 'name' => 'waiting_charge', 'type' => 'money', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Wallet Amount', 'name' => 'wallet_amount', 'type' => 'money', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Commission', 'name' => 'commission', 'type' => 'money', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Commission Percentage', 'name' => 'commission_percentage', 'type' => 'money', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Changestatus', 'name' => 'changestatus', 'type' => 'number', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Tips', 'name' => 'tips', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Distance', 'name' => 'distance', 'type' => 'number', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Total Amount', 'name' => 'total_amount', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Pickup Address', 'name' => 'pickup_address', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Plat', 'name' => 'plat', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Plng', 'name' => 'plng', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Destination Address', 'name' => 'destination_address', 'type' => 'textarea', 'validation' => 'required|string|min:5|max:5000', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Dlat', 'name' => 'dlat', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Dlng', 'name' => 'dlng', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Delivery Address', 'name' => 'delivery_address', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Dllat', 'name' => 'dllat', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Dllng', 'name' => 'dllng', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Delivery Type', 'name' => 'delivery_type', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Description', 'name' => 'description', 'type' => 'textarea', 'validation' => 'required|string|min:5|max:5000', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Images', 'name' => 'images', 'type' => 'textarea', 'validation' => 'required|string|min:5|max:5000', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Promocode', 'name' => 'promocode', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Promoamount', 'name' => 'promoamount', 'type' => 'money', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Sender Name', 'name' => 'sender_name', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Recipient Name', 'name' => 'recipient_name', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Courier Service Name', 'name' => 'courier_service_name', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Status', 'name' => 'status', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Riderstatus', 'name' => 'riderstatus', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Is Rain', 'name' => 'is_rain', 'type' => 'radio', 'validation' => 'required|integer', 'width' => 'col-sm-10', 'dataenum' => 'Array'];
	$this->form[] = ['label' => 'No Of Km', 'name' => 'no_of_km', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Pickup Time', 'name' => 'pickup_time', 'type' => 'datetime', 'validation' => 'required|date_format:Y-m-d H:i:s', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Arrive Pickup Time', 'name' => 'arrive_pickup_time', 'type' => 'datetime', 'validation' => 'required|date_format:Y-m-d H:i:s', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Is Pickup', 'name' => 'is_pickup', 'type' => 'radio', 'validation' => 'required|integer', 'width' => 'col-sm-10', 'dataenum' => 'Array'];
	$this->form[] = ['label' => 'Is At Pickup', 'name' => 'is_at_pickup', 'type' => 'radio', 'validation' => 'required|integer', 'width' => 'col-sm-10', 'dataenum' => 'Array'];
	$this->form[] = ['label' => 'Is Paid', 'name' => 'is_paid', 'type' => 'radio', 'validation' => 'required|integer', 'width' => 'col-sm-10', 'dataenum' => 'Array'];
	$this->form[] = ['label' => 'Is Reached Destination', 'name' => 'is_reached_destination', 'type' => 'radio', 'validation' => 'required|integer', 'width' => 'col-sm-10', 'dataenum' => 'Array'];
	$this->form[] = ['label' => 'Reached Destination Time', 'name' => 'reached_destination_time', 'type' => 'datetime', 'validation' => 'required|date_format:Y-m-d H:i:s', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'At Destination', 'name' => 'at_destination', 'type' => 'datetime', 'validation' => 'required|date_format:Y-m-d H:i:s', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Is At Destination', 'name' => 'is_at_destination', 'type' => 'radio', 'validation' => 'required|integer', 'width' => 'col-sm-10', 'dataenum' => 'Array'];
	$this->form[] = ['label' => 'Dropoff Time', 'name' => 'dropoff_time', 'type' => 'datetime', 'validation' => 'required|date_format:Y-m-d H:i:s', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Is Dropoff', 'name' => 'is_dropoff', 'type' => 'radio', 'validation' => 'required|integer', 'width' => 'col-sm-10', 'dataenum' => 'Array'];
	$this->form[] = ['label' => 'Is At Dropoff', 'name' => 'is_at_dropoff', 'type' => 'radio', 'validation' => 'required|integer', 'width' => 'col-sm-10', 'dataenum' => 'Array'];
	$this->form[] = ['label' => 'At Dropoff Time', 'name' => 'at_dropoff_time', 'type' => 'datetime', 'validation' => 'required|date_format:Y-m-d H:i:s', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Receipt', 'name' => 'receipt', 'type' => 'textarea', 'validation' => 'required|string|min:5|max:5000', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Signature Image', 'name' => 'signature_image', 'type' => 'textarea', 'validation' => 'required|string|min:5|max:5000', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Orderdate', 'name' => 'orderdate', 'type' => 'datetime', 'validation' => 'required|date_format:Y-m-d H:i:s', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Admincancel', 'name' => 'admincancel', 'type' => 'textarea', 'validation' => 'required|string|min:5|max:5000', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Platform', 'name' => 'platform', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Adminedit', 'name' => 'adminedit', 'type' => 'number', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Cancelby', 'name' => 'cancelby', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Is Deleted', 'name' => 'is_deleted', 'type' => 'radio', 'validation' => 'required|integer', 'width' => 'col-sm-10', 'dataenum' => 'Array'];
	$this->form[] = ['label' => 'User Deleted', 'name' => 'user_deleted', 'type' => 'number', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Rider Deleted', 'name' => 'rider_deleted', 'type' => 'number', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Delivery Time', 'name' => 'delivery_time', 'type' => 'datetime', 'validation' => 'required|date_format:Y-m-d H:i:s', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Modifieddate', 'name' => 'modifieddate', 'type' => 'datetime', 'validation' => 'required|date_format:Y-m-d H:i:s', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Pickup City', 'name' => 'pickup_city', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Pickup State', 'name' => 'pickup_state', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Pickup Country', 'name' => 'pickup_country', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Pickup Postal', 'name' => 'pickup_postal', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Drop City', 'name' => 'drop_city', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Drop State', 'name' => 'drop_state', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Drop Country', 'name' => 'drop_country', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Drop Postal', 'name' => 'drop_postal', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Order Items', 'name' => 'order_items', 'type' => 'textarea', 'validation' => 'required|string|min:5|max:5000', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Extra', 'name' => 'extra', 'type' => 'textarea', 'validation' => 'required|string|min:5|max:5000', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Merchant Order Id', 'name' => 'merchant_order_id', 'type' => 'select2', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10', 'datatable' => 'merchant_order,id'];
	$this->form[] = ['label' => 'Merchant Contact', 'name' => 'merchant_contact', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Order Total', 'name' => 'order_total', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Customer Contact', 'name' => 'customer_contact', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Customer Contact 2', 'name' => 'customer_contact_2', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Cancel Reason', 'name' => 'cancel_reason', 'type' => 'textarea', 'validation' => 'required|string|min:5|max:5000', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Alert Pending', 'name' => 'alert_pending', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Alert Pickup', 'name' => 'alert_pickup', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Alert Dropoff', 'name' => 'alert_dropoff', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Last Alert Pending', 'name' => 'last_alert_pending', 'type' => 'datetime', 'validation' => 'required|date_format:Y-m-d H:i:s', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Last Alert Pickup', 'name' => 'last_alert_pickup', 'type' => 'datetime', 'validation' => 'required|date_format:Y-m-d H:i:s', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Last Alert Dropoff', 'name' => 'last_alert_dropoff', 'type' => 'datetime', 'validation' => 'required|date_format:Y-m-d H:i:s', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Created By', 'name' => 'created_by', 'type' => 'number', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Updated By', 'name' => 'updated_by', 'type' => 'number', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10'];
	# END FORM DO NOT REMOVE THIS LINE
	# OLD START FORM
	//$this->form = [];
	//$this->form[] = ["label"=>"Orderid","name"=>"orderid","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Userid","name"=>"userid","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
	//$this->form[] = ["label"=>"Rider Id","name"=>"rider_id","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"rider,id"];
	//$this->form[] = ["label"=>"Ordertype","name"=>"ordertype","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"User Cash Payment","name"=>"user_cash_payment","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Other Amount","name"=>"other_amount","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Cash Payment","name"=>"cash_payment","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Billamount","name"=>"billamount","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Change Received","name"=>"change_received","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Waiting Charge","name"=>"waiting_charge","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
	//$this->form[] = ["label"=>"Wallet Amount","name"=>"wallet_amount","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
	//$this->form[] = ["label"=>"Commission","name"=>"commission","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
	//$this->form[] = ["label"=>"Commission Percentage","name"=>"commission_percentage","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
	//$this->form[] = ["label"=>"Changestatus","name"=>"changestatus","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
	//$this->form[] = ["label"=>"Tips","name"=>"tips","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Distance","name"=>"distance","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
	//$this->form[] = ["label"=>"Total Amount","name"=>"total_amount","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Pickup Address","name"=>"pickup_address","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Plat","name"=>"plat","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Plng","name"=>"plng","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Destination Address","name"=>"destination_address","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
	//$this->form[] = ["label"=>"Dlat","name"=>"dlat","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Dlng","name"=>"dlng","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Delivery Address","name"=>"delivery_address","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Dllat","name"=>"dllat","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Dllng","name"=>"dllng","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Delivery Type","name"=>"delivery_type","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Description","name"=>"description","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
	//$this->form[] = ["label"=>"Images","name"=>"images","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
	//$this->form[] = ["label"=>"Promocode","name"=>"promocode","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Promoamount","name"=>"promoamount","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
	//$this->form[] = ["label"=>"Sender Name","name"=>"sender_name","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Recipient Name","name"=>"recipient_name","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Courier Service Name","name"=>"courier_service_name","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Status","name"=>"status","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Riderstatus","name"=>"riderstatus","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Is Rain","name"=>"is_rain","type"=>"radio","required"=>TRUE,"validation"=>"required|integer","dataenum"=>"Array"];
	//$this->form[] = ["label"=>"No Of Km","name"=>"no_of_km","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Pickup Time","name"=>"pickup_time","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
	//$this->form[] = ["label"=>"Arrive Pickup Time","name"=>"arrive_pickup_time","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
	//$this->form[] = ["label"=>"Is Pickup","name"=>"is_pickup","type"=>"radio","required"=>TRUE,"validation"=>"required|integer","dataenum"=>"Array"];
	//$this->form[] = ["label"=>"Is At Pickup","name"=>"is_at_pickup","type"=>"radio","required"=>TRUE,"validation"=>"required|integer","dataenum"=>"Array"];
	//$this->form[] = ["label"=>"Is Paid","name"=>"is_paid","type"=>"radio","required"=>TRUE,"validation"=>"required|integer","dataenum"=>"Array"];
	//$this->form[] = ["label"=>"Is Reached Destination","name"=>"is_reached_destination","type"=>"radio","required"=>TRUE,"validation"=>"required|integer","dataenum"=>"Array"];
	//$this->form[] = ["label"=>"Reached Destination Time","name"=>"reached_destination_time","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
	//$this->form[] = ["label"=>"At Destination","name"=>"at_destination","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
	//$this->form[] = ["label"=>"Is At Destination","name"=>"is_at_destination","type"=>"radio","required"=>TRUE,"validation"=>"required|integer","dataenum"=>"Array"];
	//$this->form[] = ["label"=>"Dropoff Time","name"=>"dropoff_time","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
	//$this->form[] = ["label"=>"Is Dropoff","name"=>"is_dropoff","type"=>"radio","required"=>TRUE,"validation"=>"required|integer","dataenum"=>"Array"];
	//$this->form[] = ["label"=>"Is At Dropoff","name"=>"is_at_dropoff","type"=>"radio","required"=>TRUE,"validation"=>"required|integer","dataenum"=>"Array"];
	//$this->form[] = ["label"=>"At Dropoff Time","name"=>"at_dropoff_time","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
	//$this->form[] = ["label"=>"Receipt","name"=>"receipt","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
	//$this->form[] = ["label"=>"Signature Image","name"=>"signature_image","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
	//$this->form[] = ["label"=>"Orderdate","name"=>"orderdate","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
	//$this->form[] = ["label"=>"Admincancel","name"=>"admincancel","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
	//$this->form[] = ["label"=>"Platform","name"=>"platform","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Adminedit","name"=>"adminedit","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
	//$this->form[] = ["label"=>"Cancelby","name"=>"cancelby","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Is Deleted","name"=>"is_deleted","type"=>"radio","required"=>TRUE,"validation"=>"required|integer","dataenum"=>"Array"];
	//$this->form[] = ["label"=>"User Deleted","name"=>"user_deleted","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
	//$this->form[] = ["label"=>"Rider Deleted","name"=>"rider_deleted","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
	//$this->form[] = ["label"=>"Delivery Time","name"=>"delivery_time","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
	//$this->form[] = ["label"=>"Modifieddate","name"=>"modifieddate","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
	//$this->form[] = ["label"=>"Pickup City","name"=>"pickup_city","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Pickup State","name"=>"pickup_state","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Pickup Country","name"=>"pickup_country","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Pickup Postal","name"=>"pickup_postal","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Drop City","name"=>"drop_city","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Drop State","name"=>"drop_state","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Drop Country","name"=>"drop_country","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Drop Postal","name"=>"drop_postal","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Order Items","name"=>"order_items","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
	//$this->form[] = ["label"=>"Extra","name"=>"extra","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
	//$this->form[] = ["label"=>"Merchant Order Id","name"=>"merchant_order_id","type"=>"select2","required"=>TRUE,"validation"=>"required|min:1|max:255","datatable"=>"merchant_order,id"];
	//$this->form[] = ["label"=>"Merchant Contact","name"=>"merchant_contact","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Order Total","name"=>"order_total","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Customer Contact","name"=>"customer_contact","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Customer Contact 2","name"=>"customer_contact_2","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Cancel Reason","name"=>"cancel_reason","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
	//$this->form[] = ["label"=>"Alert Pending","name"=>"alert_pending","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Alert Pickup","name"=>"alert_pickup","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Alert Dropoff","name"=>"alert_dropoff","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Last Alert Pending","name"=>"last_alert_pending","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
	//$this->form[] = ["label"=>"Last Alert Pickup","name"=>"last_alert_pickup","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
	//$this->form[] = ["label"=>"Last Alert Dropoff","name"=>"last_alert_dropoff","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
	//$this->form[] = ["label"=>"Created By","name"=>"created_by","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
	//$this->form[] = ["label"=>"Updated By","name"=>"updated_by","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
	# OLD END FORM

	/*
	  | ----------------------------------------------------------------------
	  | Sub Module
	  | ----------------------------------------------------------------------
	  | @label          = Label of action
	  | @path           = Path of sub module
	  | @foreign_key 	  = foreign key of sub table/module
	  | @button_color   = Bootstrap Class (primary,success,warning,danger)
	  | @button_icon    = Font Awesome Class
	  | @parent_columns = Sparate with comma, e.g : name,created_at
	  |
	 */
	$this->sub_module = array();


	/*
	  | ----------------------------------------------------------------------
	  | Add More Action Button / Menu
	  | ----------------------------------------------------------------------
	  | @label       = Label of action
	  | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	  | @icon        = Font awesome class icon. e.g : fa fa-bars
	  | @color 	   = Default is primary. (primary, warning, succecss, info)
	  | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	  |
	 */
	$this->addaction = array();


	/*
	  | ----------------------------------------------------------------------
	  | Add More Button Selected
	  | ----------------------------------------------------------------------
	  | @label       = Label of action
	  | @icon 	   = Icon from fontawesome
	  | @name 	   = Name of button
	  | Then about the action, you should code at actionButtonSelected method
	  |
	 */
	$this->button_selected = array();


	/*
	  | ----------------------------------------------------------------------
	  | Add alert message to this module at overheader
	  | ----------------------------------------------------------------------
	  | @message = Text of message
	  | @type    = warning,success,danger,info
	  |
	 */
	$this->alert = array();



	/*
	  | ----------------------------------------------------------------------
	  | Add more button to header button
	  | ----------------------------------------------------------------------
	  | @label = Name of button
	  | @url   = URL Target
	  | @icon  = Icon from Awesome.
	  |
	 */
	$this->index_button = array();



	/*
	  | ----------------------------------------------------------------------
	  | Customize Table Row Color
	  | ----------------------------------------------------------------------
	  | @condition = If condition. You may use field alias. E.g : [id] == 1
	  | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
	  |
	 */
	$this->table_row_color = array();


	/*
	  | ----------------------------------------------------------------------
	  | You may use this bellow array to add statistic at dashboard
	  | ----------------------------------------------------------------------
	  | @label, @count, @icon, @color
	  |
	 */
	$this->index_statistic = array();



	/*
	  | ----------------------------------------------------------------------
	  | Add javascript at body
	  | ----------------------------------------------------------------------
	  | javascript code in the variable
	  | $this->script_js = "function() { ... }";
	  |
	 */
	$this->script_js = NULL;
	$this->script_js = "$(function () {
	    $('#select_merchants_id').select2();
	});";

	/*
	  | ----------------------------------------------------------------------
	  | Include HTML Code before index table
	  | ----------------------------------------------------------------------
	  | html code to display it before index table
	  | $this->pre_index_html = "<p>test</p>";
	  |
	 */
	$this->pre_index_html = null;



	/*
	  | ----------------------------------------------------------------------
	  | Include HTML Code after index table
	  | ----------------------------------------------------------------------
	  | html code to display it after index table
	  | $this->post_index_html = "<p>test</p>";
	  |
	 */
	$this->post_index_html = null;



	/*
	  | ----------------------------------------------------------------------
	  | Include Javascript File
	  | ----------------------------------------------------------------------
	  | URL of your javascript each array
	  | $this->load_js[] = asset("myfile.js");
	  |
	 */
	$this->load_js = array();



	/*
	  | ----------------------------------------------------------------------
	  | Add css style at body
	  | ----------------------------------------------------------------------
	  | css code in the variable
	  | $this->style_css = ".style{....}";
	  |
	 */
	$this->style_css = NULL;



	/*
	  | ----------------------------------------------------------------------
	  | Include css File
	  | ----------------------------------------------------------------------
	  | URL of your css each array
	  | $this->load_css[] = asset("myfile.css");
	  |
	 */
	$this->load_css = array();
    }

    public function getIndex() {
	$this->cbLoader();

	$module = CRUDBooster::getCurrentModule();

	if (!CRUDBooster::isView() && $this->global_privilege == false) {
	    CRUDBooster::insertLog(trans('crudbooster.log_try_view', ['module' => $module->name]));
	    CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));
	}

	if (Request::get('parent_table')) {
	    $parentTablePK = CB::pk(g('parent_table'));
	    $data['parent_table'] = DB::table(Request::get('parent_table'))->where($parentTablePK, Request::get('parent_id'))->first();
	    if (Request::get('foreign_key')) {
		$data['parent_field'] = Request::get('foreign_key');
	    } else {
		$data['parent_field'] = CB::getTableForeignKey(g('parent_table'), $this->table);
	    }

	    if ($parent_field) {
		foreach ($this->columns_table as $i => $col) {
		    if ($col['name'] == $parent_field) {
			unset($this->columns_table[$i]);
		    }
		}
	    }
	}

	$data['table'] = $this->table;
	$data['table_pk'] = CB::pk($this->table);
//	$data['page_title'] = $module->name;
	$data['page_title'] = ($module->translate_code) ? trans($module->translate_code) : $module->name;
	$data['page_description'] = trans('crudbooster.default_module_description');
	$data['date_candidate'] = $this->date_candidate;
	$data['limit'] = $limit = (Request::get('limit')) ? Request::get('limit') : $this->limit;

	$tablePK = $data['table_pk'];
	$table_columns = CB::getTableColumns($this->table);
	$result = DB::table($this->table)->select(DB::raw($this->table . "." . $this->primary_key));

	if (Request::get('parent_id')) {
	    $table_parent = $this->table;
	    $table_parent = CRUDBooster::parseSqlTable($table_parent)['table'];
	    $result->where($table_parent . '.' . Request::get('foreign_key'), Request::get('parent_id'));
	}

	$this->hook_query_index($result);

	if (in_array('deleted_at', $table_columns)) {
	    $result->where($this->table . '.deleted_at', null);
	}

	$alias = [];
	$join_alias_count = 0;
	$join_table_temp = [];
	$table = $this->table;
	$columns_table = $this->columns_table;
	foreach ($columns_table as $index => $coltab) {

	    $join = @$coltab['join'];
	    $join_where = @$coltab['join_where'];
	    $join_id = @$coltab['join_id'];
	    $field = @$coltab['name'];
	    $join_table_temp[] = $table;

	    if (!$field) {
		continue;
	    }

	    if (strpos($field, ' as ') !== false) {
		$field = substr($field, strpos($field, ' as ') + 4);
		$field_with = (array_key_exists('join', $coltab)) ? str_replace(",", ".", $coltab['join']) : $field;
		$result->addselect(DB::raw($coltab['name']));
		$columns_table[$index]['type_data'] = 'varchar';
		$columns_table[$index]['field'] = $field;
		$columns_table[$index]['field_raw'] = $field;
		$columns_table[$index]['field_with'] = $field_with;
		$columns_table[$index]['is_subquery'] = true;
		continue;
	    }

	    if (strpos($field, '.') !== false) {
		$result->addselect($field);
	    } else {
		$result->addselect($table . '.' . $field);
	    }

	    $field_array = explode('.', $field);

	    if (isset($field_array[1])) {
		$field = $field_array[1];
		$table = $field_array[0];
	    } else {
		$table = $this->table;
	    }

	    if ($join) {

		$join_exp = explode(',', $join);

		$join_table = $join_exp[0];
		$joinTablePK = CB::pk($join_table);
		$join_column = $join_exp[1];
		$join_alias = str_replace(".", "_", $join_table);

		if (in_array($join_table, $join_table_temp)) {
		    $join_alias_count += 1;
		    $join_alias = $join_table . $join_alias_count;
		}
		$join_table_temp[] = $join_table;

		$result->leftjoin($join_table . ' as ' . $join_alias, $join_alias . (($join_id) ? '.' . $join_id : '.' . $joinTablePK), '=', DB::raw($table . '.' . $field . (($join_where) ? ' AND ' . $join_where . ' ' : '')));
		$result->addselect($join_alias . '.' . $join_column . ' as ' . $join_alias . '_' . $join_column);

		$join_table_columns = CRUDBooster::getTableColumns($join_table);
		if ($join_table_columns) {
		    foreach ($join_table_columns as $jtc) {
			$result->addselect($join_alias . '.' . $jtc . ' as ' . $join_alias . '_' . $jtc);
		    }
		}

		$alias[] = $join_alias;
		$columns_table[$index]['type_data'] = CRUDBooster::getFieldType($join_table, $join_column);
		$columns_table[$index]['field'] = $join_alias . '_' . $join_column;
		$columns_table[$index]['field_with'] = $join_alias . '.' . $join_column;
		$columns_table[$index]['field_raw'] = $join_column;

		@$join_table1  = $join_exp[2];
		@$joinTable1PK = CB::pk($join_table1);
		@$join_column1 = $join_exp[3];
		@$join_alias1  = $join_table1;

		if ($join_table1 && $join_column1) {

		    if (in_array($join_table1, $join_table_temp)) {
			$join_alias_count += 1;
			$join_alias1 = $join_table1 . $join_alias_count;
		    }

		    $join_table_temp[] = $join_table1;

		    $result->leftjoin($join_table1 . ' as ' . $join_alias1, $join_alias1 . '.' . $joinTable1PK, '=', $join_alias . '.' . $join_column);
		    $result->addselect($join_alias1 . '.' . $join_column1 . ' as ' . $join_column1 . '_' . $join_alias1);
		    $alias[] = $join_alias1;
			$columns_table[$index]['type_data']  = CRUDBooster::getFieldType($join_table1, $join_column1);
			$columns_table[$index]['field']      = $join_column1 . '_' . $join_alias1;
			$columns_table[$index]['field_with'] = $join_alias1 . '.' . $join_column1;
			$columns_table[$index]['field_raw']  = $join_column1;
		}
	    } else {

		if (isset($field_array[1])) {
		    $result->addselect($table . '.' . $field . ' as ' . $table . '_' . $field);
			$columns_table[$index]['type_data'] = CRUDBooster::getFieldType($table, $field);
			$columns_table[$index]['field']     = $table . '_' . $field;
			$columns_table[$index]['field_raw'] = $table . '.' . $field;
		} else {
		    $result->addselect($table . '.' . $field);
			$columns_table[$index]['type_data'] = CRUDBooster::getFieldType($table, $field);
			$columns_table[$index]['field']     = $field;
			$columns_table[$index]['field_raw'] = $field;
		}

		$columns_table[$index]['field_with'] = $table . '.' . $field;
	    }
	}

	// vinhth add code =====================================================
	$select_merchants_id = Request::get('select_merchants_id');
	$select_start_date   = Request::get('select_start_date');
	$select_end_date     = Request::get('select_end_date');

	if (empty($select_start_date)) {
            $select_start_date = date("Y-m-01");
        }
	if (empty($select_end_date)) {
            $select_end_date = date("Y-m-d");
        }
	$start_date       = date_create($select_start_date);
	$end_date         = date_create($select_end_date);
	$where_start_date = date_format($start_date, "Y-m-d");
	$where_end_date   = date_format($end_date, "Y-m-d");

	if (isset($select_merchants_id) && !empty($select_merchants_id)) {
	    $result->where($this->table . '.userid', $select_merchants_id);
	    $data['select_merchants_id'] = $select_merchants_id;
	}

	if (!empty($select_start_date)) {
	    $result->where($this->table . '.orderdate', '>',  $where_start_date);
	    $data['select_start_date'] = date_format($start_date, "d-m-Y");
        }

	if (!empty($select_end_date)) {
	    $result->where($this->table . '.orderdate', '<=', $where_end_date );
	    $data['select_end_date'] = date_format($end_date, "d-m-Y");
        }

	$data['merchants'] = DB::table('merchants')->whereNull('deleted_at')->get();

//	echo "<pre>"; print_r($data['array_status']);die();

	$data['view_index_top'] = $this->view_index_top;
	// end vinhth add code =================================================


	if (Request::get('q')) {
	    $result->where(function ($w) use ($columns_table, $request) {
		foreach ($columns_table as $col) {
		    if (!$col['field_with']) {
			continue;
		    }
		    if ($col['is_subquery']) {
			continue;
		    }
		    $w->orwhere($col['field_with'], "like", "%" . Request::get("q") . "%");
		}
	    });
	}

	if (Request::get('where')) {
	    foreach (Request::get('where') as $k => $v) {
		$result->where($table . '.' . $k, $v);
	    }
	}

	$filter_is_orderby = false;
	if (Request::get('filter_column')) {

	    $filter_column = Request::get('filter_column');
	    $result->where(function ($w) use ($filter_column, $fc) {
		foreach ($filter_column as $key => $fc) {

		    $value = @$fc['value'];
		    $type = @$fc['type'];

		    if ($type == 'empty') {
			$w->whereNull($key)->orWhere($key, '');
			continue;
		    }

		    if ($value == '' || $type == '') {
			continue;
		    }

		    if ($type == 'between') {
			continue;
		    }

		    switch ($type) {
			default:
			    if ($key && $type && $value) {
				$w->where($key, $type, $value);
			    }
			    break;
			case 'like':
			case 'not like':
			    $value = '%' . $value . '%';
			    if ($key && $type && $value) {
				$w->where($key, $type, $value);
			    }
			    break;
			case 'in':
			case 'not in':
			    if ($value) {
				$value = explode(',', $value);
				if ($key && $value) {
				    $w->whereIn($key, $value);
				}
			    }
			    break;
		    }
		}
	    });

	    foreach ($filter_column as $key => $fc) {
		$value = @$fc['value'];
		$type = @$fc['type'];
		$sorting = @$fc['sorting'];

		if ($sorting != '') {
		    if ($key) {
			$result->orderby($key, $sorting);
			$filter_is_orderby = true;
		    }
		}

		if ($type == 'between') {
		    if ($key && $value) {
			$result->whereBetween($key, $value);
		    }
		} else {
		    continue;
		}
	    }
	}

	if ($filter_is_orderby == true) {
	    $data['result'] = $result->paginate($limit);
	} else {
	    if ($this->orderby) {
		if (is_array($this->orderby)) {
		    foreach ($this->orderby as $k => $v) {
			if (strpos($k, '.') !== false) {
			    $orderby_table = explode(".", $k)[0];
			    $k = explode(".", $k)[1];
			} else {
			    $orderby_table = $this->table;
			}
			$result->orderby($orderby_table . '.' . $k, $v);
		    }
		} else {
		    $this->orderby = explode(";", $this->orderby);
		    foreach ($this->orderby as $o) {
			$o = explode(",", $o);
			$k = $o[0];
			$v = $o[1];
			if (strpos($k, '.') !== false) {
			    $orderby_table = explode(".", $k)[0];
			} else {
			    $orderby_table = $this->table;
			}
			$result->orderby($orderby_table . '.' . $k, $v);
		    }
		}
		$data['result'] = $result->paginate($limit);
	    } else {
		$data['result'] = $result->orderby($this->table . '.' . $this->primary_key, 'desc')->paginate($limit);
	    }
	}

	$data['columns'] = $columns_table;

	if ($this->index_return) {
	    return $data;
	}

	//LISTING INDEX HTML
	$addaction = $this->data['addaction'];

	if ($this->sub_module) {
	    foreach ($this->sub_module as $s) {
		$table_parent = CRUDBooster::parseSqlTable($this->table)['table'];
		$addaction[] = [
			'label'  => $s['label'],
			'icon'   => $s['button_icon'],
			'url'    => CRUDBooster::adminPath($s['path']) . '?return_url=' . urlencode(Request::fullUrl()) . '&parent_table=' . $table_parent . '&parent_columns=' . $s['parent_columns'] . '&parent_columns_alias=' . $s['parent_columns_alias'] . '&parent_id=[' . (!isset($s['custom_parent_id']) ? "id" : $s['custom_parent_id']) . ']&foreign_key=' . $s['foreign_key'] . '&label=' . urlencode($s['label']),
			'color'  => $s['button_color'],
			'showIf' => $s['showIf'],
		];
	    }
	}

	$mainpath = CRUDBooster::mainpath();
	$orig_mainpath = $this->data['mainpath'];
	$title_field   = $this->title_field;
	$html_contents = [];
	$page = (Request::get('page')) ? Request::get('page') : 1;
	$number = ($page - 1) * $limit + 1;
	foreach ($data['result'] as $row) {
	    $html_content = [];

	    if ($this->button_bulk_action) {

		$html_content[] = "<input type='checkbox' class='checkbox' name='checkbox[]' value='" . $row->{$tablePK} . "'/>";
	    }

	    if ($this->show_numbering) {
		$html_content[] = $number . '. ';
		$number++;
	    }

	    foreach ($columns_table as $col) {
		if ($col['visible'] === false) {
		    continue;
		}

		$value = @$row->{$col['field']};
		$title = @$row->{$this->title_field};
		$label = $col['label'];

		if (isset($col['image'])) {
		    if ($value == '') {
			$value = "<a  data-lightbox='roadtrip' rel='group_{{$table}}' title='$label: $title' href='" . asset('vendor/crudbooster/noimage.png') . "'><img width='40px' height='40px' src='" . asset('vendor/crudbooster/noimage.png') . "'/></a>";
		    } else {
			$pic = (strpos($value, 'http://') !== false) ? $value : asset($value);
			$value = "<a data-lightbox='roadtrip'  rel='group_{{$table}}' title='$label: $title' href='" . $pic . "'><img width='40px' height='40px' src='" . $pic . "'/></a>";
		    }
		}

		if (@$col['download']) {
		    $url = (strpos($value, 'http://') !== false) ? $value : asset($value) . '?download=1';
		    if ($value) {
			$value = "<a class='btn btn-xs btn-primary' href='$url' target='_blank' title='Download File'><i class='fa fa-download'></i> Download</a>";
		    } else {
			$value = " - ";
		    }
		}

		if ($col['str_limit']) {
		    $value = trim(strip_tags($value));
		    $value = str_limit($value, $col['str_limit']);
		}

		if ($col['nl2br']) {
		    $value = nl2br($value);
		}

		if ($col['callback_php']) {
		    foreach ($row as $k => $v) {
			$col['callback_php'] = str_replace("[" . $k . "]", $v, $col['callback_php']);
		    }
		    @eval("\$value = " . $col['callback_php'] . ";");
		}

		//New method for callback
		if (isset($col['callback'])) {
		    $value = call_user_func($col['callback'], $row);
		}

		$datavalue = @unserialize($value);
		if ($datavalue !== false) {
		    if ($datavalue) {
			$prevalue = [];
			foreach ($datavalue as $d) {
			    if ($d['label']) {
				$prevalue[] = $d['label'];
			    }
			}
			if ($prevalue && count($prevalue)) {
			    $value = implode(", ", $prevalue);
			}
		    }
		}

		$html_content[] = $value;
	    } //end foreach columns_table

	    if ($this->button_table_action):

		$button_action_style = $this->button_action_style;
		$html_content[] = "<div class='button_action' style='text-align:right'>" . view($this->view_action, compact('addaction', 'row', 'button_action_style', 'parent_field'))->render() . "</div>";
//		$html_content[] = "<div class='button_action' style='text-align:right'>" . view('crudbooster::components.action', compact('addaction', 'row', 'button_action_style', 'parent_field'))->render() . "</div>";

	    endif; //button_table_action

	    foreach ($html_content as $i => $v) {
		$this->hook_row_index($i, $v);
		$html_content[$i] = $v;
	    }

	    $html_contents[] = $html_content;
	} //end foreach data[result]

	$html_contents = ['html' => $html_contents, 'data' => $data['result']];

	$data['html_contents'] = $html_contents;

	return view($this->view_index, $data);
//	return view("crudbooster::default.index", $data);
    }

}
