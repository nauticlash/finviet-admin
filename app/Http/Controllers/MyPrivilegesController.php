<?php

namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\PrivilegesController;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

class MyPrivilegesController extends PrivilegesController
{
  public function getAdd()
  {
    $this->cbLoader();

    if (!CRUDBooster::isCreate() && $this->global_privilege == false) {
      CRUDBooster::insertLog(trans('crudbooster.log_try_add', ['module' => CRUDBooster::getCurrentModule()->name]));
      CRUDBooster::redirect(CRUDBooster::adminPath(), trans("crudbooster.denied_access"));
    }

    $id = 0;
    $data['page_title'] = "Add Data";
    $data['moduls'] = DB::table("cms_moduls")->where('is_protected', 0)->whereNull('deleted_at')->select(
      "cms_moduls.*",
      DB::raw("(select is_visible from cms_privileges_roles where id_cms_moduls = cms_moduls.id and id_cms_privileges = '$id') as is_visible"),
      DB::raw("(select is_create from cms_privileges_roles where id_cms_moduls  = cms_moduls.id and id_cms_privileges = '$id') as is_create"),
      DB::raw("(select is_read from cms_privileges_roles where id_cms_moduls    = cms_moduls.id and id_cms_privileges = '$id') as is_read"),
      DB::raw("(select is_edit from cms_privileges_roles where id_cms_moduls    = cms_moduls.id and id_cms_privileges = '$id') as is_edit"),
      DB::raw("(select is_delete from cms_privileges_roles where id_cms_moduls  = cms_moduls.id and id_cms_privileges = '$id') as is_delete")
    )->orderby("name", "asc")->get();
    $data['page_menu'] = Route::getCurrentRoute()->getActionName();
    return view('custom-privileges', $data);
  }

  public function postAdd()
  {
    $this->cbLoader();
    $isSuperAdmin = request('is_superadmin');
    if (!CRUDBooster::isCreate() && $this->global_privilege == false) {
      CRUDBooster::insertLog(trans('crudbooster.log_try_add_save', [
        'name' => Request::input($this->title_field),
        'module' => CRUDBooster::getCurrentModule()->name,
      ]));
      CRUDBooster::redirect(CRUDBooster::adminPath(), trans("crudbooster.denied_access"));
    }

    $this->validation($request);
    $this->input_assignment($request);

    $this->arr[$this->primary_key] = DB::table($this->table)->max($this->primary_key) + 1;

    DB::table($this->table)->insert($this->arr);
    $id = $this->arr[$this->primary_key];

    //set theme
    Session::put('theme_color', $this->arr['theme_color']);

    $priv = Request::input("privileges");
    if ($priv) {
      foreach ($priv as $id_modul => $data) {
        $arrs = [];
        $arrs['id'] = DB::table('cms_privileges_roles')->max('id') + 1;
        $arrs['is_visible'] = @$data['is_visible'] ?: 0;
        $arrs['is_create'] = @$data['is_create'] ?: 0;
        $arrs['is_read'] = @$data['is_read'] ?: 0;
        $arrs['is_edit'] = @$data['is_edit'] ?: 0;
        $arrs['is_delete'] = @$data['is_delete'] ?: 0;
        $arrs['id_cms_privileges'] = $id;
        $arrs['id_cms_moduls'] = $id_modul;
        DB::table("cms_privileges_roles")->insert($arrs);

        $module = DB::table('cms_moduls')->where('id', $id_modul)->first();
        $menus = DB::table('cms_menus')->where([['is_active', 1], ['path', 'like', $module->controller . "%"]])->first();
        if (!$menus) continue;

        $arrMenus = [];
        $arrMenus['id_cms_menus'] = $menus->id;
        $arrMenus['id_cms_privileges'] = $id;
        DB::table('cms_menus_privileges')->insert($arrMenus);
      }
    }
    if ($isSuperAdmin == 1) {
      $cms_moduls_id = DB::table('cms_moduls')->where('is_protected', 0)->pluck('id');
      $cms_menus_id = DB::table('cms_menus')->pluck('id');

      $cms_privileges_concat = "VALUES ";
      $cms_menus_concat = "VALUES ";

      for ($i = 0; $i < count($cms_moduls_id); $i++) {
        $curMod = $cms_moduls_id[$i];
        if ($i < count($cms_moduls_id) - 1) {
          $cms_privileges_concat .= "(1,1,1,1,1,$id,$curMod),";
        } else {
          $cms_privileges_concat .= "(1,1,1,1,1,$id,$curMod)";
        }
      }
      for ($j = 0; $j < count($cms_menus_id); $j++) {
        $curMenu = $cms_menus_id[$j];
        if ($j < count($cms_menus_id) - 1) {
          $cms_menus_concat .= "($curMenu,$id),";
        } else {
          $cms_menus_concat .= "($curMenu,$id)";
        }
      }
      DB::statement("INSERT IGNORE INTO cms_privileges_roles (is_visible, is_create, is_read, is_edit, is_delete, id_cms_privileges, id_cms_moduls) $cms_privileges_concat ");
      DB::statement("INSERT IGNORE INTO cms_menus_privileges (id_cms_menus, id_cms_privileges) $cms_menus_concat ");
    }

    //Refresh Session Roles
    $roles = DB::table('cms_privileges_roles')->where('id_cms_privileges', CRUDBooster::myPrivilegeId())->join('cms_moduls', 'cms_moduls.id', '=', 'id_cms_moduls')->select('cms_moduls.name', 'cms_moduls.path', 'is_visible', 'is_create', 'is_read', 'is_edit', 'is_delete')->get();
    Session::put('admin_privileges_roles', $roles);
    CRUDBooster::redirect(CRUDBooster::adminPath(), trans("crudbooster.alert_add_data_success"), 'success');
  }

  public function getEdit($id)
  {
    $this->cbLoader();

    $row = DB::table($this->table)->where("id", $id)->first();
    if (!$row) return redirect('/admin/privileges'); // không tìm thấy id này sẽ tự chuyển hướng sang list
    if (!CRUDBooster::isRead() && $this->global_privilege == false) { // check quyền , nếu không có quyền superAdmin hay quyền đặc biệt sẽ báo lỗi phân quyền
      CRUDBooster::insertLog(trans("crudbooster.log_try_edit", [
        'name' => $row->{$this->title_field},
        'module' => CRUDBooster::getCurrentModule()->name,
      ]));
      CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));
    }

    $page_title = trans('crudbooster.edit_data_page_title', ['module' => 'Privilege', 'name' => $row->name]);

    $moduls = DB::table("cms_moduls")->where('is_protected', 0)->where('deleted_at', null)->select("cms_moduls.*")->orderby("name", "asc")->get();
    $page_menu = Route::getCurrentRoute()->getActionName();

    return view('custom-privileges', compact('row', 'page_title', 'moduls', 'page_menu'));
  }

  public function postEdit($id)
  {
    $this->cbLoader();

    $row = CRUDBooster::first($this->table, $id);

    if (!CRUDBooster::isUpdate() && $this->global_privilege == false) {
      CRUDBooster::insertLog(trans("crudbooster.log_try_add", ['name' => $row->{$this->title_field}, 'module' => CRUDBooster::getCurrentModule()->name]));
      CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));
    }

    $this->validation($id);
    $this->input_assignment($id);

    DB::table($this->table)->where($this->primary_key, $id)->update($this->arr);
    $priv = request('privileges');
    $isSuperAdmin = request('is_superadmin');
    // This solves issue #1074
    DB::table("cms_privileges_roles")->where("id_cms_privileges", $id)->delete();
    DB::table("cms_menus_privileges")->where("id_cms_privileges", $id)->delete();

    if ($priv) {
      foreach ($priv as $id_modul => $data) {
        //Check Menu
        $module = DB::table('cms_moduls')->where('id', $id_modul)->first();
        $currentPermission = DB::table('cms_privileges_roles')->where('id_cms_moduls', $id_modul)->where('id_cms_privileges', $id)->first();

        if ($currentPermission) {
          $arrs = [];
          foreach ($data as $keyIs => $valueIs) {
            $arrs[$keyIs] = $valueIs ?: 0;
          }
          DB::table('cms_privileges_roles')->where('id', $currentPermission->id)->update($arrs);
        } else {
          $arrs = [];
          $arrs['id_cms_privileges'] = $id;
          $arrs['id_cms_moduls'] = $id_modul;
          foreach ($data as $keyIs => $valueIs) {
            $arrs[$keyIs] = $valueIs ?: 0;
          }

          $menus = DB::table('cms_menus')->where([['is_active', 1], ['path', 'like', $module->controller . "%"]])->first();
          if (!$menus) continue;
          $arrMenus = [];
          $arrMenus['id_cms_menus'] = $menus->id;
          $arrMenus['id_cms_privileges'] = $id;
          DB::table('cms_menus_privileges')->insert($arrMenus);
          DB::table("cms_privileges_roles")->insert($arrs);
        }
      }
    }
    if ($isSuperAdmin == 1) {
      $cms_moduls_id = DB::table('cms_moduls')->where('is_protected', 0)->pluck('id');
      $cms_menus_id = DB::table('cms_menus')->pluck('id');

      $cms_privileges_concat = "VALUES ";
      $cms_menus_concat = "VALUES ";

      for ($i = 0; $i < count($cms_moduls_id); $i++) {
        $curMod = $cms_moduls_id[$i];
        if ($i < count($cms_moduls_id) - 1) {
          $cms_privileges_concat .= "(1,1,1,1,1,$id,$curMod),";
        } else {
          $cms_privileges_concat .= "(1,1,1,1,1,$id,$curMod)";
        }
      }
      for ($j = 0; $j < count($cms_menus_id); $j++) {
        $curMenu = $cms_menus_id[$j];
        if ($j < count($cms_menus_id) - 1) {
          $cms_menus_concat .= "($curMenu,$id),";
        } else {
          $cms_menus_concat .= "($curMenu,$id)";
        }
      }
      DB::statement("INSERT IGNORE INTO cms_privileges_roles (is_visible, is_create, is_read, is_edit, is_delete, id_cms_privileges, id_cms_moduls) $cms_privileges_concat ");
      DB::statement("INSERT IGNORE INTO cms_menus_privileges (id_cms_menus, id_cms_privileges) $cms_menus_concat ");
    }

    //Refresh Session Roles
    if ($id == CRUDBooster::myPrivilegeId()) {
      $roles = DB::table('cms_privileges_roles')->where('id_cms_privileges', CRUDBooster::myPrivilegeId())->join('cms_moduls', 'cms_moduls.id', '=', 'id_cms_moduls')->select('cms_moduls.name', 'cms_moduls.path', 'is_visible', 'is_create', 'is_read', 'is_edit', 'is_delete')->get();
      Session::put('admin_privileges_roles', $roles);

      Session::put('theme_color', $this->arr['theme_color']);
    }

    CRUDBooster::redirect('admin/privileges', trans("crudbooster.alert_update_data_success", [
      'module' => "Privilege",
      'title' => $row->name,
    ]), 'success');
  }

  public function getDelete($id)
  {
    $this->cbLoader();

    $row = DB::table($this->table)->where($this->primary_key, $id)->first();
    if (!CRUDBooster::isDelete() && $this->global_privilege == false) {
      CRUDBooster::insertLog(trans("crudbooster.log_try_delete", [
        'name' => $row->{$this->title_field},
        'module' => CRUDBooster::getCurrentModule()->name,
      ]));
      CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));
    }
    if ($row->is_superadmin)
      CRUDBooster::redirect('admin/privileges', trans('crudbooster.delete_admin_denied'));

    DB::table($this->table)->where($this->primary_key, $id)->delete();
    DB::table("cms_privileges_roles")->where("id_cms_privileges", $row->id)->delete();
    DB::table("cms_menus_privileges")->where("id_cms_privileges", $row->id)->delete();

    CRUDBooster::redirect('admin/privileges', trans("crudbooster.alert_delete_data_success"), 'success');
  }
}
