<?php

namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use CRUDBooster;
use Exception;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Tab\FormTab as FormTab;
use Illuminate\Support\Facades\URL;

class AdminSettingController extends MyCBController{
  public function cbInit()
  {
    $this->table = 'holiday_pricing';
    $this->primary_key = 'id';
    $this->title_field = "nama";
    $this->button_show = false;
    $this->button_new = false;
    $this->button_delete = false;
    $this->button_add = false;
    $this->button_import = false;
    $this->button_export = false;
  }

  function getEditSetting()
  {
    $this->cbLoader();
    $formTab = new FormTab(new AdminSettingController());
    $formTab->setTab(
      [
        'tabName' => 'Giá ngày lễ',
        'apiUrl' => '/admin/setting/holiday-pricing',
        'apiController' => 'getHolidayPricing',
      ]
      // ,[
      //   'tabName' => 'Tùy Chỉnh',
      //   'tabView' => '<h1 style="color:red"> Nội dung tùy chỉnh </h1>',
      // ]
      // ,[
      //   'tabName' => 'Khác',
      //   'tabView' => '<h1 style="color:blue"> Tab Khác </h1>'
      // ]
    );
    return $formTab->getIndex();
  }

  public function getHolidayPricing()
  {
    $id = 1;
    if ($id != 1) {
      return Redirect::to('admin/setting/config-setting');
    }

    $this->cbLoader();
    $row = DB::table('holiday_pricing')->first();

    if (!CRUDBooster::isRead() && $this->global_privilege == false || $this->button_edit == false) {
      CRUDBooster::insertLog(trans("crudbooster.log_try_edit", [
        'name' => $row->{$this->title_field},
        'module' => CRUDBooster::getCurrentModule()->name,
      ]));
      CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));
    }

    $page_menu = Route::getCurrentRoute()->getActionName();
    $module = (isset(CRUDBooster::getCurrentModule()->translate_code)) ? trans(CRUDBooster::getCurrentModule()->translate_code) : CRUDBooster::getCurrentModule()->name;
    $page_title = trans("crudbooster.edit_data_page_title", ['module' => $module, 'name' => $row->{$this->title_field}]);
    $command = 'edit';
    Session::put('current_row_id', $id);

    $view_edit = $this->view_edit;
    $updated_at = (isset($row->updated_at)) ? $row->updated_at : '';
    $data = $row;

    return ['view' => view('setting.holiday_pricing', compact('id', 'row', 'page_menu', 'page_title', 'command', 'view_edit', 'updated_at', 'data'))->render()];
  }

  public function postHolidayPricing()
  {
    try {
      $min_km_re = (int) request('min_km_re');
      $min_km_price_re = (int) request('min_km_price_re');
      $min_up_price_re = (int) request('min_up_price_re');

      DB::table('holiday_pricing')->where('pricing_id', 3)->update([
        'min_km_re' => $min_km_re,
        'min_km_price_re' => $min_km_price_re,
        'min_up_price_re' => $min_up_price_re,
        'modify_date' => date('Y-m-d H:i:s')
      ]);
      return ['status' => 200, 'msg' => __('crudbooster.update_success')];
    } catch (Exception $e) {
      throw ['status' => 500 , 'msg' => new Exception($e)];
    }
  }
}
