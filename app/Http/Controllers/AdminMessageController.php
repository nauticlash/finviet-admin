<?php

namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use CRUDBooster;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Log;

class AdminMessageController extends MyCBController
{

  public function cbInit()
  {

    # START CONFIGURATION DO NOT REMOVE THIS LINE
    $this->title_field = "id";
    $this->limit = "20";
    $this->orderby = "id,desc";
    $this->global_privilege = false;
    $this->button_table_action = true;
    $this->button_bulk_action = false;
    $this->button_action_style = "button_icon";
    $this->button_add = false;
    $this->button_edit = false;
    $this->button_delete = false;
    $this->button_detail = true;
    $this->button_show = false;
    $this->button_filter = false;
    $this->button_import = false;
    $this->button_export = false;
    $this->table = "message";

    // vinhth add code
    $this->view_detail = "message.form_detail";

    # END CONFIGURATION DO NOT REMOVE THIS LINE
    # START COLUMNS DO NOT REMOVE THIS LINE
    $this->col = [];
    $this->col[] = ["label" => "Id", "name" => "id"];
    $this->col[] = [
      "label" => trans("crudbooster.field_time"),
      "name" => "(SELECT MAX(m.create_date)
	    FROM message m where m.fromId = message.fromId) as message_date"
    ];

    //	$this->col[] = ["label" => "Orderid", "name" => "orderid"];
    //	$this->col[] = ["label" => "FromId", "name" => "fromId"];
    $this->col[] = [
      "label" => trans("crudbooster.field_firstname"),
      "name" => "(SELECT IF ( CONCAT(IFNULL(um.firstname, '') ,' ',IFNULL(um.lastname, '')) != '' , CONCAT(IFNULL(um.firstname, '') ,' ',IFNULL(um.lastname, '')) , um.contactno )
	    FROM user_master um where um.id = message.FromId) as fullname"
    ];

    $this->col[] = [
      "label" => trans("crudbooster.field_phone_number"),
      "name" => "(SELECT u.contactno
	    FROM user_master u where u.id = message.FromId) as contactno"
    ];

    //	$this->col[] = ["label" => "ToId", "name" => "toId"];
    //	$this->col[] = ["label" => "Tin nhắn", "name" => "message"];
    //	$this->col[] = ["label" => "Attachment", "name" => "attachment"];
    $this->col[] = [
      "label" => trans("crudbooster.field_mess"),
      "name" => "(SELECT MIN(m.isViewed)
	    FROM message m where m.fromId = message.fromId) as isView",
      "callback_php" => 'get_string_in_array(\CommonData::$MESSAGE_ISVIEW, $row->isView)'
    ];

    //	$this->col[] = ["label" => "Is Deleted", "name" => "is_deleted"];
    # END COLUMNS DO NOT REMOVE THIS LINE
    # START FORM DO NOT REMOVE THIS LINE

    /*
	  $this->form = [];
	  $this->form[] = ['label' => 'Orderid', 'name' => 'orderid', 'type' => 'number', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10'];
	  $this->form[] = ['label' => 'FromId', 'name' => 'fromId', 'type' => 'number', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10'];
	  $this->form[] = ['label' => 'ToId', 'name' => 'toId', 'type' => 'number', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10'];
	  $this->form[] = ['label' => 'Message', 'name' => 'message', 'type' => 'textarea', 'validation' => 'required|string|min:5|max:5000', 'width' => 'col-sm-10'];
	  $this->form[] = ['label' => 'Attachment', 'name' => 'attachment', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	  $this->form[] = ['label' => 'IsViewed', 'name' => 'isViewed', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	  $this->form[] = ['label' => 'Is Deleted', 'name' => 'is_deleted', 'type' => 'radio', 'validation' => 'required|integer', 'width' => 'col-sm-10', 'dataenum' => 'Array'];
	  $this->form[] = ['label' => 'Create Date', 'name' => 'create_date', 'type' => 'datetime', 'validation' => 'required|date_format:Y-m-d H:i:s', 'width' => 'col-sm-10'];
	  $this->form[] = ['label' => 'Modify Date', 'name' => 'modify_date', 'type' => 'datetime', 'validation' => 'required|date_format:Y-m-d H:i:s', 'width' => 'col-sm-10'];
	  $this->form[] = ['label' => 'Delete Date', 'name' => 'delete_date', 'type' => 'datetime', 'validation' => 'required|date_format:Y-m-d H:i:s', 'width' => 'col-sm-10'];
	  $this->form[] = ['label' => 'Created By', 'name' => 'created_by', 'type' => 'number', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10'];
	  $this->form[] = ['label' => 'Updated By', 'name' => 'updated_by', 'type' => 'number', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10'];
	 */

    # END FORM DO NOT REMOVE THIS LINE
    # OLD START FORM
    //$this->form = [];
    //$this->form[] = ["label"=>"Orderid","name"=>"orderid","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
    //$this->form[] = ["label"=>"FromId","name"=>"fromId","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
    //$this->form[] = ["label"=>"ToId","name"=>"toId","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
    //$this->form[] = ["label"=>"Message","name"=>"message","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
    //$this->form[] = ["label"=>"Attachment","name"=>"attachment","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
    //$this->form[] = ["label"=>"IsViewed","name"=>"isViewed","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
    //$this->form[] = ["label"=>"Is Deleted","name"=>"is_deleted","type"=>"radio","required"=>TRUE,"validation"=>"required|integer","dataenum"=>"Array"];
    //$this->form[] = ["label"=>"Create Date","name"=>"create_date","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
    //$this->form[] = ["label"=>"Modify Date","name"=>"modify_date","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
    //$this->form[] = ["label"=>"Delete Date","name"=>"delete_date","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
    //$this->form[] = ["label"=>"Created By","name"=>"created_by","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
    //$this->form[] = ["label"=>"Updated By","name"=>"updated_by","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
    # OLD END FORM

    /*
	  | ----------------------------------------------------------------------
	  | Sub Module
	  | ----------------------------------------------------------------------
	  | @label          = Label of action
	  | @path           = Path of sub module
	  | @foreign_key 	  = foreign key of sub table/module
	  | @button_color   = Bootstrap Class (primary,success,warning,danger)
	  | @button_icon    = Font Awesome Class
	  | @parent_columns = Sparate with comma, e.g : name,created_at
	  |
	 */
    $this->sub_module = array();


    /*
	  | ----------------------------------------------------------------------
	  | Add More Action Button / Menu
	  | ----------------------------------------------------------------------
	  | @label       = Label of action
	  | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	  | @icon        = Font awesome class icon. e.g : fa fa-bars
	  | @color 	   = Default is primary. (primary, warning, succecss, info)
	  | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	  |
	 */
    $this->addaction = array();


    /*
	  | ----------------------------------------------------------------------
	  | Add More Button Selected
	  | ----------------------------------------------------------------------
	  | @label       = Label of action
	  | @icon 	   = Icon from fontawesome
	  | @name 	   = Name of button
	  | Then about the action, you should code at actionButtonSelected method
	  |
	 */
    $this->button_selected = array();


    /*
	  | ----------------------------------------------------------------------
	  | Add alert message to this module at overheader
	  | ----------------------------------------------------------------------
	  | @message = Text of message
	  | @type    = warning,success,danger,info
	  |
	 */
    $this->alert = array();



    /*
	  | ----------------------------------------------------------------------
	  | Add more button to header button
	  | ----------------------------------------------------------------------
	  | @label = Name of button
	  | @url   = URL Target
	  | @icon  = Icon from Awesome.
	  |
	 */
    $this->index_button = array();



    /*
	  | ----------------------------------------------------------------------
	  | Customize Table Row Color
	  | ----------------------------------------------------------------------
	  | @condition = If condition. You may use field alias. E.g : [id] == 1
	  | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
	  |
	 */
    $this->table_row_color = array();


    /*
	  | ----------------------------------------------------------------------
	  | You may use this bellow array to add statistic at dashboard
	  | ----------------------------------------------------------------------
	  | @label, @count, @icon, @color
	  |
	 */
    $this->index_statistic = array();



    /*
	  | ----------------------------------------------------------------------
	  | Add javascript at body
	  | ----------------------------------------------------------------------
	  | javascript code in the variable
	  | $this->script_js = "function() { ... }";
	  |
	 */
    $this->script_js = NULL;


    /*
	  | ----------------------------------------------------------------------
	  | Include HTML Code before index table
	  | ----------------------------------------------------------------------
	  | html code to display it before index table
	  | $this->pre_index_html = "<p>test</p>";
	  |
	 */
    $this->pre_index_html = null;



    /*
	  | ----------------------------------------------------------------------
	  | Include HTML Code after index table
	  | ----------------------------------------------------------------------
	  | html code to display it after index table
	  | $this->post_index_html = "<p>test</p>";
	  |
	 */
    $this->post_index_html = null;



    /*
	  | ----------------------------------------------------------------------
	  | Include Javascript File
	  | ----------------------------------------------------------------------
	  | URL of your javascript each array
	  | $this->load_js[] = asset("myfile.js");
	  |
	 */
    $this->load_js = array();



    /*
	  | ----------------------------------------------------------------------
	  | Add css style at body
	  | ----------------------------------------------------------------------
	  | css code in the variable
	  | $this->style_css = ".style{....}";
	  |
	 */
    $this->style_css = NULL;



    /*
	  | ----------------------------------------------------------------------
	  | Include css File
	  | ----------------------------------------------------------------------
	  | URL of your css each array
	  | $this->load_css[] = asset("myfile.css");
	  |
	 */
    $this->load_css = array();
  }

  public function hook_query_index(&$query)
  {
    $query->groupBy('fromId')
      ->where('fromId', '<>', 1)
      //		->orderBy('isView', 'ASC')
      //		->orderBy('message_date', 'DESC')
    ;
  }

  public function hook_row_index($column_index, &$column_value)
  {
    //Your code here
  }

  public function getDetail($id)
  {
    $this->cbLoader();
    $row = DB::table($this->table)->where($this->primary_key, $id)->first();

    if (!CRUDBooster::isRead() && $this->global_privilege == false || $this->button_detail == false) {
      CRUDBooster::insertLog(trans("crudbooster.log_try_view", [
        'name' => $row->{$this->title_field},
        'module' => CRUDBooster::getCurrentModule()->name,
      ]));
      CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));
    }
    $send_msg_permission = DB::table('cms_privileges_roles')->where([
      ['id_cms_privileges', CRUDBooster::myPrivilegeId()],
      ['id_cms_moduls', 15]
    ])->first();
    $allowSendNotification = $send_msg_permission->is_create ?? false;
    $module = CRUDBooster::getCurrentModule();

    $page_menu = Route::getCurrentRoute()->getActionName();
    $module_name = ($module->translate_code) ? trans($module->translate_code) : $module->name;
    $page_title = trans("crudbooster.detail_data_page_title", ['module' => $module_name, 'name' => $row->{$this->title_field}]);
    $command = 'detail';

    Session::put('current_row_id', $id);

    $toId = ($row->fromId > 1) ? $row->fromId : $row->toId;
    $query = "SELECT message.*, user_master.profilepic, user_master.contactno, user_master.firstname, user_master.lastname FROM message LEFT JOIN user_master ON user_master.id = message.fromId WHERE is_deleted != 1 AND (message.fromId = $toId OR message.toId = $toId) ORDER BY message.create_date ASC";
    $complaininfo = DB::select($query);
    $data = $complaininfo;
    //	echo "<pre>"; print_r($complaininfo);die();
    //vinhth add code
    $view_detail = $this->view_detail;

    return view($this->view_form, compact('row', 'page_menu', 'page_title', 'command', 'id', 'view_detail', 'data', 'toId' ,'allowSendNotification'));
    //	return view('crudbooster::default.form', compact('row', 'page_menu', 'page_title', 'command', 'id'));
  }

  public function postSendMessage()
  {
    $toId = Request::get('toId');
    $message = Request::get('message');

    if (!empty($toId) && !empty($message)) {
      $url = env('API_URL');
      $ch = curl_init();
      $requestData = [
        "service" => "sendmessage",
        "request" => ["data" => ["toId" => $toId, "attachment" => "", "message" => $message]],
        "auth" => [
          "token" => "finviet",
          "id" => 1
        ]
      ];

      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));



      $response = curl_exec($ch);

      curl_close($ch);

      $response = json_decode($response);
      Log::info('error: ' . 'Chat Response ' . json_encode($response));


      if ($response->success) {
        		echo "<pre>";print_r($response);

        DB::table('message')->where([
          'isViewed' => 0,
          'fromId' => $toId,
          'toId' => 1
        ])->update([
          'isViewed' => 1
        ]);

        $res['status'] = 1;
        return $res;
      }
    }
  }
}
