<?php

namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use CRUDBooster;
use CB;
use Schema;
use Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

class AdminOrderController extends MyCBController
{

  public $table = 'order_master';

  public function cbInit()
  {

    # START CONFIGURATION DO NOT REMOVE THIS LINE
    $this->title_field = "sender_name";
    $this->limit = "20";
    $this->orderby = "id,desc";
    $this->global_privilege = false;
    $this->button_table_action = true;
    $this->button_bulk_action = FALSE;
    $this->button_action_style = "button_icon";
    $this->button_add = FALSE;
    $this->button_edit = FALSE;
    $this->button_delete = FALSE;
    $this->button_detail = true;
    $this->button_show = FALSE;
    $this->button_filter = true;
    $this->button_import = false;
    $this->button_export = true;
    $this->table = "order_master";
    $this->query_cols = ["merchant_order_id", "pickup_address", "delivery_address"];

    // vinhth add code
    $this->view_index_top = "order.index_top";
    $this->view_detail = "order.form_detail";
    $this->view_index = "order.index";

    # END CONFIGURATION DO NOT REMOVE THIS LINE
    # START COLUMNS DO NOT REMOVE THIS LINE
    $this->col = [];
    $this->col[] = ["label" => "Id", "name" => "id", "style" => "style=display:none"];
    $this->col[] = ["label" => trans("crudbooster.order_id"), "name" => "merchant_order_id"];
    $this->col[] = ["label" => trans("order.created_at"), "name" => "orderdate", "callback_php" => 'get_formatted_date($row->orderdate)'];
    // $this->col[] = ["label" => trans("order.partner"), "name" => "m.name", "sortable" => false];
    $this->col[] = ["label" => trans("crudbooster.field_status"), "name" => "status", "sortable" => false];
    $this->col[] = ["label" => trans("crudbooster.driver_list"), "name" => "CONCAT(um.firstname, ' - ' ,um.contactno) as fullname", "sortable" => false];
    $this->col[] = ["label" => trans("crudbooster.field_pickup_address"), "name" => "pickup_address", "sortable" => false];
    $this->col[] = ["label" => trans("crudbooster.field_delivery_address"), "name" => "delivery_address", "sortable" => false];
    $this->col[] = ["label" => trans("order.distance"), "name" => "no_of_km"];
    $this->col[] = ["label" => trans("order.shipping_amount"), "name" => "shipping_amount", "callback_php" => 'get_format_money_usd($row->shipping_amount)'];
    # END COLUMNS DO NOT REMOVE THIS LINE
    # START FORM DO NOT REMOVE THIS LINE
    $this->form = [];
    $this->form[] = ['label' => 'Địa điểm lấy hàng', 'name' => 'pickup_address', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
    $this->form[] = ['label' => 'Địa điểm giao hàng', 'name' => 'delivery_address', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
    $this->form[] = ['label' => 'Mô tả', 'name' => 'description', 'type' => 'textarea', 'validation' => 'required|string|min:5|max:5000', 'width' => 'col-sm-10'];
    $this->form[] = ['label' => 'Số điện thoại cửa hàng', 'name' => 'merchant_contact', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
    $this->form[] = ['label' => 'Tên người nhận', 'name' => 'recipient_name', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
    $this->form[] = ['label' => 'Số điện thoại người nhận', 'name' => 'customer_contact', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
    # END FORM DO NOT REMOVE THIS LINE

    $this->script_js = NULL;

    $this->load_js = array();

    $this->style_css = NULL;

    $this->load_css = array();
    $this->load_css[] = asset("assets/css/style.css?v=" . Config::get('constants.VERSION_FILE'));
    if (CRUDBooster::getCurrentMethod() == "getDetail") {
      $this->load_css[] = asset("assets/css/popup_modal.css?v=" . Config::get('constants.VERSION_FILE'));
    }
  }

  public function getIndex()
  {
    $this->cbLoader();

    $module = CRUDBooster::getCurrentModule();

    if (!CRUDBooster::isView() && $this->global_privilege == false) {
      CRUDBooster::insertLog(trans('crudbooster.log_try_view', ['module' => $module->name]));
      CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));
    }

    $data['table'] = $this->table;
    $data['table_pk'] = CB::pk($this->table);
    //	$data['page_title']   = $module->name;
    $data['page_title'] = ($module->translate_code) ? trans($module->translate_code) : $module->name;
    $data['page_description'] = trans('crudbooster.default_module_description');
    $data['date_candidate'] = $this->date_candidate;
    $data['limit'] = $limit = (Request::get('limit')) ? Request::get('limit') : $this->limit;

    $tablePK = $data['table_pk'];
    $table_columns = CB::getTableColumns($this->table);
    $result = DB::table($this->table)->select(DB::raw($this->table . "." . $this->primary_key));

    $this->hook_query_index($result);

    if (in_array('deleted_at', $table_columns)) {
      $result->where($this->table . '.deleted_at', null);
    }

    $alias = [];
    $join_alias_count = 0;
    $join_table_temp = [];
    $table = $this->table;
    $columns_table = $this->columns_table;
    foreach ($columns_table as $index => $coltab) {

      $join = @$coltab['join'];
      $join_where = @$coltab['join_where'];
      $join_id = @$coltab['join_id'];
      $field = @$coltab['name'];
      $join_table_temp[] = $table;

      if (!$field) {
        continue;
      }

      if (strpos($field, ' as ') !== false) {
        $field = substr($field, strpos($field, ' as ') + 4);
        $field_with = (array_key_exists('join', $coltab)) ? str_replace(",", ".", $coltab['join']) : $field;
        $result->addselect(DB::raw($coltab['name']));
        $columns_table[$index]['type_data'] = 'varchar';
        $columns_table[$index]['field'] = $field;
        $columns_table[$index]['field_raw'] = $field;
        $columns_table[$index]['field_with'] = $field_with;
        $columns_table[$index]['is_subquery'] = true;
        continue;
      }

      if (strpos($field, '.') !== false) {
        $result->addselect($field);
      } else {
        $result->addselect($table . '.' . $field);
      }

      $field_array = explode('.', $field);

      if (isset($field_array[1])) {
        $field = $field_array[1];
        $table = $field_array[0];
      } else {
        $table = $this->table;
      }

      if ($join) {

        $join_exp = explode(',', $join);

        $join_table = $join_exp[0];
        $joinTablePK = CB::pk($join_table);
        $join_column = $join_exp[1];
        $join_alias = str_replace(".", "_", $join_table);

        if (in_array($join_table, $join_table_temp)) {
          $join_alias_count += 1;
          $join_alias = $join_table . $join_alias_count;
        }
        $join_table_temp[] = $join_table;

        $result->leftjoin($join_table . ' as ' . $join_alias, $join_alias . (($join_id) ? '.' . $join_id : '.' . $joinTablePK), '=', DB::raw($table . '.' . $field . (($join_where) ? ' AND ' . $join_where . ' ' : '')));
        $result->addselect($join_alias . '.' . $join_column . ' as ' . $join_alias . '_' . $join_column);

        $join_table_columns = CRUDBooster::getTableColumns($join_table);
        if ($join_table_columns) {
          foreach ($join_table_columns as $jtc) {
            $result->addselect($join_alias . '.' . $jtc . ' as ' . $join_alias . '_' . $jtc);
          }
        }

        $alias[] = $join_alias;
        $columns_table[$index]['type_data'] = CRUDBooster::getFieldType($join_table, $join_column);
        $columns_table[$index]['field'] = $join_alias . '_' . $join_column;
        $columns_table[$index]['field_with'] = $join_alias . '.' . $join_column;
        $columns_table[$index]['field_raw'] = $join_column;

        @$join_table1 = $join_exp[2];
        @$joinTable1PK = CB::pk($join_table1);
        @$join_column1 = $join_exp[3];
        @$join_alias1 = $join_table1;

        if ($join_table1 && $join_column1) {

          if (in_array($join_table1, $join_table_temp)) {
            $join_alias_count += 1;
            $join_alias1 = $join_table1 . $join_alias_count;
          }

          $join_table_temp[] = $join_table1;

          $result->leftjoin($join_table1 . ' as ' . $join_alias1, $join_alias1 . '.' . $joinTable1PK, '=', $join_alias . '.' . $join_column);
          $result->addselect($join_alias1 . '.' . $join_column1 . ' as ' . $join_column1 . '_' . $join_alias1);
          $alias[] = $join_alias1;
          $columns_table[$index]['type_data'] = CRUDBooster::getFieldType($join_table1, $join_column1);
          $columns_table[$index]['field'] = $join_column1 . '_' . $join_alias1;
          $columns_table[$index]['field_with'] = $join_alias1 . '.' . $join_column1;
          $columns_table[$index]['field_raw'] = $join_column1;
        }
      } else {

        if (isset($field_array[1])) {
          $result->addselect($table . '.' . $field . ' as ' . $table . '_' . $field);
          $columns_table[$index]['type_data'] = CRUDBooster::getFieldType($table, $field);
          $columns_table[$index]['field'] = $table . '_' . $field;
          $columns_table[$index]['field_raw'] = $table . '.' . $field;
        } else {
          $result->addselect($table . '.' . $field);
          $columns_table[$index]['type_data'] = CRUDBooster::getFieldType($table, $field);
          $columns_table[$index]['field'] = $field;
          $columns_table[$index]['field_raw'] = $field;
        }

        $columns_table[$index]['field_with'] = $table . '.' . $field;
      }
    }

    // vinhth add code =====================================================
    $select_status = Request::get('select_status');
    $select_rider_id = Request::get('select_rider_id');
    $select_userid = Request::get('select_userid');
    $select_end_date = Request::get('select_end_date');
    // $select_merchant_id = Request::get('select_merchant_id');
    $result->addselect($this->table . '.rider_id');
    $result->addselect($this->table . '.rider_accept_time');
    $result->addselect($this->table . '.is_pickup');
    $result->addselect($this->table . '.pickup_time');
    $result->addselect($this->table . '.arrive_pickup_time');
    $result->addselect($this->table . '.is_dropoff');
    $result->addselect($this->table . '.dropoff_time');
    $result->addselect($this->table . '.is_return');

    if (isset($select_status) && !empty($select_status)) {
      $status = 'In Progress';
      $riderId = 0;
      $isPickup = 0;
      $isDropoff = 0;
      switch ($select_status) {
        case 'allocating':
          $status = 'Pending';
          $result->where($this->table . '.status', $status);
          $result->where($this->table . '.rider_id', $riderId);
          $result->where($this->table . '.is_pickup', $isPickup);
          $result->where($this->table . '.is_dropoff', $isDropoff);
          break;
        case 'accepted':
          $result->where($this->table . '.status', $status);
          $result->where($this->table . '.rider_id', '>', $riderId);
          $result->where($this->table . '.status', $status);
          $result->where($this->table . '.is_pickup', $isPickup);
          $result->where($this->table . '.is_dropoff', $isDropoff);
          break;
        case 'picking_up':
          $isPickup = 1;
          $result->where($this->table . '.status', $status);
          //                    $result->where($this->table . '.rider_id', '>', $riderId);
          $result->where($this->table . '.is_pickup', $isPickup);
          $result->where($this->table . '.is_dropoff', $isDropoff);
          break;
        case 'delivering':
          $isPickup = 3;
          $result->where($this->table . '.status', $status);
          //                    $result->where($this->table . '.rider_id', '>', $riderId);
          $result->where($this->table . '.is_pickup', $isPickup);
          $result->where($this->table . '.is_dropoff', $isDropoff);
          break;
        case 'delivered':
          $isPickup = 3;
          $isDropoff = 2;
          $result->where($this->table . '.status', $status);
          $result->where($this->table . '.is_pickup', $isPickup);
          $result->where($this->table . '.is_dropoff', $isDropoff);
          break;
        case 'completed':
          $isPickup = 3;
          $isDropoff = 2;
          $status = 'Complete';
          $result->where($this->table . '.status', $status);
          $result->where($this->table . '.is_pickup', $isPickup);
          $result->where($this->table . '.is_dropoff', $isDropoff);
          break;
        case 'canceled':
          $status = 'Canceled';
          $result->where($this->table . '.status', $status);
          break;
        default:
          break;
      }
      $data['select_status'] = $select_status;
    }

    if (isset($select_rider_id) && !empty($select_rider_id)) {
      $result->where($this->table . '.rider_id', $select_rider_id);
      $data['select_rider_id'] = $select_rider_id;
      $rider = DB::table("user_master")->where($this->primary_key, $select_rider_id)->first();
      $data['rider'] = $rider;
    }

    if (isset($select_userid) && !empty($select_userid)) {
      $result->where($this->table . '.userid', $select_userid);
      $data['select_userid'] = $select_userid;
    }
    $end_date = date_create($select_end_date);
    $where_end_date = date_format($end_date, "Y-m-d");

    if (!empty(Request::get('select_start_date'))) {
      $select_start_date = Request::get('select_start_date');
      $start_date = date_create($select_start_date);
      $where_start_date = date_format($start_date, "Y-m-d");
      if (!empty($select_start_date)) {
        $result->where($this->table . '.created_at', '>', $where_start_date . ' 00:00:00');
        $data['select_start_date'] = date_format($start_date, "d-m-Y");
      }
    }

    if (!empty($select_end_date)) {
      $result->where($this->table . '.created_at', '<=', $where_end_date . ' 23:59:59');
      $data['select_end_date'] = date_format($end_date, "d-m-Y");
    }
    // if (isset($select_merchant_id) && !empty($select_merchant_id)) {
    //     $result->where($this->table . '.merchant_id', $select_merchant_id);
    //     $data['select_merchant_id'] = $select_merchant_id;
    // }

    // $data['merchants'] = DB::table('merchants')->select('id', 'name')
    //     ->where('status', '=', 1)
    //     ->get();

    $str_status = \CommonData::$ORDER_MASTER_STATUS;

    $data['array_status'] = create_array_key_value_from_string_common_data($str_status);

    // end vinhth add code =================================================


      if (Request::get('q')) {
          $query_cols = $this->query_cols;
          $result->where(function ($w) use ($query_cols) {
              foreach ($query_cols as $col) {
                  $w->orwhere($col, "like", "%" . Request::get("q") . "%");
              }
          });
      }

    if (Request::get('where')) {
      foreach (Request::get('where') as $k => $v) {
        $result->where($table . '.' . $k, $v);
      }
    }

    $filter_is_orderby = false;
    if (Request::get('filter_column')) {

      $filter_column = Request::get('filter_column');
      $result->where(function ($w) use ($filter_column, $fc) {
        foreach ($filter_column as $key => $fc) {

          $value = @$fc['value'];
          $type = @$fc['type'];

          if ($type == 'empty') {
            $w->whereNull($key)->orWhere($key, '');
            continue;
          }

          if ($value == '' || $type == '') {
            continue;
          }

          if ($type == 'between') {
            continue;
          }

          switch ($type) {
            default:
              if ($key && $type && $value) {
                $w->where($key, $type, $value);
              }
              break;
            case 'like':
            case 'not like':
              $value = '%' . $value . '%';
              if ($key && $type && $value) {
                $w->where($key, $type, $value);
              }
              break;
            case 'in':
            case 'not in':
              if ($value) {
                $value = explode(',', $value);
                if ($key && $value) {
                  $w->whereIn($key, $value);
                }
              }
              break;
          }
        }
      });

      foreach ($filter_column as $key => $fc) {
        $value = @$fc['value'];
        $type = @$fc['type'];
        $sorting = @$fc['sorting'];

        if ($sorting != '') {
          if ($key) {
            $result->orderby($key, $sorting);
            $filter_is_orderby = true;
          }
        }

        if ($type == 'between') {
          if ($key && $value) {
            $result->whereBetween($key, $value);
          }
        } else {
          continue;
        }
      }
    }
    $result->leftjoin('user_master as um', 'um.id', '=', $this->table . '.rider_id');
    $result->leftjoin('merchants as m', 'm.id', '=', $this->table . '.merchant_id');

    if ($filter_is_orderby == true) {
      $data['result'] = $result->paginate($limit);
    } else {
      if ($this->orderby) {
        if (is_array($this->orderby)) {
          foreach ($this->orderby as $k => $v) {
            if (strpos($k, '.') !== false) {
              $orderby_table = explode(".", $k)[0];
              $k = explode(".", $k)[1];
            } else {
              $orderby_table = $this->table;
            }
            $result->orderby($orderby_table . '.' . $k, $v);
          }
        } else {
          $this->orderby = explode(";", $this->orderby);
          foreach ($this->orderby as $o) {
            $o = explode(",", $o);
            $k = $o[0];
            $v = $o[1];
            if (strpos($k, '.') !== false) {
              $orderby_table = explode(".", $k)[0];
            } else {
              $orderby_table = $this->table;
            }
            $result->orderby($orderby_table . '.' . $k, $v);
          }
        }
        $data['result'] = $result->paginate($limit);
      } else {
        $data['result'] = $result->orderby($this->table . '.' . $this->primary_key, 'desc')->paginate($limit);
      }
    }

    $data['columns'] = $columns_table;

    if ($this->index_return) {
      return $data;
    }

    //LISTING INDEX HTML
    $addaction = $this->data['addaction'];

    $addaction[] = [
      'title' => 'Lịch sử đơn hàng',
      'icon' => 'fa fa-history',
      'url' => Config::get('constants.BASE_URL_ADMIN') . '/order/order-timeline/[id]',
      'color' => 'danger',
      'showIf' => true,
    ];

    if ($this->sub_module) {
      foreach ($this->sub_module as $s) {
        $table_parent = CRUDBooster::parseSqlTable($this->table)['table'];
        $addaction[] = [
          'label' => $s['label'],
          'icon' => $s['button_icon'],
          'url' => CRUDBooster::adminPath($s['path']) . '?return_url=' . urlencode(Request::fullUrl()) . '&parent_table=' . $table_parent . '&parent_columns=' . $s['parent_columns'] . '&parent_columns_alias=' . $s['parent_columns_alias'] . '&parent_id=[' . (!isset($s['custom_parent_id']) ? "id" : $s['custom_parent_id']) . ']&foreign_key=' . $s['foreign_key'] . '&label=' . urlencode($s['label']),
          'color' => $s['button_color'],
          'showIf' => $s['showIf'],
        ];
      }
    }
    $html_contents = [];
    $page = (Request::get('page')) ? Request::get('page') : 1;
    $number = ($page - 1) * $limit + 1;
    foreach ($data['result'] as $row) {
      $html_content = [];

      if ($this->button_bulk_action) {

        $html_content[] = "<input type='checkbox' class='checkbox' name='checkbox[]' value='" . $row->{$tablePK} . "'/>";
      }

      if ($this->show_numbering) {
        $html_content[] = $number . '. ';
        $number++;
      }

      foreach ($columns_table as $col) {
        if ($col['visible'] === false) {
          continue;
        }

        $value = @$row->{$col['field']};
        $title = @$row->{$this->title_field};
        $label = $col['label'];

        if (isset($col['image'])) {
          if ($value == '') {
            $value = "<a  data-lightbox='roadtrip' rel='group_{{$table}}' title='$label: $title' href='" . asset('vendor/crudbooster/noimage.png') . "'><img width='40px' height='40px' src='" . asset('vendor/crudbooster/noimage.png') . "'/></a>";
          } else {
            $pic = (strpos($value, 'http://') !== false) ? $value : asset($value);
            $value = "<a data-lightbox='roadtrip'  rel='group_{{$table}}' title='$label: $title' href='" . $pic . "'><img width='40px' height='40px' src='" . $pic . "'/></a>";
          }
        }

        if (@$col['download']) {
          $url = (strpos($value, 'http://') !== false) ? $value : asset($value) . '?download=1';
          if ($value) {
            $value = "<a class='btn btn-xs btn-primary' href='$url' target='_blank' title='Download File'><i class='fa fa-download'></i> Download</a>";
          } else {
            $value = " - ";
          }
        }

        if ($col['str_limit']) {
          $value = trim(strip_tags($value));
          $value = str_limit($value, $col['str_limit']);
        }

        if ($col['nl2br']) {
          $value = nl2br($value);
        }

        if ($col['callback_php']) {
          foreach ($row as $k => $v) {
            $col['callback_php'] = str_replace("[" . $k . "]", $v, $col['callback_php']);
          }
          @eval("\$value = " . $col['callback_php'] . ";");
        }

        //New method for callback
        if (isset($col['callback'])) {
          $value = call_user_func($col['callback'], $row);
        }

        $datavalue = @unserialize($value);
        if ($datavalue !== false) {
          if ($datavalue) {
            $prevalue = [];
            foreach ($datavalue as $d) {
              if ($d['label']) {
                $prevalue[] = $d['label'];
              }
            }
            if ($prevalue && count($prevalue)) {
              $value = implode(", ", $prevalue);
            }
          }
        }

        $html_content[] = $value;
      } //end foreach columns_table

      if ($this->button_table_action) :

        $button_action_style = $this->button_action_style;
        $html_content[] = "<div class='button_action' style='text-align:right'>" . view($this->view_action, compact('addaction', 'row', 'button_action_style', 'parent_field'))->render() . "</div>";
      //		$html_content[] = "<div class='button_action' style='text-align:right'>" . view('crudbooster::components.action', compact('addaction', 'row', 'button_action_style', 'parent_field'))->render() . "</div>";

      endif; //button_table_action

      foreach ($html_content as $i => $v) {
        $this->hook_row_index($i, $v);
        $html_content[$i] = $v;
      }

      $html_contents[] = $html_content;
    } //end foreach data[result]

    $html_contents = ['html' => $html_contents, 'data' => $data['result']];

    $data['html_contents'] = $html_contents;

    return view($this->view_index, $data);
  }

  public function getOrderTimeline($orderid)
  {

    $order_master = DB::table('order_master')
      ->where('id', $orderid)
      ->first();

    $data['data'] = $order_master;
    $data['page_title'] = 'Order Time Line';
    return view('order.timeline', $data);
  }

  public function getAssginOrderToRider()
  {
    //	echo "<pre>"; print_r(Request::all());die();

    $orderid = Request::get('orderid');
    $rider_id = Request::get('select_rider_id');

    $orderinfo = DB::table('order_master')->where('id', $orderid)->first();

    if (!is_numeric($orderid)) {
      CRUDBooster::redirect($_SERVER['HTTP_REFERER'], 'Vui lòng chọn đơn hàng !', "warning");
    }

    if (!is_numeric($rider_id)) {
      CRUDBooster::redirect($_SERVER['HTTP_REFERER'], 'Vui lòng chọn rider !', "warning");
    }

    if ($orderinfo->is_deleted) {
      CRUDBooster::redirect($_SERVER['HTTP_REFERER'], 'Order Not Valid For Assign !', "warning");
    }

    if ($orderinfo->status != 'Pending' && $orderinfo->status != 'Canceled') {
      CRUDBooster::redirect($_SERVER['HTTP_REFERER'], 'Order Already Has Rider !', "warning");
    }

    $url = env('API_URL');

    Log::info('error: ' . 'Assign Rider For Order ' . json_encode($orderinfo));
    if ($orderinfo->status != "Pending") {
      $ch = curl_init();
      $userToken = DB::table('user_session')
        ->select('token')
        ->where('userid', $orderinfo->userid)
        ->where('is_active', 1)
        ->orderBy('start_date', 'desc')
        ->first();
      $requestData = [
        "service" => "placeOrder",
        "auth" => [
          "token" => $userToken->token,
          "id" => $orderinfo->userid,
        ],
        "request" => [
          "data" => [
            "merchant_id" => $orderinfo->merchant_id,
            "cash_payment" => $orderinfo->cash_payment,
            "courier_service_name" => $orderinfo->courier_service_name,
            "delivery_address" => $orderinfo->delivery_address,
            "delivery_type" => $orderinfo->delivery_type,
            "description" => $orderinfo->description,
            "destination_address" => $orderinfo->destination_address,
            "dlat" => $orderinfo->dlat,
            "dllat" => $orderinfo->dllat,
            "dllng" => $orderinfo->dllng,
            "dlng" => $orderinfo->dlng,
            "images" => $orderinfo->images,
            "is_rain" => $orderinfo->is_rain,
            "lastorderid" => $orderinfo->id,
            "no_of_km" => $orderinfo->no_of_km,
            "ordertype" => $orderinfo->ordertype,
            "pickup_address" => $orderinfo->pickup_address,
            "pickup_city" => $orderinfo->pickup_city,
            "pickup_country" => $orderinfo->pickup_country,
            "pickup_postal" => $orderinfo->pickup_postal,
            "order_items" => $orderinfo->order_items,
            "order_id" => $orderinfo->merchant_order_id,
            "order_total" => $orderinfo->order_total,
            "extra" => $orderinfo->extra,
            "merchant_contact" => $orderinfo->merchant_contact,
            "customer_contact" => $orderinfo->customer_contact,
            "customer_contact_2" => $orderinfo->customer_contact_2,
            "pickup_state" => $orderinfo->pickup_state,
            "plat" => $orderinfo->plat,
            "platform" => $orderinfo->platform,
            "plng" => $orderinfo->plng,
            "promoamount" => $orderinfo->promoamount,
            "promocode" => $orderinfo->promocode,
            "recipient_name" => $orderinfo->recipient_name,
            "sender_name" => $orderinfo->sender_name,
            "tipamt" => $orderinfo->tipamt,
            "user_cash_payment" => $orderinfo->user_cash_payment,
            "userid" => $orderinfo->userid,
            "transaction_id" => $orderinfo->transaction_id
          ]
        ],
      ];
      Log::info('error: ' . 'Assign Rider Create Order Detail ' . json_encode($requestData));
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));

      $response = curl_exec($ch);
      curl_close($ch);

      $response = json_decode($response);
      Log::info('error: ' . 'Assign Rider Create Order Response ' . json_encode($response));
      if (!empty($response->success)) {
        $orderid = $response->orderid;
      } else {
        CRUDBooster::redirect($_SERVER['HTTP_REFERER'], !empty($response->message) ? $response->message : null, "warning");
      }
    }
    $token = DB::table('user_session')
      ->select('token')
      ->where('userid', $rider_id)
      ->where('is_active', 1)
      ->orderBy('start_date', 'desc')
      ->first();
    Log::info('error: ' . 'Assign Rider Token ' . json_encode($token));
    if (!empty($token->token)) {
      $ch = curl_init();
      $requestData = [
        "service" => "postbid",
        "request" => ["orderid" => $orderid],
        "auth" => [
          "token" => $token->token,
          "id" => $rider_id
        ]
      ];
      Log::info('error: ' . 'Assign Rider Detail ' . json_encode($requestData));

      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));

      $response = curl_exec($ch);
      curl_close($ch);

      $response = json_decode($response);
      Log::info('error: ' . 'Assign Rider Response ' . json_encode($response));
      if (!empty($response->success)) {
        CRUDBooster::redirect($_SERVER['HTTP_REFERER'], "Assign Rider Successfully", "success");
      } else {
        CRUDBooster::redirect($_SERVER['HTTP_REFERER'], !empty($response->message) ? $response->message : null, "warning");
      }
    } else {
      Log::info('error: ' . 'Assign Rider ID ' . $rider_id);
      CRUDBooster::redirect($_SERVER['HTTP_REFERER'], "Can't assign this rider.", "warning");
    }
  }

  public function getSomeThingInOrderMaTer()
  {
    $this->cbLoader();
    //$data=DB::table('order_master')->get()
    return view('order/getID');
  }

  public function getDetail($id)
  {
    $this->cbLoader();
    $row = DB::table($this->table)->where($this->primary_key, $id)->first();
    $permission = DB::table('cms_privileges_roles')->where([
      ['id_cms_privileges', CRUDBooster::myPrivilegeId()],
      ['id_cms_moduls', CRUDBooster::getCurrentModule()->id]
    ])->first();
    $permitOrdeny = $row->status != 'Complete' && $permission->is_edit ? true : false;
    $riders = DB::table('notification_master as n')
      ->select('n.isview', 'u.firstname', 'u.contactno', 'u.id')
      ->leftjoin('user_master as u', 'u.id', '=', 'n.riderid')
      ->where('n.order_id', $id)
      ->where('n.type', 'neworder')
      ->where('n.isbided', 0)
      ->get();

    if (!CRUDBooster::isRead() && $this->global_privilege == false || $this->button_detail == false) {
      CRUDBooster::insertLog(trans("crudbooster.log_try_view", [
        'name' => $row->{$this->title_field},
        'module' => CRUDBooster::getCurrentModule()->name,
      ]));
      CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));
    }

    $module = CRUDBooster::getCurrentModule();
    Session::put('current_row_id', $id);

    $data = [
      'object' => $row,
      'page_menu' => Route::getCurrentRoute()->getActionName(),
      'page_title' => trans("crudbooster.detail_data_page_title", ['module' => $module->name, 'name' => $row->{$this->title_field}]) . ' - ' . $row->merchant_order_id,
      'id' => $id,
      'riders' => $riders,
      'status' => $permitOrdeny
    ];

    if (!empty($row->rider_id)) {
      $rider = DB::table("user_master")->where("id", $row->rider_id)->first();
      $data['rider'] = $rider;
    }

    return view($this->view_detail, $data);
  }

  public function postCancelOrder()
  {
    $id = Request::post('order_id');
    $cancel_reason = Request::post('cancel_reason');
    $refind = Request::post('refind');
    $row = DB::table($this->table)->where($this->primary_key, $id)->first();
    if (!CRUDBooster::isRead() && $this->global_privilege == false || $this->button_detail == false) {
      CRUDBooster::insertLog(trans("crudbooster.log_try_view", [
        'name' => $row->{$this->title_field},
        'module' => CRUDBooster::getCurrentModule()->name,
      ]));
      CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));
    }
    $url = env('API_URL');
    $ch = curl_init();
    $requestData = [
      "service" => "admin_cancel_order",
      "auth" => [
        "token" => "finviet",
        "id" => 1,
      ],
      "request" => ["orderId" => $id, "reason" => $cancel_reason, "refind" => $refind],
    ];
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));

    $response = curl_exec($ch);
    curl_close($ch);

    $response = json_decode($response);
    return response()->json($response);
  }

  public function getCompleteOrder($id)
  {
    $this->cbLoader();
    $row = DB::table($this->table)->where($this->primary_key, $id)->first();

    if (!CRUDBooster::isRead() && $this->global_privilege == false || $this->button_detail == false) {
      CRUDBooster::insertLog(trans("crudbooster.log_try_view", [
        'name' => $row->{$this->title_field},
        'module' => CRUDBooster::getCurrentModule()->name,
      ]));
      CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));
    }

    $userToken = DB::table('user_session')
      ->select('token')
      ->where('userid', $row->rider_id)
      ->where('is_active', 1)
      ->orderBy('start_date', 'desc')
      ->first();
    $url = env('API_URL');
    $ch = curl_init();
    $requestData = [
      "service" => "give_feedback_to_user",
      "auth" => [
        "token" => $userToken->token,
        "id" => $row->rider_id,
      ],
      "request" => [
        "data" => [
          "riderid" => $row->rider_id,
          "comments" => "",
          "orderid" => $id,
          "overallexperience" => 5,
          "signature_image" => "698251481570681619.jpg",
          "communication" => 5,
          "userid" => $row->userid
        ]
      ],
    ];
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));

    $response = curl_exec($ch);
    curl_close($ch);

    $response = json_decode($response);
    if (!empty($response->success)) {
      CRUDBooster::redirect(action("AdminOrderController@getDetail", [$id]), "Cập nhật thành công", "success");
    } else {
      CRUDBooster::redirect(action("AdminOrderController@getDetail", [$id]), "Cập nhật thất bại", "warning");
    }
  }

  public function getDropOff($id, $is_return)
  {
    $this->cbLoader();
    $row = DB::table($this->table)->where($this->primary_key, $id)->first();

    if (!CRUDBooster::isRead() && $this->global_privilege == false || $this->button_detail == false) {
      CRUDBooster::insertLog(trans("crudbooster.log_try_view", [
        'name' => $row->{$this->title_field},
        'module' => CRUDBooster::getCurrentModule()->name,
      ]));
      CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));
    }

    $userToken = DB::table('user_session')
      ->select('token')
      ->where('userid', $row->rider_id)
      ->where('is_active', 1)
      ->orderBy('start_date', 'desc')
      ->first();
    $url = env('API_URL');
    $ch = curl_init();
    $requestData = [
      "service" => "dropoff",
      "auth" => [
        "token" => $userToken->token,
        "id" => $row->rider_id,
      ],
      "request" => [
        "riderid" => $row->rider_id,
        "orderid" => $id,
        "is_return" => $is_return
      ],
    ];
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));

    $response = curl_exec($ch);
    curl_close($ch);

    $response = json_decode($response);
    if (!empty($response->success)) {
      CRUDBooster::redirect(action("AdminOrderController@getDetail", [$id]), "Cập nhật thành công", "success");
    } else {
      CRUDBooster::redirect(action("AdminOrderController@getDetail", [$id]), "Cập nhật thất bại", "warning");
    }
  }

  public function getPickUp($id)
  {
    $this->cbLoader();
    $row = DB::table($this->table)->where($this->primary_key, $id)->first();

    if (!CRUDBooster::isRead() && $this->global_privilege == false || $this->button_detail == false) {
      CRUDBooster::insertLog(trans("crudbooster.log_try_view", [
        'name' => $row->{$this->title_field},
        'module' => CRUDBooster::getCurrentModule()->name,
      ]));
      CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));
    }

    $userToken = DB::table('user_session')
      ->select('token')
      ->where('userid', $row->rider_id)
      ->where('is_active', 1)
      ->orderBy('start_date', 'desc')
      ->first();
    $url = env('API_URL');
    $ch = curl_init();
    $requestData = [
      "service" => "repickupdone",
      "auth" => [
        "token" => $userToken->token,
        "id" => $row->rider_id,
      ],
      "request" => [
        "riderid" => $row->rider_id,
        "orderid" => $id,
      ],
    ];
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));

    $response = curl_exec($ch);
    curl_close($ch);

    $response = json_decode($response);
    if (!empty($response->success)) {
      CRUDBooster::redirect(action("AdminOrderController@getDetail", [$id]), "Cập nhật thành công", "success");
    } else {
      CRUDBooster::redirect(action("AdminOrderController@getDetail", [$id]), "Cập nhật thất bại", "warning");
    }
  }

  public function getStartOrder($id)
  {
    $this->cbLoader();
    $row = DB::table($this->table)->where($this->primary_key, $id)->first();

    if (!CRUDBooster::isRead() && $this->global_privilege == false || $this->button_detail == false) {
      CRUDBooster::insertLog(trans("crudbooster.log_try_view", [
        'name' => $row->{$this->title_field},
        'module' => CRUDBooster::getCurrentModule()->name,
      ]));
      CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));
    }

    $userToken = DB::table('user_session')
      ->select('token')
      ->where('userid', $row->rider_id)
      ->where('is_active', 1)
      ->orderBy('start_date', 'desc')
      ->first();
    $url = env('API_URL');
    $ch = curl_init();
    $requestData = [
      "service" => "startpickup",
      "auth" => [
        "token" => $userToken->token,
        "id" => $row->rider_id,
      ],
      "request" => [
        "orderid" => $id,
        "riderid" => $row->rider_id,
      ],
    ];
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));

    $response = curl_exec($ch);
    curl_close($ch);

    $response = json_decode($response);
    if (!empty($response->success)) {
      CRUDBooster::redirect(action("AdminOrderController@getDetail", [$id]), "Cập nhật thành công", "success");
    } else {
      CRUDBooster::redirect(action("AdminOrderController@getDetail", [$id]), "Cập nhật thất bại", "warning");
    }
  }
}
