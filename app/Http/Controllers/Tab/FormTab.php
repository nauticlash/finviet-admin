<?php

namespace App\Http\Controllers\Tab;

use Exception;

class FormTab
{
  private $listTab = [];
  private $tabData = [];
  private $activeIndex = 0;
  private $outsideController;
  private $viewDefault;

  public function __construct($outsideController, $viewDefault = null)
  {
    $this->outsideController = $outsideController;
    $this->viewDefault = $viewDefault ? $viewDefault : 'components.tabs';
  }

  /**
   * can set active tab by 
   */
  function getIndex()
  {
    $this->getTab();
    return view($this->viewDefault, $this->tabData);
  }

  /**
   * @param tabContents : include 
   * 
   * {
   * 
   *   @param tabName : tab name
   * 
   *   @param tabView : content include with tab , canbe string or view()
   * 
   *   @param apiUrl : route api
   * 
   *   @param apiController : only for active tab , must be ONLY
   * 
   * }
   */
  public function setTab(...$tabContents)
  {
    foreach ($tabContents as $key => $val) {
      array_push($this->listTab, $val);
    }
    return $this;
  }

  public function getTab()
  {
    return $this->handleTabContent();
  }

  /**
   * @param $tabIndex : int , start from 0
   */
  public function setActive($tabIndex = 0)
  {
    if (is_int($tabIndex)) {
      if ($tabIndex < 0) {
        $tabIndex = 0;
      } else if ($tabIndex > count($this->listTab) - 1) {
        $tabIndex = count($this->listTab) - 1;
      }
      $this->activeIndex = $tabIndex;
      return $this;
    } else {
      throw new Exception('tabIndex must be integer!');
    }
  }

  private function handleTabContent()
  {

    for ($i = 0; $i < count($this->listTab); $i++) {
      $this->listTab[$i]['tabHref'] = $i;
    }

    $this->listTab[$this->activeIndex]['tabActive'] = true;
    $func_active_name = $this->listTab[$this->activeIndex]['apiController'] ?? null;
    if ($func_active_name) {
      $call_func = $this->outsideController->$func_active_name();
      if ($call_func) {
        $this->listTab[$this->activeIndex]['defaultViewActive'] = $call_func['view'];
      }
    } else {
      $this->listTab[$this->activeIndex]['tabView'] ? 
      $this->listTab[$this->activeIndex]['defaultViewActive'] = $this->listTab[$this->activeIndex]['tabView'] : 
      $this->listTab[$this->activeIndex]['defaultViewActive'] ='<h3> Please add 
      <b style="color:red"> apiController </b> or 
      <b style="color:red"> tabView </b> 
      to view default active tab </h3>';

    }

    $this->tabData['listTab'] = $this->listTab;
    return $this->listTab;
  }
}
