<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Tab\FormTab;
use Session;
use Request;
use DB;
use CRUDBooster;
use Illuminate\Support\Facades\Route;
use Maatwebsite\Excel\Facades\Excel;

class AdminReportsController extends MyCBController
{

  public function cbInit()
  {

    $this->table = 'user_master';
    $this->primary_key = 'id';
    $this->title_field = "nama";
    $this->button_show = false;
    $this->button_new = false;
    $this->button_delete = false;
    $this->button_add = false;
    $this->button_import = false;
    $this->button_export = false;
  }

  public function getAllReport()
  {
    $this->cbLoader();
    $formTab = new FormTab(new AdminReportsController());
    $formTab->setTab(
      [
        'tabName' => 'Tổng tiền vận chuyển của lái xe',
        'apiUrl' => '/admin/reports/driver-report',
        'apiController' => 'getDriverReport',
      ]
      // [
      //   'tabName' => 'Khác',
      //   'tabView' => '<h1> HEllo </h1>'
      // ]
    );
    return $formTab->getIndex();
  }
  public function getDriverReport()
  {
    $pageParam = request('page') ?? 1;
    $limitParam = request('limit') ?? 10;
    $filterParam = request('filter');
    $exportExcel = request('exportExcel') ?? false;
    $select_start_date = request('select_start_date');
    $select_end_date = request('select_end_date');
    $start_date = date_create($select_start_date);
    $end_date = date_create($select_end_date);
    $where_start_date = date_format($start_date, "Y-m-d");
    $where_end_date = date_format($end_date, "Y-m-d");


    $query_exec_driver = DB::table('user_master as um')->select('um.id', DB::raw('
      IF( CONCAT(IFNULL(um.firstname, "") ," ",IFNULL(um.lastname, "")) != "" , CONCAT(IFNULL(um.firstname, "") ," ",IFNULL(um.lastname, "")) , um.contactno) as driver_name,
      um.contactno as driver_phone,
      IFNULL( SUM( om.shipping_amount ), 0 ) AS total_shipping_amount,
      IFNULL( SUM( om.no_of_km ), 0 ) AS total_km,
      SUM(IF( om.STATUS = "Complete", 1, 0 )) AS total_order_success,
      SUM(IF( om.STATUS = "Canceled", 1, 0 )) AS total_order_failed
      '))->leftJoin('order_master as om', 'om.rider_id', '=', 'um.id')->where('um.usertype', 'driver')->where(function ($query) use ($filterParam) {
      if ($filterParam != '') {
        return $query->orwhere('um.contactno', $filterParam)->orWhere('um.firstname', $filterParam)->orWhere('um.lastname', $filterParam);
      }
    })->where(function ($query) use ($select_start_date, $select_end_date, $where_start_date, $where_end_date) {
      if ($select_start_date != '' && $select_end_date != '') {
        return $query->where([
          ['om.created_at', '>=', $where_start_date . ' 00:00:00'],
          ['om.created_at', '<=', $where_end_date . ' 23:59:59']
        ]);
      }
      if ($select_start_date != '') {
        return $query->where('om.created_at', '>=', $where_start_date . ' 00:00:00');
      }
      if ($select_end_date != '') {
        return $query->where('om.created_at', '<=', $where_end_date . ' 23:59:59');
      }
    })->groupBy('um.id')->orderByDesc('total_shipping_amount');
    $driver_header = [
      __('rider.driver_name'),
      __('rider.driver_phone'),
      __('rider.driver_total_delivery_money'),
      __('rider.driver_total_km'),
      __('rider.driver_total_success_order'),
      __('rider.driver_total_failed_order'),
    ];
    $field_format = ['total_shipping_amount', 'total_km'];
    if ($exportExcel) {
      $appName = CRUDBooster::getSetting('appname');
      $data = $query_exec_driver->get()->toArray();
      $filename = 'Report '.date_format(now(),'d-m-Y');
      ob_end_clean();
      ob_start();
      return Excel::create($filename, function ($excel) use ($data, $filename, $appName ,$driver_header) {
        $excel->setTitle($filename)->setCreator($appName)->setCompany($appName);
        $excel->sheet($filename, function ($sheet) use ($data ,$driver_header) {
          $temp_arr[] = $driver_header;
          foreach ($data as $key => $val) {
            $val = (array) $val;
            unset($val['id']); // Bỏ id đi
            $temp_arr[] = array_values($val);
          }
          $sheet->fromArray($temp_arr, null, 'A1', false, false);
        });
      })->export('xlsx');
    }
    $total = $query_exec_driver->get()->count();
    $driver_infos = $query_exec_driver->limit($limitParam)->offset(($pageParam - 1) * $limitParam)->get();
    $perPage = $pageParam * $limitParam;
    $from = $total > 0 ? $perPage - $limitParam + 1 : 0;
    $to = $perPage < $total ? $perPage : $total;
    // Setup number of page
    $html_show = [];
    for ($i = 1; $i <= ceil($total / $limitParam); $i++) {
      array_push($html_show, $i);
    }
    return ['view' => view('driver.list_report', compact('driver_infos', 'driver_header', 'field_format', 'total', 'from', 'to', 'pageParam', 'limitParam', 'html_show'))->render()];
  }
}
