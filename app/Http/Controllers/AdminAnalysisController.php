<?php

namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use CRUDBooster;

class AdminAnalysisController extends MyCBController {

    public function cbInit() {

	# START CONFIGURATION DO NOT REMOVE THIS LINE
	$this->title_field = "sender_name";
	$this->limit = "20";
	$this->orderby = "id,desc";
	$this->global_privilege = false;
	$this->button_table_action = true;
	$this->button_bulk_action = FALSE;
	$this->button_action_style = "button_icon";
	$this->button_add = false;
	$this->button_edit = false;
	$this->button_delete = false;
	$this->button_detail = true;
	$this->button_show = false;
	$this->button_filter = false;
	$this->button_import = false;
	$this->button_export = false;
	$this->table = "order_master";
	# END CONFIGURATION DO NOT REMOVE THIS LINE
	# START COLUMNS DO NOT REMOVE THIS LINE
	$this->col = [];
	$this->col[] = ["label" => "Orderid", "name" => "orderid"];
	$this->col[] = ["label" => "Userid", "name" => "userid"];
	$this->col[] = ["label" => "Rider Id", "name" => "rider_id"];
	$this->col[] = ["label" => "Ordertype", "name" => "ordertype"];
	$this->col[] = ["label" => "User Cash Payment", "name" => "user_cash_payment"];
	$this->col[] = ["label" => "Other Amount", "name" => "other_amount"];
	$this->col[] = ["label" => "Cash Payment", "name" => "cash_payment"];
	# END COLUMNS DO NOT REMOVE THIS LINE
	# START FORM DO NOT REMOVE THIS LINE
	$this->form = [];
	$this->form[] = ['label' => 'Orderid', 'name' => 'orderid', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Userid', 'name' => 'userid', 'type' => 'number', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Rider Id', 'name' => 'rider_id', 'type' => 'select2', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10', 'datatable' => 'rider,id'];
	$this->form[] = ['label' => 'Ordertype', 'name' => 'ordertype', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
	# END FORM DO NOT REMOVE THIS LINE
	# OLD START FORM
	//$this->form = [];
	//$this->form[] = ["label"=>"Orderid","name"=>"orderid","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Userid","name"=>"userid","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
	//$this->form[] = ["label"=>"Rider Id","name"=>"rider_id","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"rider,id"];
	//$this->form[] = ["label"=>"Ordertype","name"=>"ordertype","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"User Cash Payment","name"=>"user_cash_payment","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Other Amount","name"=>"other_amount","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Cash Payment","name"=>"cash_payment","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Billamount","name"=>"billamount","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Change Received","name"=>"change_received","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Waiting Charge","name"=>"waiting_charge","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
	//$this->form[] = ["label"=>"Wallet Amount","name"=>"wallet_amount","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
	//$this->form[] = ["label"=>"Commission","name"=>"commission","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
	//$this->form[] = ["label"=>"Commission Percentage","name"=>"commission_percentage","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
	//$this->form[] = ["label"=>"Changestatus","name"=>"changestatus","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
	//$this->form[] = ["label"=>"Tips","name"=>"tips","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Distance","name"=>"distance","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
	//$this->form[] = ["label"=>"Total Amount","name"=>"total_amount","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Pickup Address","name"=>"pickup_address","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Plat","name"=>"plat","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Plng","name"=>"plng","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Destination Address","name"=>"destination_address","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
	//$this->form[] = ["label"=>"Dlat","name"=>"dlat","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Dlng","name"=>"dlng","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Delivery Address","name"=>"delivery_address","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Dllat","name"=>"dllat","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Dllng","name"=>"dllng","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Delivery Type","name"=>"delivery_type","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Description","name"=>"description","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
	//$this->form[] = ["label"=>"Images","name"=>"images","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
	//$this->form[] = ["label"=>"Promocode","name"=>"promocode","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Promoamount","name"=>"promoamount","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
	//$this->form[] = ["label"=>"Sender Name","name"=>"sender_name","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Recipient Name","name"=>"recipient_name","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Courier Service Name","name"=>"courier_service_name","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Status","name"=>"status","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Riderstatus","name"=>"riderstatus","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Is Rain","name"=>"is_rain","type"=>"radio","required"=>TRUE,"validation"=>"required|integer","dataenum"=>"Array"];
	//$this->form[] = ["label"=>"No Of Km","name"=>"no_of_km","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Pickup Time","name"=>"pickup_time","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
	//$this->form[] = ["label"=>"Arrive Pickup Time","name"=>"arrive_pickup_time","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
	//$this->form[] = ["label"=>"Is Pickup","name"=>"is_pickup","type"=>"radio","required"=>TRUE,"validation"=>"required|integer","dataenum"=>"Array"];
	//$this->form[] = ["label"=>"Is At Pickup","name"=>"is_at_pickup","type"=>"radio","required"=>TRUE,"validation"=>"required|integer","dataenum"=>"Array"];
	//$this->form[] = ["label"=>"Is Paid","name"=>"is_paid","type"=>"radio","required"=>TRUE,"validation"=>"required|integer","dataenum"=>"Array"];
	//$this->form[] = ["label"=>"Is Reached Destination","name"=>"is_reached_destination","type"=>"radio","required"=>TRUE,"validation"=>"required|integer","dataenum"=>"Array"];
	//$this->form[] = ["label"=>"Reached Destination Time","name"=>"reached_destination_time","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
	//$this->form[] = ["label"=>"At Destination","name"=>"at_destination","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
	//$this->form[] = ["label"=>"Is At Destination","name"=>"is_at_destination","type"=>"radio","required"=>TRUE,"validation"=>"required|integer","dataenum"=>"Array"];
	//$this->form[] = ["label"=>"Dropoff Time","name"=>"dropoff_time","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
	//$this->form[] = ["label"=>"Is Dropoff","name"=>"is_dropoff","type"=>"radio","required"=>TRUE,"validation"=>"required|integer","dataenum"=>"Array"];
	//$this->form[] = ["label"=>"Is At Dropoff","name"=>"is_at_dropoff","type"=>"radio","required"=>TRUE,"validation"=>"required|integer","dataenum"=>"Array"];
	//$this->form[] = ["label"=>"At Dropoff Time","name"=>"at_dropoff_time","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
	//$this->form[] = ["label"=>"Receipt","name"=>"receipt","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
	//$this->form[] = ["label"=>"Signature Image","name"=>"signature_image","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
	//$this->form[] = ["label"=>"Orderdate","name"=>"orderdate","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
	//$this->form[] = ["label"=>"Admincancel","name"=>"admincancel","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
	//$this->form[] = ["label"=>"Platform","name"=>"platform","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Adminedit","name"=>"adminedit","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
	//$this->form[] = ["label"=>"Cancelby","name"=>"cancelby","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Is Deleted","name"=>"is_deleted","type"=>"radio","required"=>TRUE,"validation"=>"required|integer","dataenum"=>"Array"];
	//$this->form[] = ["label"=>"User Deleted","name"=>"user_deleted","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
	//$this->form[] = ["label"=>"Rider Deleted","name"=>"rider_deleted","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
	//$this->form[] = ["label"=>"Delivery Time","name"=>"delivery_time","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
	//$this->form[] = ["label"=>"Modifieddate","name"=>"modifieddate","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
	//$this->form[] = ["label"=>"Pickup City","name"=>"pickup_city","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Pickup State","name"=>"pickup_state","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Pickup Country","name"=>"pickup_country","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Pickup Postal","name"=>"pickup_postal","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Drop City","name"=>"drop_city","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Drop State","name"=>"drop_state","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Drop Country","name"=>"drop_country","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Drop Postal","name"=>"drop_postal","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Order Items","name"=>"order_items","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
	//$this->form[] = ["label"=>"Extra","name"=>"extra","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
	//$this->form[] = ["label"=>"Merchant Order Id","name"=>"merchant_order_id","type"=>"select2","required"=>TRUE,"validation"=>"required|min:1|max:255","datatable"=>"merchant_order,id"];
	//$this->form[] = ["label"=>"Merchant Contact","name"=>"merchant_contact","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Order Total","name"=>"order_total","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Customer Contact","name"=>"customer_contact","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Customer Contact 2","name"=>"customer_contact_2","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Cancel Reason","name"=>"cancel_reason","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
	//$this->form[] = ["label"=>"Alert Pending","name"=>"alert_pending","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Alert Pickup","name"=>"alert_pickup","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Alert Dropoff","name"=>"alert_dropoff","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Last Alert Pending","name"=>"last_alert_pending","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
	//$this->form[] = ["label"=>"Last Alert Pickup","name"=>"last_alert_pickup","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
	//$this->form[] = ["label"=>"Last Alert Dropoff","name"=>"last_alert_dropoff","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
	//$this->form[] = ["label"=>"Created By","name"=>"created_by","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
	//$this->form[] = ["label"=>"Updated By","name"=>"updated_by","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
	# OLD END FORM

	/*
	  | ----------------------------------------------------------------------
	  | Sub Module
	  | ----------------------------------------------------------------------
	  | @label          = Label of action
	  | @path           = Path of sub module
	  | @foreign_key 	  = foreign key of sub table/module
	  | @button_color   = Bootstrap Class (primary,success,warning,danger)
	  | @button_icon    = Font Awesome Class
	  | @parent_columns = Sparate with comma, e.g : name,created_at
	  |
	 */
	$this->sub_module = array();


	/*
	  | ----------------------------------------------------------------------
	  | Add More Action Button / Menu
	  | ----------------------------------------------------------------------
	  | @label       = Label of action
	  | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	  | @icon        = Font awesome class icon. e.g : fa fa-bars
	  | @color 	   = Default is primary. (primary, warning, succecss, info)
	  | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	  |
	 */
	$this->addaction = array();


	/*
	  | ----------------------------------------------------------------------
	  | Add More Button Selected
	  | ----------------------------------------------------------------------
	  | @label       = Label of action
	  | @icon 	   = Icon from fontawesome
	  | @name 	   = Name of button
	  | Then about the action, you should code at actionButtonSelected method
	  |
	 */
	$this->button_selected = array();


	/*
	  | ----------------------------------------------------------------------
	  | Add alert message to this module at overheader
	  | ----------------------------------------------------------------------
	  | @message = Text of message
	  | @type    = warning,success,danger,info
	  |
	 */
	$this->alert = array();



	/*
	  | ----------------------------------------------------------------------
	  | Add more button to header button
	  | ----------------------------------------------------------------------
	  | @label = Name of button
	  | @url   = URL Target
	  | @icon  = Icon from Awesome.
	  |
	 */
	$this->index_button = array();



	/*
	  | ----------------------------------------------------------------------
	  | Customize Table Row Color
	  | ----------------------------------------------------------------------
	  | @condition = If condition. You may use field alias. E.g : [id] == 1
	  | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
	  |
	 */
	$this->table_row_color = array();


	/*
	  | ----------------------------------------------------------------------
	  | You may use this bellow array to add statistic at dashboard
	  | ----------------------------------------------------------------------
	  | @label, @count, @icon, @color
	  |
	 */
	$this->index_statistic = array();



	/*
	  | ----------------------------------------------------------------------
	  | Add javascript at body
	  | ----------------------------------------------------------------------
	  | javascript code in the variable
	  | $this->script_js = "function() { ... }";
	  |
	 */
	$this->script_js = NULL;


	/*
	  | ----------------------------------------------------------------------
	  | Include HTML Code before index table
	  | ----------------------------------------------------------------------
	  | html code to display it before index table
	  | $this->pre_index_html = "<p>test</p>";
	  |
	 */
	$this->pre_index_html = null;



	/*
	  | ----------------------------------------------------------------------
	  | Include HTML Code after index table
	  | ----------------------------------------------------------------------
	  | html code to display it after index table
	  | $this->post_index_html = "<p>test</p>";
	  |
	 */
	$this->post_index_html = null;



	/*
	  | ----------------------------------------------------------------------
	  | Include Javascript File
	  | ----------------------------------------------------------------------
	  | URL of your javascript each array
	  | $this->load_js[] = asset("myfile.js");
	  |
	 */
	$this->load_js = array();



	/*
	  | ----------------------------------------------------------------------
	  | Add css style at body
	  | ----------------------------------------------------------------------
	  | css code in the variable
	  | $this->style_css = ".style{....}";
	  |
	 */
	$this->style_css = NULL;



	/*
	  | ----------------------------------------------------------------------
	  | Include css File
	  | ----------------------------------------------------------------------
	  | URL of your css each array
	  | $this->load_css[] = asset("myfile.css");
	  |
	 */
	$this->load_css = array();
    }

    public function getShow() {
	$this->cbLoader();

	$module = CRUDBooster::getCurrentModule();

	if (!CRUDBooster::isView() && $this->global_privilege == false) {
	    CRUDBooster::insertLog(trans('crudbooster.log_try_view', ['module' => $module->name]));
	    CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));
	}

	$select_start_date = Request::get('select_start_date');
	$select_end_date = Request::get('select_end_date');

	if (empty($select_start_date)) {
            $select_start_date = date("Y-m-01");
        }
	if (empty($select_end_date)) {
            $select_end_date = date("Y-m-d");
        }
	$start_date = date_create($select_start_date);
	$end_date = date_create($select_end_date);
	$where_start_date = date_format($start_date, "Y-m-d");
	$where_end_date = date_format($end_date, "Y-m-d");

//	echo $select_start_date . ' - ' . $select_end_date; die();

	$data['select_start_date'] = date_format($start_date, "d-m-Y");
	$data['select_end_date'] = date_format($end_date, "d-m-Y");
	$data['where_start_date'] = $where_start_date;
	$data['where_end_date'] = $where_end_date;
	$data['page_title'] = ($module->translate_code) ? trans($module->translate_code) : $module->name;

	return view('analysis.show', $data);
    }
}
