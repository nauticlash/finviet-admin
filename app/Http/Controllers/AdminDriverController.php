<?php

namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use CRUDBooster;
use CB;
use Schema;
use Config;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Input;

class AdminDriverController extends MyCBController
{

  public $table = 'user_master';
  public $view_table = "driver.table";

  public function cbInit()
  {

    # START CONFIGURATION DO NOT REMOVE THIS LINE
    $this->title_field = "firstname";
    $this->limit = "20";
    $this->orderby = "id,desc";
    $this->global_privilege = false;
    $this->button_table_action = true;
    $this->button_bulk_action = FALSE;
    $this->button_action_style = "button_icon";
    $this->button_add = FALSE;
    $this->button_edit = FALSE;
    $this->button_delete = FALSE;
    $this->button_detail = true;
    $this->button_show = FALSE;
    $this->button_filter = FALSE;
    $this->button_import = false;
    $this->button_export = true;
    $this->table = "user_master";
    $this->query_cols = ["firstname"];


    // vinhth add code
    $this->view_index_top = "driver.index_top";
    $this->view_detail = "driver.form_detail";
    $this->view_edit = "driver.form_edit";
    $this->view_table = "default.table";

    # END CONFIGURATION DO NOT REMOVE THIS LINE

    # START COLUMNS DO NOT REMOVE THIS LINE
    $this->col = [];
    $this->col[] = ["label" => "Id", "name" => "id", "visible" => false];
    $this->col[] = ["label" => "Profile", "name" => "profilepic", "visible" => false];
    $this->col[] = ["label" => trans("rider.info"), "name" => "contactno", "sortable" => false, "callback_php" => 'get_html_rider_info($row)'];
    $this->col[] = ["label" => trans('rider.registered_on'), "name" => "created_at", "callback_php" => 'get_formatted_date($row->created_at)'];
    $this->col[] = ["label" => trans('rider.verify_otp'), "name" => "is_verify", "callback_php" => 'get_format_col_is_verify($row->id, $row->is_verify);'];
    $this->col[] = ["label" => trans('rider.last_update_gps'), "name" => "last_location_updated", "callback_php" => 'get_last_formatted_date($row->last_location_updated)'];
    $this->col[] = ["label" => trans('rider.online_status'), "name" => "online_status", "callback_php" => 'get_format_online_offline($row->online_status);'];
    # END COLUMNS DO NOT REMOVE THIS LINE

    # START FORM DO NOT REMOVE THIS LINE
    $width_label = 'col-sm-12';
    $width = 'col-sm-12';
    $style = 'width:33%; display:inline-block';

    $this->form = [];
    $this->form[] = ['label' => trans("crudbooster.field_firstname"), 'name' => 'firstname', 'type' => 'text', 'validation' => 'required', 'width_label' => $width_label, 'width' => $width, 'style' => $style];
    $this->form[] = ['label' => trans("crudbooster.field_lastname"), 'name' => 'lastname', 'type' => 'text', 'validation' => 'required', 'width_label' => $width_label, 'width' => $width, 'style' => $style];
    $this->form[] = ['label' => trans("crudbooster.field_phone_number"), 'name' => 'contactno', 'type' => 'number', 'validation' => 'required', 'width_label' => $width_label, 'width' => $width, 'style' => $style];
    $this->form[] = ['label' => 'Email', 'name' => 'email', 'type' => 'email', 'validation' => 'required', 'width_label' => $width_label, 'width' => $width, 'style' => $style];
    $this->form[] = ['label' => trans("crudbooster.field_address"), 'name' => 'address', 'type' => 'text', 'validation' => 'required', 'width_label' => $width_label, 'width' => $width, 'style' => $style];
    $this->form[] = ['label' => trans("crudbooster.field_city"), 'name' => 'city', 'type' => 'text', 'validation' => 'required', 'width_label' => $width_label, 'width' => $width, 'style' => $style];
    $this->form[] = ['label' => trans("crudbooster.field_status"), 'name' => 'status', 'type' => 'radio', 'value' => 0, 'validation' => 'required|min:0', 'dataenum' => \CommonData::$USER_MASTER_STATUS, 'width_label' => $width_label, 'width' => $width, 'style' => $style, 'class_radio_value' => 'display:inline-block; margin-right:10px;'];
    $this->form[] = ['label' => trans("crudbooster.field_is_verify"), 'name' => 'is_verify', 'type' => 'radio', 'value' => 0, 'validation' => 'required|min:0', 'dataenum' => \CommonData::$USER_MASTER_IS_VERIFY, 'width_label' => $width_label, 'width' => $width, 'style' => $style, 'class_radio_value' => 'display:inline-block; margin-right:10px;'];
    $this->form[] = ["label" => "Avatar", "name" => "profilepic", "type" => "upload", "help" => "Recommended resolution is 200x200px", 'validation' => 'image|max:1000', 'width_label' => $width_label, 'width' => $width, 'style' => $style];

    /*
          | ----------------------------------------------------------------------
          | Sub Module
          | ----------------------------------------------------------------------
          | @label          = Label of action
          | @path           = Path of sub module
          | @foreign_key 	  = foreign key of sub table/module
          | @button_color   = Bootstrap Class (primary,success,warning,danger)
          | @button_icon    = Font Awesome Class
          | @parent_columns = Sparate with comma, e.g : name,created_at
          |
         */
    $this->sub_module = array();


    /*
          | ----------------------------------------------------------------------
          | Add More Action Button / Menu
          | ----------------------------------------------------------------------
          | @label       = Label of action
          | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
          | @icon        = Font awesome class icon. e.g : fa fa-bars
          | @color 	   = Default is primary. (primary, warning, succecss, info)
          | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
          |
         */
    $this->addaction = array();

    /*
          | ----------------------------------------------------------------------
          | Add More Button Selected
          | ----------------------------------------------------------------------
          | @label       = Label of action
          | @icon 	   = Icon from fontawesome
          | @name 	   = Name of button
          | Then about the action, you should code at actionButtonSelected method
          |
         */
    $this->button_selected = array();


    /*
          | ----------------------------------------------------------------------
          | Add alert message to this module at overheader
          | ----------------------------------------------------------------------
          | @message = Text of message
          | @type    = warning,success,danger,info
          |
         */
    $this->alert = array();


    /*
          | ----------------------------------------------------------------------
          | Add more button to header button
          | ----------------------------------------------------------------------
          | @label = Name of button
          | @url   = URL Target
          | @icon  = Icon from Awesome.
          |
         */
    $this->index_button = array();
    $this->index_button[] = [
      "label" => trans("rider.rider_on_map"),
      "icon" => "fa fa-map",
      "url" => CRUDBooster::mainpath('rider-on-map'),
      "color" => "danger"
    ];


    /*
          | ----------------------------------------------------------------------
          | Customize Table Row Color
          | ----------------------------------------------------------------------
          | @condition = If condition. You may use field alias. E.g : [id] == 1
          | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
          |
         */
    $this->table_row_color = array();


    /*
          | ----------------------------------------------------------------------
          | You may use this bellow array to add statistic at dashboard
          | ----------------------------------------------------------------------
          | @label, @count, @icon, @color
          |
         */
    $this->index_statistic = array();


    /*
          | ----------------------------------------------------------------------
          | Add javascript at body
          | ----------------------------------------------------------------------
          | javascript code in the variable
          | $this->script_js = "function() { ... }";
          |
         */
    $this->script_js = NULL;

    /*
          | ----------------------------------------------------------------------
          | Include HTML Code before index table
          | ----------------------------------------------------------------------
          | html code to display it before index table
          | $this->pre_index_html = "<p>test</p>";
          |
         */
    $this->pre_index_html = null;


    /*
          | ----------------------------------------------------------------------
          | Include HTML Code after index table
          | ----------------------------------------------------------------------
          | html code to display it after index table
          | $this->post_index_html = "<p>test</p>";
          |
         */
    $this->post_index_html = null;


    /*
          | ----------------------------------------------------------------------
          | Include Javascript File
          | ----------------------------------------------------------------------
          | URL of your javascript each array
          | $this->load_js[] = asset("myfile.js");
          |
         */
    $this->load_js = array();
    $this->load_js[] = asset("assets/js/driver.js?v=" . Config::get('constants.VERSION_FILE'));


    /*
          | ----------------------------------------------------------------------
          | Add css style at body
          | ----------------------------------------------------------------------
          | css code in the variable
          | $this->style_css = ".style{....}";
          |
         */
    $this->style_css = NULL;


    /*
          | ----------------------------------------------------------------------
          | Include css File
          | ----------------------------------------------------------------------
          | URL of your css each array
          | $this->load_css[] = asset("myfile.css");
          |
         */
    $this->load_css = array();
    $this->load_css[] = asset("assets/css/style.css?v=" . Config::get('constants.VERSION_FILE'));
    if (CRUDBooster::getCurrentMethod() == "getDetail") {
      $this->load_css[] = asset("assets/css/popup_modal.css?v=" . Config::get('constants.VERSION_FILE'));
    }
  }

  /*
      | ----------------------------------------------------------------------
      | Hook for button selected
      | ----------------------------------------------------------------------
      | @id_selected = the id selected
      | @button_name = the name of button
      |
     */

  public function actionButtonSelected($id_selected, $button_name)
  {
    //Your code here
  }

  /*
      | ----------------------------------------------------------------------
      | Hook for manipulate query of index result
      | ----------------------------------------------------------------------
      | @query = current sql query
      |
     */

  public function hook_query_index(&$query)
  {
    $query->where('usertype', 'driver');
  }

  /*
      | ----------------------------------------------------------------------
      | Hook for manipulate row of index table html
      | ----------------------------------------------------------------------
      |
     */

  public function hook_row_index($index, &$value)
  {
  }

  /*
      | ----------------------------------------------------------------------
      | Hook for manipulate data input before add data is execute
      | ----------------------------------------------------------------------
      | @arr
      |
     */

  public function hook_before_add(&$postdata)
  {
    //Your code here
  }

  /*
      | ----------------------------------------------------------------------
      | Hook for execute command after add public static function called
      | ----------------------------------------------------------------------
      | @id = last insert id
      |
     */

  public function hook_after_add($id)
  {
    //Your code here
  }

  /*
      | ----------------------------------------------------------------------
      | Hook for manipulate data input before update data is execute
      | ----------------------------------------------------------------------
      | @postdata = input post data
      | @id       = current id
      |
     */

  public function hook_before_edit(&$postdata, $id)
  {
    //Your code here
  }

  /*
      | ----------------------------------------------------------------------
      | Hook for execute command after edit public static function called
      | ----------------------------------------------------------------------
      | @id       = current id
      |
     */

  public function hook_after_edit($id)
  {
    //Your code here
  }

  /*
      | ----------------------------------------------------------------------
      | Hook for execute command before delete public static function called
      | ----------------------------------------------------------------------
      | @id       = current id
      |
     */

  public function hook_before_delete($id)
  {
    //Your code here
  }

  /*
      | ----------------------------------------------------------------------
      | Hook for execute command after delete public static function called
      | ----------------------------------------------------------------------
      | @id       = current id
      |
     */

  public function hook_after_delete($id)
  {
    //Your code here
  }

  public function getSetVerify($userid, $is_verify)
  {
    $message = ($is_verify == 1) ? trans('crudbooster.AUTHENTICED') . ' ' . $userid : trans('crudbooster.NO_AUTHENTICATION') . ' ' . $userid;
    $color = ($is_verify == 1) ? 'success' : 'danger';
    DB::table($this->table)->where('id', $userid)->update([
      'is_verify' => $is_verify,
      'updated_at' => date('Y-m-d H:i:s'),
      'updated_by' => CRUDBooster::myId()
    ]);
    CRUDBooster::redirect($_SERVER['HTTP_REFERER'], $message, $color);
  }

  public function getIndex()
  {
    $this->cbLoader();
    $driver = DB::Table('user_master')->where('Usertype', 'LIKE', 'driver')->GET();
    $module = CRUDBooster::getCurrentModule();

    if (!CRUDBooster::isView() && $this->global_privilege == false) {
      CRUDBooster::insertLog(trans('crudbooster.log_try_view', ['module' => $module->name]));
      CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));
    }

    if (Request::get('parent_table')) {
      $parentTablePK = CB::pk(g('parent_table'));
      $data['parent_table'] = DB::table(Request::get('parent_table'))->where($parentTablePK, Request::get('parent_id'))->first();
      if (Request::get('foreign_key')) {
        $data['parent_field'] = Request::get('foreign_key');
      } else {
        $data['parent_field'] = CB::getTableForeignKey(g('parent_table'), $this->table);
      }

      if ($parent_field) {
        foreach ($this->columns_table as $i => $col) {
          if ($col['name'] == $parent_field) {
            unset($this->columns_table[$i]);
          }
        }
      }
    }

    $data['table'] = $this->table;
    $data['table_pk'] = CB::pk($this->table);

    //	echo "<pre>"; print_r($module);die();
    $data['page_title'] = ($module->translate_code) ? trans($module->translate_code) : $module->name;


    $data['page_description'] = trans('crudbooster.default_module_description');
    $data['date_candidate'] = $this->date_candidate;
    $data['limit'] = $limit = (Request::get('limit')) ? Request::get('limit') : $this->limit;

    $tablePK = $data['table_pk'];
    $table_columns = CB::getTableColumns($this->table);
    $result = DB::table($this->table)->select(DB::raw($this->table . "." . $this->primary_key));

    if (Request::get('parent_id')) {
      $table_parent = $this->table;
      $table_parent = CRUDBooster::parseSqlTable($table_parent)['table'];
      $result->where($table_parent . '.' . Request::get('foreign_key'), Request::get('parent_id'));
    }

    $this->hook_query_index($result);

    if (in_array('deleted_at', $table_columns)) {
      $result->where($this->table . '.deleted_at', null);
    }

    $alias = [];
    $join_alias_count = 0;
    $join_table_temp = [];
    $table = $this->table;
    $columns_table = $this->columns_table;
    foreach ($columns_table as $index => $coltab) {

      $join = @$coltab['join'];
      $join_where = @$coltab['join_where'];
      $join_id = @$coltab['join_id'];
      $field = @$coltab['name'];
      $join_table_temp[] = $table;

      if (!$field) {
        continue;
      }

      if (strpos($field, ' as ') !== false) {
        $field = substr($field, strpos($field, ' as ') + 4);
        $field_with = (array_key_exists('join', $coltab)) ? str_replace(",", ".", $coltab['join']) : $field;
        $result->addselect(DB::raw($coltab['name']));
        $columns_table[$index]['type_data'] = 'varchar';
        $columns_table[$index]['field'] = $field;
        $columns_table[$index]['field_raw'] = $field;
        $columns_table[$index]['field_with'] = $field_with;
        $columns_table[$index]['is_subquery'] = true;
        continue;
      }

      if (strpos($field, '.') !== false) {
        $result->addselect($field);
      } else {
        $result->addselect($table . '.' . $field);
      }

      $field_array = explode('.', $field);

      if (isset($field_array[1])) {
        $field = $field_array[1];
        $table = $field_array[0];
      } else {
        $table = $this->table;
      }

      if ($join) {

        $join_exp = explode(',', $join);

        $join_table = $join_exp[0];
        $joinTablePK = CB::pk($join_table);
        $join_column = $join_exp[1];
        $join_alias = str_replace(".", "_", $join_table);

        if (in_array($join_table, $join_table_temp)) {
          $join_alias_count += 1;
          $join_alias = $join_table . $join_alias_count;
        }
        $join_table_temp[] = $join_table;

        $result->leftjoin($join_table . ' as ' . $join_alias, $join_alias . (($join_id) ? '.' . $join_id : '.' . $joinTablePK), '=', DB::raw($table . '.' . $field . (($join_where) ? ' AND ' . $join_where . ' ' : '')));
        $result->addselect($join_alias . '.' . $join_column . ' as ' . $join_alias . '_' . $join_column);

        $join_table_columns = CRUDBooster::getTableColumns($join_table);
        if ($join_table_columns) {
          foreach ($join_table_columns as $jtc) {
            $result->addselect($join_alias . '.' . $jtc . ' as ' . $join_alias . '_' . $jtc);
          }
        }

        $alias[] = $join_alias;
        $columns_table[$index]['type_data'] = CRUDBooster::getFieldType($join_table, $join_column);
        $columns_table[$index]['field'] = $join_alias . '_' . $join_column;
        $columns_table[$index]['field_with'] = $join_alias . '.' . $join_column;
        $columns_table[$index]['field_raw'] = $join_column;

        @$join_table1 = $join_exp[2];
        @$joinTable1PK = CB::pk($join_table1);
        @$join_column1 = $join_exp[3];
        @$join_alias1 = $join_table1;

        if ($join_table1 && $join_column1) {

          if (in_array($join_table1, $join_table_temp)) {
            $join_alias_count += 1;
            $join_alias1 = $join_table1 . $join_alias_count;
          }

          $join_table_temp[] = $join_table1;

          $result->leftjoin($join_table1 . ' as ' . $join_alias1, $join_alias1 . '.' . $joinTable1PK, '=', $join_alias . '.' . $join_column);
          $result->addselect($join_alias1 . '.' . $join_column1 . ' as ' . $join_column1 . '_' . $join_alias1);
          $alias[] = $join_alias1;
          $columns_table[$index]['type_data'] = CRUDBooster::getFieldType($join_table1, $join_column1);
          $columns_table[$index]['field'] = $join_column1 . '_' . $join_alias1;
          $columns_table[$index]['field_with'] = $join_alias1 . '.' . $join_column1;
          $columns_table[$index]['field_raw'] = $join_column1;
        }
      } else {

        if (isset($field_array[1])) {
          $result->addselect($table . '.' . $field . ' as ' . $table . '_' . $field);
          $columns_table[$index]['type_data'] = CRUDBooster::getFieldType($table, $field);
          $columns_table[$index]['field'] = $table . '_' . $field;
          $columns_table[$index]['field_raw'] = $table . '.' . $field;
        } else {
          $result->addselect($table . '.' . $field);
          $columns_table[$index]['type_data'] = CRUDBooster::getFieldType($table, $field);
          $columns_table[$index]['field'] = $field;
          $columns_table[$index]['field_raw'] = $field;
        }

        $columns_table[$index]['field_with'] = $table . '.' . $field;
      }
    }

    // vinhth add code =====================================================
    $select_online_status = Request::get('select_online_status');
    $select_isfree = Request::get('select_isfree');
    $select_is_verify = Request::get('select_is_verify');
    $select_is_contactno = Request::get('select_is_contactno');
    $select_merchants_id = Request::get('select_merchants_id');
    $select_start_date = Request::get('select_start_date');
    $select_end_date = Request::get('select_end_date');
    $result->addselect($this->table . '.firstname');

    if (isset($select_online_status) && !empty($select_online_status)) {
      $result->where($this->table . '.online_status', $select_online_status);
      $data['select_online_status'] = $select_online_status;
      //	    echo $data['select_online_status'];die();
    }

    if (isset($select_isfree) && !empty($select_isfree)) {
      $result->where($this->table . '.isfree', $select_isfree);
      $data['select_isfree'] = $select_isfree;
    }

    if (isset($select_is_verify) || $select_is_verify == 0 && !empty($select_is_verify)) {
      $result->where($this->table . '.is_verify', $select_is_verify);
      $data['select_is_verify'] = $select_is_verify;
    }
    if (isset($select_is_contactno) && !empty($select_is_contactno)) {
      $result->where($this->table . '.contactno', 'LIKE', "%$select_is_contactno%");
      $data['select_is_contactno'] = $select_is_contactno;
    }
    $start_date = date_create($select_start_date);
    $end_date = date_create($select_end_date);
    $where_start_date = date_format($start_date, "Y-m-d");
    $where_end_date = date_format($end_date, "Y-m-d");

    if (!empty($select_start_date)) {
      $result->where($this->table . '.createdate', '>', $where_start_date . ' 00:00:00');
      $data['select_start_date'] = date_format($start_date, "d-m-Y");
    }

    if (!empty($select_end_date)) {
      $result->where($this->table . '.createdate', '<=', $where_end_date . ' 23:59:59');
      $data['select_end_date'] = date_format($end_date, "d-m-Y");
    }


    $str_online_status = \CommonData::$USER_MASTER_ONLINE_STATUS;
    $str_isfree = \CommonData::$USER_MASTER_ISFREE;
    $str_is_verify = \CommonData::$USER_MASTER_IS_VERIFY;
    $str_is_contactno = \CommonData::$USER_MASTER_IS_CONTACTNO;

    $data['array_online_status'] = create_array_key_value_from_string_common_data($str_online_status);
    $data['array_isfree'] = create_array_key_value_from_string_common_data($str_isfree);
    $data['array_is_verify'] = create_array_key_value_from_string_common_data($str_is_verify);
    $data['array_is_contactno'] = DB::table('user_master')->where('contactno', '<>', '')->get();

    $data['view_index_top'] = $this->view_index_top;
    $data['view_table'] = $this->view_table;
    // end vinhth add code =================================================


    if (Request::get('q')) {
        $query_cols = $this->query_cols;
      $result->where(function ($w) use ($query_cols) {
        foreach ($query_cols as $col) {
          $w->orwhere($col, "like", "%" . Request::get("q") . "%");
        }
      });
    }

    if (Request::get('where')) {
      foreach (Request::get('where') as $k => $v) {
        $result->where($table . '.' . $k, $v);
      }
    }

    $filter_is_orderby = false;
    if (Request::get('filter_column')) {

      $filter_column = Request::get('filter_column');
      $result->where(function ($w) use ($filter_column, $fc) {
        foreach ($filter_column as $key => $fc) {

          $value = @$fc['value'];
          $type = @$fc['type'];

          if ($type == 'empty') {
            $w->whereNull($key)->orWhere($key, '');
            continue;
          }

          if ($value == '' || $type == '') {
            continue;
          }

          if ($type == 'between') {
            continue;
          }

          switch ($type) {
            default:
              if ($key && $type && $value) {
                $w->where($key, $type, $value);
              }
              break;
            case 'like':
            case 'not like':
              $value = '%' . $value . '%';
              if ($key && $type && $value) {
                $w->where($key, $type, $value);
              }
              break;
            case 'in':
            case 'not in':
              if ($value) {
                $value = explode(',', $value);
                if ($key && $value) {
                  $w->whereIn($key, $value);
                }
              }
              break;
          }
        }
      });

      foreach ($filter_column as $key => $fc) {
        $value = @$fc['value'];
        $type = @$fc['type'];
        $sorting = @$fc['sorting'];

        if ($sorting != '') {
          if ($key) {
            $result->orderby($key, $sorting);
            $filter_is_orderby = true;
          }
        }

        if ($type == 'between') {
          if ($key && $value) {
            $result->whereBetween($key, $value);
          }
        } else {
          continue;
        }
      }
    }

    if ($filter_is_orderby == true) {
      $data['result'] = $result->paginate($limit);
    } else {
      if ($this->orderby) {
        if (is_array($this->orderby)) {
          foreach ($this->orderby as $k => $v) {
            if (strpos($k, '.') !== false) {
              $orderby_table = explode(".", $k)[0];
              $k = explode(".", $k)[1];
            } else {
              $orderby_table = $this->table;
            }
            $result->orderby($orderby_table . '.' . $k, $v);
          }
        } else {
          $this->orderby = explode(";", $this->orderby);
          foreach ($this->orderby as $o) {
            $o = explode(",", $o);
            $k = $o[0];
            $v = $o[1];
            if (strpos($k, '.') !== false) {
              $orderby_table = explode(".", $k)[0];
            } else {
              $orderby_table = $this->table;
            }
            $result->orderby($orderby_table . '.' . $k, $v);
          }
        }
        $data['result'] = $result->paginate($limit);
      } else {
        $data['result'] = $result->orderby($this->table . '.' . $this->primary_key, 'desc')->paginate($limit);
      }
    }

    $data['columns'] = $columns_table;

    if ($this->index_return) {
      return $data;
    }

    //LISTING INDEX HTML
    $addaction = $this->data['addaction'];

    if ($this->sub_module) {
      foreach ($this->sub_module as $s) {
        $table_parent = CRUDBooster::parseSqlTable($this->table)['table'];
        $addaction[] = [
          'label' => $s['label'],
          'icon' => $s['button_icon'],
          'url' => CRUDBooster::adminPath($s['path']) . '?return_url=' . urlencode(Request::fullUrl()) . '&parent_table=' . $table_parent . '&parent_columns=' . $s['parent_columns'] . '&parent_columns_alias=' . $s['parent_columns_alias'] . '&parent_id=[' . (!isset($s['custom_parent_id']) ? "id" : $s['custom_parent_id']) . ']&foreign_key=' . $s['foreign_key'] . '&label=' . urlencode($s['label']),
          'color' => $s['button_color'],
          'showIf' => $s['showIf'],
        ];
      }
    }

    $mainpath = CRUDBooster::mainpath();
    $orig_mainpath = $this->data['mainpath'];
    $title_field = $this->title_field;
    $html_contents = [];
    $page = (Request::get('page')) ? Request::get('page') : 1;
    $number = ($page - 1) * $limit + 1;
    foreach ($data['result'] as $row) {
      $html_content = [];

      if ($this->button_bulk_action) {

        $html_content[] = "<input type='checkbox' class='checkbox' name='checkbox[]' value='" . $row->{$tablePK} . "'/>";
      }

      if ($this->show_numbering) {
        $html_content[] = $number . '. ';
        $number++;
      }

      foreach ($columns_table as $col) {
        if ($col['visible'] === false) {
          continue;
        }

        $value = @$row->{$col['field']};
        $title = @$row->{$this->title_field};
        $label = $col['label'];

        if (isset($col['image'])) {
          if ($value == '') {
            $value = "<a  data-lightbox='roadtrip' rel='group_{{$table}}' title='$label: $title' href='" . asset('vendor/crudbooster/noimage.png') . "'><img width='40px' height='40px' src='" . asset('vendor/crudbooster/noimage.png') . "'/></a>";
          } else {
            $pic = (strpos($value, 'http://') !== false) ? $value : asset($value);
            $value = "<a data-lightbox='roadtrip'  rel='group_{{$table}}' title='$label: $title' href='" . $pic . "'><img width='40px' height='40px' src='" . $pic . "'/></a>";
          }
        }

        if (@$col['download']) {
          $url = (strpos($value, 'http://') !== false) ? $value : asset($value) . '?download=1';
          if ($value) {
            $value = "<a class='btn btn-xs btn-primary' href='$url' target='_blank' title='Download File'><i class='fa fa-download'></i> Download</a>";
          } else {
            $value = " - ";
          }
        }

        if ($col['str_limit']) {
          $value = trim(strip_tags($value));
          $value = str_limit($value, $col['str_limit']);
        }

        if ($col['nl2br']) {
          $value = nl2br($value);
        }

        if ($col['callback_php']) {
          foreach ($row as $k => $v) {
            $col['callback_php'] = str_replace("[" . $k . "]", $v, $col['callback_php']);
          }
          @eval("\$value = " . $col['callback_php'] . ";");
        }

        //New method for callback
        if (isset($col['callback'])) {
          $value = call_user_func($col['callback'], $row);
        }

        $datavalue = @unserialize($value);
        if ($datavalue !== false) {
          if ($datavalue) {
            $prevalue = [];
            foreach ($datavalue as $d) {
              if ($d['label']) {
                $prevalue[] = $d['label'];
              }
            }
            if ($prevalue && count($prevalue)) {
              $value = implode(", ", $prevalue);
            }
          }
        }

        $html_content[] = $value;
      } //end foreach columns_table

      if ($this->button_table_action) :

        $button_action_style = $this->button_action_style;
        $html_content[] = "<div class='button_action' style='text-align:right'>" . view($this->view_action, compact('addaction', 'row', 'button_action_style', 'parent_field'))->render() . "</div>";
      //		$html_content[] = "<div class='button_action' style='text-align:right'>" . view('crudbooster::components.action', compact('addaction', 'row', 'button_action_style', 'parent_field'))->render() . "</div>";

      endif; //button_table_action

      foreach ($html_content as $i => $v) {
        $this->hook_row_index($i, $v);
        $html_content[$i] = $v;
      }

      $html_contents[] = $html_content;
    } //end foreach data[result]

    $html_contents = ['html' => $html_contents, 'data' => $data['result']];

    $data['html_contents'] = $html_contents;

    return view($this->view_index, $data, compact('driver'));
    //	return view("crudbooster::default.index", $data);
  }

  public function getDetail($id)
  {
    $this->cbLoader();
    $row = DB::table($this->table)->where($this->primary_key, $id)->first();

    $total_success = $this->countOrderStatus($id, 'Complete');
    $total_fail = $this->countOrderStatus($id, 'Canceled');
    $row->total_success = $total_success;
    $row->total_fail = $total_fail;


    if (!CRUDBooster::isRead() && $this->global_privilege == false || $this->button_detail == false) {
      CRUDBooster::insertLog(trans("crudbooster.log_try_view", [
        'name' => $row->{$this->title_field},
        'module' => CRUDBooster::getCurrentModule()->name,
      ]));
      CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));
    }
    $send_msg_permission = DB::table('cms_privileges_roles')->where([
      ['id_cms_privileges', CRUDBooster::myPrivilegeId()],
      ['id_cms_moduls', 15]
    ])->first();
    $allowSendNotification = $send_msg_permission->is_create ?? false;
    $allowSendResetPassword = $send_msg_permission->is_edit ?? false;
    $module = CRUDBooster::getCurrentModule();
    $page_menu = Route::getCurrentRoute()->getActionName();
    $module_name = ($module->translate_code) ? trans($module->translate_code) : $module->name;
    $page_title = trans("crudbooster.detail_data_page_title", ['module' => $module_name, 'name' => $row->{$this->title_field}]);
    $command = 'detail';

    Session::put('current_row_id', $id);

    $order_master = DB::table('order_master')
      ->where('rider_id', $id)
      ->orderBy('id', 'DESC')
      ->get();


    #region PAGINATION AND SHOW INFO

    // Config pagination
    $limitDefault = 10;
    $pageDefault = 1;
    $limitParam = $_GET['limit'] ?? $limitDefault;
    $pageParam = $_GET['page'] ?? $pageDefault;

    $total = $order_master->count();
    $perPage = $pageParam * $limitParam;
    $from = $total > 0 ? $perPage - $limitParam + 1 : 0;
    $to = $perPage < $total ? $perPage : $total;
    $order_master = $order_master->paginate($limitParam);

    // Setup number of page
    $html_show = [];
    for ($i = 1; $i <= ceil($total / $limitParam); $i++) {
      array_push($html_show, $i);
    }

    #endregion
    $view_detail = $this->view_detail;
    return view($this->view_form, compact(
      'row',
      'page_menu',
      'page_title',
      'command',
      'id',
      'view_detail',
      'order_master',
      'pagination',
      'total',
      'from',
      'to',
      'pageParam',
      'limitParam',
      'html_show',
      'allowSendNotification',
      'allowSendResetPassword'
    ));
  }

  private function countOrderStatus($rider_id, $status)
  {
    return count(DB::table('order_master')
      ->where([
        ['rider_id', $rider_id],
        ['status', $status]
      ])
      ->get());
  }

  public function getSendMessageToDriver()
  {
    //	echo "<pre>"; print_r(Request::all());die();
    $riderid = Request::get('riderid');
    $message = Request::get('message');

    if (!is_numeric($riderid)) {
      CRUDBooster::redirect($_SERVER['HTTP_REFERER'], 'Vui lòng chọn driver !', "warning");
    }

    if (trim($message) == "") {
      CRUDBooster::redirect($_SERVER['HTTP_REFERER'], 'Vui lòng nhập nội dung message !', "warning");
    }

    // sendPushNotification here
    $rows = DB::table('user_master')
      ->where('id', $riderid)
      ->select('id', 'deviceid', 'platform')
      ->get();

    $android = $ios = [];
    foreach ($rows as $user_master) {
      if ($user_master->platform == 'android') {
        $android[] = $user_master->deviceid;
      } else if ($user_master->platform == 'ios') {
        $ios[] = $user_master->deviceid;
      }
    }

    if (!empty($android)) {
      $msg = array('title' => 'Eco Delivery', 'body' => $message);
      $r = $this->sendPushNotification($android, $message, $msg, 'android');
    }
    if (!empty($ios)) {
      $msg = array('title' => 'Eco Delivery', 'body' => $message);
      $r = $this->sendPushNotification($ios, $message, $msg, 'ios');
    }

    $a = [];
    $a['userid'] = CRUDBooster::myId();
    $a['riderid'] = $riderid;
    $a['message'] = $message;
    $a['order_id'] = 0;
    $a['isbided'] = 0;
    $a['status'] = 'admin';
    $a['isview'] = 0;
    $a['createdate'] = date('Y-m-d H:i:s');
    $a['type'] = 'admin';
    DB::table('notification_master')->insert($a);


    CRUDBooster::redirect(CRUDBooster::mainpath(), 'Gửi message thành công', 'success');
  }

  public function getSendPasswordToDriver()
  {
    if (!CRUDBooster::isUpdate() && $this->global_privilege == false || $this->button_edit == false) {
      CRUDBooster::insertLog(trans("crudbooster.log_try_edit", [
        'name' => $row->{$this->title_field},
        'module' => CRUDBooster::getCurrentModule()->name,
      ]));
      CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));
    }

    $url = env('API_URL');
    $riderid = request('riderid');
    $contactNo = DB::table('user_master')->find($riderid)->contactno ?? null;
    if (!$contactNo) return response('Cannot found rider contact , please contact with administrator to solve this issue', 404);
    $headers = array(
      'Content-Type:application/json',
      'Authorization:key=' . env('FCM_API_KEY')
    );
    $requestData = [
      "service" => "reset_password_sms",
      "request" => ["contactno" => $contactNo]
    ];

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    if ($headers)
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));
    $response = curl_exec($ch);
    curl_close($ch);

    $response = json_decode($response);
    CRUDBooster::redirect(CRUDBooster::mainpath(), $response->message, $response->success == 1 ? 'success' : 'danger');
  }

  public function getSendMessageToDrivers()
  {
    $message = Request::get('message');

    if (trim($message) == "") {
      CRUDBooster::redirect($_SERVER['HTTP_REFERER'], 'Vui lòng nhập nội dung message !', "warning");
    }

    // sendPushNotification here
    $rows = DB::table('user_master')
      ->whereNull('deleted_at')
      ->select('id', 'deviceid', 'platform')
      ->get();

    $android = $ios = [];
    foreach ($rows as $user_master) {
      $a = [];
      $a['userid'] = CRUDBooster::myId();
      $a['riderid'] = $user_master->id;
      $a['message'] = $message;
      $a['order_id'] = 0;
      $a['isbided'] = 0;
      $a['status'] = 'admin';
      $a['isview'] = 0;
      $a['createdate'] = date('Y-m-d H:i:s');
      $a['type'] = 'admin';
      DB::table('notification_master')->insert($a);
      if ($user_master->platform == 'android') {
        $android[] = $user_master->deviceid;
      } else if ($user_master->platform == 'ios') {
        $ios[] = $user_master->deviceid;
      }
    }

    if (!empty($android)) {
      $msg = array('title' => 'Eco Delivery', 'body' => $message);
      $r = $this->sendPushNotification($android, $message, $msg, 'android');
    }
    if (!empty($ios)) {
      $msg = array('title' => 'Eco Delivery', 'body' => $message);
      $r = $this->sendPushNotification($ios, $message, $msg, 'ios');
    }

    CRUDBooster::redirect(CRUDBooster::mainpath(), 'Gửi message thành công', 'success');
  }

  public function sendPushNotification($registration_ids, $messages, $msg = array('title' => 'Bungkushit', 'body' => ""), $platform = '', $mtype = "admin")
  {

    $url = 'https://fcm.googleapis.com/fcm/send';
    $serverApiKey = "AAAA-uuJo6U:APA91bHLdAhYa8tcCIXBYs8NcP8ieeSXYX6LLfrHZH4YpzxDTxy89VKroctplwnGTyLqVZY72mJUIxLZBk1XIbciQwhWhcPna-MO8-xed5Z02JZIzFoGfXOtOA_1hb4HvYt33K-A4TMN"; //"Your Api key"
    //$message="Your Notification Worked";
    $headers = array(
      'Content-Type:application/json',
      'Authorization:key=' . env('FCM_API_KEY')
    );

    $message = array("message" => $messages, "mtype" => $mtype);

    $data = array(
      'registration_ids' => $registration_ids
    );
    if ($platform == 'android') {
      $message['data'] = $message;
      $data['data'] = $message;
    }
    if ($platform == 'ios') {
      $data['notification'] = $msg;
      $data['notification']['sound'] = "default";
      $data['data'] = $message;
    }
    //echo "<pre>"; print_r($data);
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);

    if ($headers)
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

    $response = curl_exec($ch);
    //print_r($response); die;
    curl_close($ch);

    $response = json_decode($response);
    if (isset($response->success) && $response->success == 1) {
      return true;
    } else {
      return false;
    }
  }

  public function getContactNo(Request $request)
  {
    //autosearch contactno
    $data = [];
    if (Request::has('q')) {

      $search = $_GET['q'];
      //echo $search;
      $data = DB::table('user_master')->select('id', 'contactno', 'firstname')
        ->whereRaw("contactno LIKE '%$search%' OR firstname LIKE '%$search%'")
        ->where('usertype', '=', 'driver')
        ->get();
    }

    return response()->json($data);
  }

  public function getRiderOnMap()
  {
    $timeRange = Input::get('time_range', '-30 minutes');
    $minTime = date("Y-m-d H:i:s", strtotime($timeRange));
    $riders = DB::table('user_master')
      ->select('id', 'lat', 'lng', 'firstname', 'contactno', 'profilepic')
      ->where('online_status', "online")
      ->where('usertype', "driver")
      ->where('last_location_updated', '>=', $minTime)
      ->get();
    $data['riders'] = $riders;
    $data['page_title'] = trans('rider.rider_on_map');
    $data['timeRange'] = $timeRange;
    return view('driver.rider_on_map', $data);
  }

  public function getFreeRider(Request $request)
  {
    //autosearch contactno
    $data = [];
    if (Request::has('q')) {

      $search = $_GET['q'];
      //echo $search;
      $data = DB::table('user_master')->select('id', 'contactno', 'firstname', DB::raw("(SELECT COUNT(id) FROM order_master WHERE rider_id = user_master.id AND `riderstatus` NOT IN ('Complete','PickupDone')) AS freerider"))
        ->whereRaw("contactno LIKE '%$search%' OR firstname LIKE '%$search%'")
        ->where('usertype', 'driver')
        ->where('online_status', 'online')
        ->where('is_verify', 1)
        ->having("freerider", 0)
        ->get();
    }

    return response()->json($data);
  }
}
