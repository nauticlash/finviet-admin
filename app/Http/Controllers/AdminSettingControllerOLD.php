<?php

namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use CRUDBooster;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;

class AdminSettingController extends MyCBController {

    public function cbInit() {

	# START CONFIGURATION DO NOT REMOVE THIS LINE
	$this->title_field = "id";
	$this->limit = "20";
	$this->orderby = "setting_id,desc";
	$this->global_privilege = false;
	$this->button_table_action = FALSE;
	$this->button_bulk_action = FALSE;
	$this->button_action_style = "button_icon";
	$this->button_add = TRUE;
	$this->button_edit = TRUE;
	$this->button_delete = FALSE;
	$this->button_detail = FALSE;
	$this->button_show = FALSE;
	$this->button_filter = FALSE;
	$this->button_import = false;
	$this->button_export = false;
	$this->table = "setting_master";

	$this->view_edit = 'setting.form_edit';

	# END CONFIGURATION DO NOT REMOVE THIS LINE
	# START COLUMNS DO NOT REMOVE THIS LINE
	$this->col = [];
	$this->col[] = ["label" => "Setting Id", "name" => "setting_id"];
//	$this->col[] = ["label" => "Company1", "name" => "company1"];
//	$this->col[] = ["label" => "Company2", "name" => "company2"];
//	$this->col[] = ["label" => "Terms Link", "name" => "terms_link"];
//	$this->col[] = ["label" => "Referral Price", "name" => "referral_price"];
//	$this->col[] = ["label" => "Referral Price Per Order", "name" => "referral_price_per_order"];
//	$this->col[] = ["label" => "Commission", "name" => "commission"];
//	$this->col[] = ["label" => "Bank Details", "name" => "bank_details"];
	# END COLUMNS DO NOT REMOVE THIS LINE
	# START FORM DO NOT REMOVE THIS LINE
	$this->form = [];
	$this->form[] = ['label' => 'Order Pending Time', 'name' => 'order_pending_time', 'type' => 'number', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Rider Pickup Velocity', 'name' => 'rider_pickup_velocity', 'type' => 'number', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Rider Dropoff Velocity', 'name' => 'rider_dropoff_velocity', 'type' => 'number', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10'];
	$this->form[] = ['label' => 'Alert Email List', 'name' => 'alert_email_list', 'type' => 'textarea', 'validation' => 'required|string|min:5|max:200', 'width' => 'col-sm-10'];


	# END FORM DO NOT REMOVE THIS LINE
	# OLD START FORM
	//$this->form = [];
	//$this->form[] = ["label"=>"Setting Id","name"=>"setting_id","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"setting,id"];
	//$this->form[] = ["label"=>"Company1","name"=>"company1","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Company2","name"=>"company2","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Terms Link","name"=>"terms_link","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
	//$this->form[] = ["label"=>"Referral Price","name"=>"referral_price","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
	//$this->form[] = ["label"=>"Referral Price Per Order","name"=>"referral_price_per_order","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
	//$this->form[] = ["label"=>"Commission","name"=>"commission","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
	//$this->form[] = ["label"=>"Bank Details","name"=>"bank_details","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
	//$this->form[] = ["label"=>"Emailverify","name"=>"emailverify","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
	//$this->form[] = ["label"=>"Vcustomerapp","name"=>"vcustomerapp","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Vriderapp","name"=>"vriderapp","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Createdate","name"=>"createdate","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
	//$this->form[] = ["label"=>"Androheremap App Id","name"=>"android_heremap_app_id","type"=>"select2","required"=>TRUE,"validation"=>"required|min:1|max:255","datatable"=>"android_heremap_app,id"];
	//$this->form[] = ["label"=>"Androheremap App Code","name"=>"android_heremap_app_code","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Ios Heremap App Id","name"=>"ios_heremap_app_id","type"=>"select2","required"=>TRUE,"validation"=>"required|min:1|max:255","datatable"=>"ios_heremap_app,id"];
	//$this->form[] = ["label"=>"Ios Heremap App Code","name"=>"ios_heremap_app_code","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Order Pending Time","name"=>"order_pending_time","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
	//$this->form[] = ["label"=>"Alert Email Notification","name"=>"alert_email_notification","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
	//$this->form[] = ["label"=>"Alert Email List","name"=>"alert_email_list","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
	//$this->form[] = ["label"=>"Rider Pickup Velocity","name"=>"rider_pickup_velocity","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
	//$this->form[] = ["label"=>"Rider Dropoff Velocity","name"=>"rider_dropoff_velocity","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
	//$this->form[] = ["label"=>"Created By","name"=>"created_by","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
	//$this->form[] = ["label"=>"Updated By","name"=>"updated_by","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
	# OLD END FORM

	/*
	  | ----------------------------------------------------------------------
	  | Sub Module
	  | ----------------------------------------------------------------------
	  | @label          = Label of action
	  | @path           = Path of sub module
	  | @foreign_key 	  = foreign key of sub table/module
	  | @button_color   = Bootstrap Class (primary,success,warning,danger)
	  | @button_icon    = Font Awesome Class
	  | @parent_columns = Sparate with comma, e.g : name,created_at
	  |
	 */
	$this->sub_module = array();


	/*
	  | ----------------------------------------------------------------------
	  | Add More Action Button / Menu
	  | ----------------------------------------------------------------------
	  | @label       = Label of action
	  | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	  | @icon        = Font awesome class icon. e.g : fa fa-bars
	  | @color 	   = Default is primary. (primary, warning, succecss, info)
	  | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	  |
	 */
	$this->addaction = array();


	/*
	  | ----------------------------------------------------------------------
	  | Add More Button Selected
	  | ----------------------------------------------------------------------
	  | @label       = Label of action
	  | @icon 	   = Icon from fontawesome
	  | @name 	   = Name of button
	  | Then about the action, you should code at actionButtonSelected method
	  |
	 */
	$this->button_selected = array();


	/*
	  | ----------------------------------------------------------------------
	  | Add alert message to this module at overheader
	  | ----------------------------------------------------------------------
	  | @message = Text of message
	  | @type    = warning,success,danger,info
	  |
	 */
	$this->alert = array();



	/*
	  | ----------------------------------------------------------------------
	  | Add more button to header button
	  | ----------------------------------------------------------------------
	  | @label = Name of button
	  | @url   = URL Target
	  | @icon  = Icon from Awesome.
	  |
	 */
	$this->index_button = array();



	/*
	  | ----------------------------------------------------------------------
	  | Customize Table Row Color
	  | ----------------------------------------------------------------------
	  | @condition = If condition. You may use field alias. E.g : [id] == 1
	  | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
	  |
	 */
	$this->table_row_color = array();


	/*
	  | ----------------------------------------------------------------------
	  | You may use this bellow array to add statistic at dashboard
	  | ----------------------------------------------------------------------
	  | @label, @count, @icon, @color
	  |
	 */
	$this->index_statistic = array();



	/*
	  | ----------------------------------------------------------------------
	  | Add javascript at body
	  | ----------------------------------------------------------------------
	  | javascript code in the variable
	  | $this->script_js = "function() { ... }";
	  |
	 */
	$this->script_js = NULL;


	/*
	  | ----------------------------------------------------------------------
	  | Include HTML Code before index table
	  | ----------------------------------------------------------------------
	  | html code to display it before index table
	  | $this->pre_index_html = "<p>test</p>";
	  |
	 */
	$this->pre_index_html = null;



	/*
	  | ----------------------------------------------------------------------
	  | Include HTML Code after index table
	  | ----------------------------------------------------------------------
	  | html code to display it after index table
	  | $this->post_index_html = "<p>test</p>";
	  |
	 */
	$this->post_index_html = null;



	/*
	  | ----------------------------------------------------------------------
	  | Include Javascript File
	  | ----------------------------------------------------------------------
	  | URL of your javascript each array
	  | $this->load_js[] = asset("myfile.js");
	  |
	 */
	$this->load_js = array();



	/*
	  | ----------------------------------------------------------------------
	  | Add css style at body
	  | ----------------------------------------------------------------------
	  | css code in the variable
	  | $this->style_css = ".style{....}";
	  |
	 */
	$this->style_css = NULL;



	/*
	  | ----------------------------------------------------------------------
	  | Include css File
	  | ----------------------------------------------------------------------
	  | URL of your css each array
	  | $this->load_css[] = asset("myfile.css");
	  |
	 */
	$this->load_css = array();
    }

    public function getIndex() {
	$this->cbLoader();

	$module = CRUDBooster::getCurrentModule();

	if (!CRUDBooster::isView() && $this->global_privilege == false) {
	    CRUDBooster::insertLog(trans('crudbooster.log_try_view', ['module' => $module->name]));
	    CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));
	}

	return Redirect::to('admin/setting/edit/1');
    }

    public function getEditSetting() {
//	echo $id; die();
	$id = 1;

	if ($id != 1) {
	    return Redirect::to('admin/setting/edit/1');
	}

	$this->cbLoader();
	$row = DB::table($this->table)->where($this->primary_key, $id)->first();

	if (!CRUDBooster::isRead() && $this->global_privilege == false || $this->button_edit == false) {
	    CRUDBooster::insertLog(trans("crudbooster.log_try_edit", [
		'name' => $row->{$this->title_field},
		'module' => CRUDBooster::getCurrentModule()->name,
	    ]));
	    CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));
	}

	$page_menu = Route::getCurrentRoute()->getActionName();
	$module = (isset(CRUDBooster::getCurrentModule()->translate_code)) ? trans(CRUDBooster::getCurrentModule()->translate_code) : CRUDBooster::getCurrentModule()->name;
	$page_title = trans("crudbooster.edit_data_page_title", ['module' => $module, 'name' => $row->{$this->title_field}]);
	$command = 'edit';
	Session::put('current_row_id', $id);

	//vinhth add code
	$view_edit = $this->view_edit;
	$updated_at = (isset($row->updated_at)) ? $row->updated_at : '';
	$data = $row;

	return view($this->view_form, compact('id', 'row', 'page_menu', 'page_title', 'command', 'view_edit', 'updated_at', 'data'));



//	return view('crudbooster::default.form', compact('id', 'row', 'page_menu', 'page_title', 'command'));
    }

    public function postEditSave($id) {
	$this->cbLoader();
	$row = DB::table($this->table)->where($this->primary_key, $id)->first();

	if (!CRUDBooster::isUpdate() && $this->global_privilege == false) {
	    CRUDBooster::insertLog(trans("crudbooster.log_try_add", ['name' => $row->{$this->title_field}, 'module' => CRUDBooster::getCurrentModule()->name]));
	    CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));
	}

	$this->validation($id);
	$this->input_assignment($id);

	//echo "<pre>"; print_r(Request::all());die();

	$order_pending_time = Request::get('order_pending_time');
	$rider_pickup_velocity = Request::get('rider_pickup_velocity');
	$rider_dropoff_velocity = Request::get('rider_dropoff_velocity');
	$alert_email_list = Request::get('alert_email_list');

	if (!is_numeric($order_pending_time) || $order_pending_time < 0) {
	    CRUDBooster::redirect($_SERVER['HTTP_REFERER'], 'Thời gian cảnh báo tính theo phút !', "warning");
	}

	if (!is_numeric($rider_pickup_velocity) || $rider_pickup_velocity < 0) {
	    CRUDBooster::redirect($_SERVER['HTTP_REFERER'], 'Vận tốc trung bình của lái xe khi lấy hàng (km/h) !', "warning");
	}

	if (!is_numeric($rider_dropoff_velocity) || $rider_dropoff_velocity < 0) {
	    CRUDBooster::redirect($_SERVER['HTTP_REFERER'], 'Vận tốc trung bình của lái xe khi giao hàng (km/h) !', "warning");
	}

	DB::table('setting_master')->where('id', 1)->update([
	    'order_pending_time' => $order_pending_time,
	    'rider_pickup_velocity' => $rider_pickup_velocity,
	    'rider_dropoff_velocity' => $rider_dropoff_velocity,
	    'alert_email_list' => $alert_email_list,
	    'updated_at' => date('Y-m-d H:i:s'),
	    'updated_by' => CRUDBooster::myId()
	]);

	CRUDBooster::redirect($_SERVER['HTTP_REFERER'], 'Cập nhật cấu hình thành công', 'success');
    }

}
