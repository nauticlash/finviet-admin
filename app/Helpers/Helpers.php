<?php

class CommonData {

    //_LOGS
    public static $LOG_ACTION_NEW = 'New';
    public static $LOG_ACTION_UPDATE = 'Update';
    public static $LOG_ACTION_DELETE = 'Delete';
    //public static $USER_MASTER_STATUS = "0|KHÓA;1|HOẠT ĐỘNG";
    public static $USER_MASTER_STATUS = "0|INACTIVE;1|ACTIVE";
    public static $USER_MASTER_ONLINE_STATUS = "offline|OFFLINE;online|ONLINE";
    public static $USER_MASTER_ISFREE = "no|NO;yes|YES";
    //public static $USER_MASTER_IS_VERIFY = "0|CHƯA XÁC THỰC;1|ĐÃ XÁC THỰC";
    public static $USER_MASTER_IS_VERIFY = "0|NO_AUTHENTICATION;1|AUTHENTICED";

    public static $USER_MASTER_IS_CONTACTNO = "0|NO_AUTHENTICATION;1|AUTHENTICED";
    //public static $ORDER_MASTER_STATUS = "Pending|Mới tạo;In Progress|Đang giao;Complete|Đã giao;Cancel|Đã hủy";
    public static $ORDER_MASTER_STATUS = "allocating|status_allocating;accepted|status_accepted;picking_up|status_picking_up;delivering|status_delivering;delivered|status_delivered;completed|status_completed;canceled|status_canceled";
    public static $MESSAGE_ISVIEW = "0|YES;1|NO"; //nếu isview = 1: đã xem = không có message mới

}

if (!function_exists('find_string_in_array')) {

    function find_string_in_array($arr, $string) {
	return array_filter($arr, function($value) use ($string) {
	    return strpos($value, $string) !== false;
	});
    }

}

if (!function_exists('get_string_in_array')) {

    function get_string_in_array($key, $status) {
	$dataenum = (is_array($key)) ? $key : explode(";", $key);
	$results = find_string_in_array($dataenum, $status . "|");
	if (!empty($results)) {
	    foreach ($results as $value) {
		//$results = array_values($results)[0];
		$val = $lab = '';
		if (strpos($value, '|') !== FALSE) {
		    $draw = explode("|", $value);
		    $val = $draw[0];
		    $lab = $draw[1];
		} else {
		    $val = $lab = $value;
		}
		if ($status == $val) {
//		    return $lab;
		    return trans('crudbooster.' . $lab);
		}
	    }
	    return '';
	}
    }

}

//==============================================================================
if (!function_exists('insert_table_logs')) {

    function insert_table_logs($table_name, $query, $id, $action) {
	$a = array();
	$table_columns = DB::getSchemaBuilder()->getColumnListing($table_name);
	foreach ($table_columns as $sk => $s) {
	    $a[$s] = !isset($query[$s]) ? null : $query[$s];
	}
	unset($a['id']);
	$a['id_' . $table_name] = $id;
	$a['action'] = $action;
	DB::table($table_name . '_logs')->insert($a);
    }

}

if (!function_exists('get_format_money_usd')) {

    function get_format_money_usd($number) {
	if (!empty($number) && $number != '' && $number != NULL && is_numeric($number)) {

	    $money = number_format($number, 0, '.', ',');
	    return $money . ' đ';
	}
	return 0 . ' đ';
    }

}

if (!function_exists('gext_text_is_the_product')) {

    function gext_text_is_the_product($statusNumber) {
	if ($statusNumber == 1) {
	    return 'Phải';
	}
	return '';
    }

}

if (!function_exists('create_array_key_value_from_string_common_data')) {

    function create_array_key_value_from_string_common_data($string) {
	$a = explode(";", $string);
	$array = [];
	foreach ($a as $b) {
	    $c = explode('|', $b);
	    $array[$c[0]] = $c[1];
	}
	return $array;
    }

}

if (!function_exists('create_array_key_value_from_string_common_data_after')) {

    function create_array_key_value_from_string_common_data_after($string) {
	$a = explode(";", $string);
	$array = [];
	foreach ($a as $b) {
	    $array[$b] = $b;
	}
	//echo "<pre>"; print_r($array);die();

	return $array;
    }

}

if (!function_exists('get_format_date')) {

    function get_format_date($datetime) {
	if ($datetime != '' && $datetime != NULL) {
	    return date("Y-m-d", strtotime($datetime));
	}
    }

}


//format data show trong td table ở index, xài trong những view my_table.blade.php
if (!function_exists('get_format_html_for_data_in_table')) {

    function get_format_html_for_data_in_table($text) {
	if (check_is_format_float_right($text) == 1) {
	    echo "<td class='float-right' style='text-align:right;'>" . $text . "</td>";
	} else {
	    if ($text == 'MOI') {
		echo "<td title='" . $text . "' style='text-align:center'>"
		. "<i style='font-size:30px; color:#00a65a;' class='fa fa-plus fa-lg' aria-hidden='true'></i>"
		. "</td>";
	    } else if ($text == 'DA_TU_CHOI') {
		echo "<td  title='" . $text . "' style='text-align:center'>"
		. "<i style='font-size:30px; color:#f39c12;' class='fa fa-ban fa-lg' aria-hidden='true'></i>"
		. "</td>";
	    } else if ($text == 'DA_CHAP_NHAN') {
		echo "<td  title='" . $text . "' style='text-align:center'>"
		. "<i style='font-size:30px; color:red;' class='fa fa-check fa-lg' aria-hidden='true'></i>"
		. "</td>";
	    } else if ($text == 'DA_XAC_NHAN') {
		echo "<td  title='" . $text . "' style='text-align:center'>"
		. "<i style='font-size:30px; color:#337ab7;' class='fa fa-thumbs-up fa-lg' aria-hidden='true'></i>"
		. "</td>";
	    } else if ($text == 'DA_GIAO_HANG') {
		echo "<td  title='" . $text . "' style='text-align:center'>"
		. "<i style='font-size:30px; color:orange;' class='fa fa-truck fa-lg' aria-hidden='true'></i>"
		. "</td>";
	    } else {
		echo "<td 5>" . $text . "</td>";
	    }
	}
    }

}

if (!function_exists('check_is_format_float_right')) {

    function check_is_format_float_right($text) {
	if (DateTime::createFromFormat('Y-m-d', $text) !== FALSE) {
	    return 1;
	}
	if (DateTime::createFromFormat('Y-m-d H:m:s', $text) !== FALSE) {
	    return 1;
	}
//    if(is_numeric($text) == true){
//	return 1;
//    }
	if (is_numeric(str_replace(".", "", $text)) == true) {
	    return 1;
	}

	if (is_numeric(str_replace(",", "", $text)) == true) {
	    return 1;
	}
	return 0;
    }

}

if (!function_exists('get_month_diff')) {

    function get_month_diff($date1, $date2) {
	$ts1 = strtotime($date1);
	$ts2 = strtotime($date2);

	$year1 = date('Y', $ts1);
	$year2 = date('Y', $ts2);

	$month1 = date('m', $ts1);
	$month2 = date('m', $ts2);

	$diff = (($year2 - $year1) * 12) + ($month2 - $month1);

	return $diff + 1;
    }

}

if (!function_exists('get_list_all_months_between_two_dates')) {

    function get_list_all_months_between_two_dates($date1, $date2) {
	$start = (new DateTime($date1))->modify('first day of this month');
	$end = (new DateTime($date2))->modify('first day of next month');
	$interval = DateInterval::createFromDateString('1 month');
	$period = new DatePeriod($start, $interval, $end);

//	foreach ($period as $dt) {
//	    echo $dt->format("Y-m") . "<br>\n";
//	}
	return $period;
    }

}

if (!function_exists('cmp')) {

    function cmp($a, $b) {
	return strcmp($a->report_ym, $b->report_ym);
    }

}
//==============================================================================

if (!function_exists('get_format_online_offline')) {

    function get_format_online_offline($online_status) {
	if ($online_status == 'online') {
	    $text = '<font color="green">ONLINE</font>';
	}
	if ($online_status == 'offline') {
	    $text = '<font color="red">OFFLINE</font>';
	}

	return $text;
    }

}

if (!function_exists('get_format_col_is_verify')) {

    function get_format_col_is_verify($userid, $is_verify) {
	if ($is_verify == 1) {
	    $text = '<a class="btn btn-xs btn-success" title="' . trans('crudbooster.AUTHENTICED') . '" onclick="" href="' . Config::get('constants.BASE_URL_ADMIN') . '/driver/set-verify/' . $userid . '/' . 0 . '" target="_self"><i class="fa fa-check"></i> ' . trans('crudbooster.AUTHENTICED') . '</a>';
	}

	if ($is_verify == 0) {
	    $text = '<a class="btn btn-xs btn-danger" title="' . trans('crudbooster.NO_AUTHENTICATION') . '" onclick="" href="' . Config::get('constants.BASE_URL_ADMIN') . '/driver/set-verify/' . $userid . '/' . 1 . '" target="_self"><i class="fa fa-ban"></i> ' . trans('crudbooster.NO_AUTHENTICATION') . '</a>';
	}

	return $text;
    }

}


if (!function_exists('get_format_col_timeline')) {

    function get_format_col_timeline($orderid) {
	$text = '<a class="btn btn-xs" style="background-color: #b30047" href="' . Config::get('constants.BASE_URL_ADMIN') . '/order/order-timeline/' . $orderid . ' "title="'.trans('crudbooster.timeline'). '" >
		<i class="fa fa-history" style="color: white"></i> </a>';

	return $text;
    }

}

if (!function_exists('get_assign_rider')) {

    function get_assign_rider($status, $orderid) {
	if ($status != 'Pending' && $status != 'Canceled') {
	    $text = '';
	} else {
	    $text = '<a class="btn btn-xs" href="' . Config::get('constants.BASE_URL_ADMIN') . '/order/detail/' . $orderid . '" title="' . trans('crudbooster.assign_driver_for_order') . '">
                        <img src="' . Config::get('constants.BASE_URL') . '/assets/images/scooter.png" alt="" style="max-width: 24px;max-height: 24px">
                    </a>';
	}

	return $text;
    }

}

if (!function_exists('get_assign_rider_v2')) {

    function get_assign_rider_v2($status, $orderid, $rider_id) {
	if ($status != 'Pending' && $status != 'Canceled') {
	    $text = '';
	} else {
	    $text = '<button style="margin-bottom:5px;" type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#exampleModal' . $orderid . '">
		Assign  Rider
	    </button>';
	}

	return $text;
    }

}

if (!function_exists('order_total_sum_total_amount')) {

    function order_total_sum_total_amount($order_total, $total_amount) {
	$order_total_number = ($order_total > 0) ? $order_total : 0;
	$total_amount_number = ($total_amount > 0) ? $total_amount : 0;

	$total = $order_total_number + $total_amount_number;
	return get_format_money_usd($total);
    }

}


if (!function_exists('post_display')) {

    function post_display($str = '', $data = '') {
	if (isset($_POST[$str])) {
	    return htmlspecialchars($_POST[$str]);
	} else {
	    return htmlspecialchars($data);
	}
    }

}

if (!function_exists('avatar')) {

    function avatar($pic_name) {
        if (!empty($pic_name)) {
            $pic = '<img class="avatar" src="'.Config::get('constant.BASE_URL_ADMIN').'/images/profile/'.$pic_name.'"/>';
        } else {
            $pic = '<img class="avatar" src="'.Config::get('constant.BASE_URL_ADMIN').'/vendor/crudbooster/noimage.png"/>';
        }
	    return $pic;
    }
}

if (!function_exists('get_order_status')) {

    function get_order_status($order) {
        switch ($order->status) {
            case 'Pending':
                return 'allocating';
                break;
            case 'In Progress':
                switch ($order->is_pickup) {
                    case 0:
                        return 'accepted';
                        break;
                    case 1:
                        return 'picking_up';
                        break;
                    case 3:
                        switch ($order->is_dropoff) {
                            case 0:
                                return 'delivering';
                                break;
                            case 2:
                                if (!empty($order->is_return)) {
                                    return 'returning';
                                } else {
                                    return 'delivered';
                                }
                                break;
                        }
                        break;
                }
                break;
            case 'Complete':
                return 'completed';
                break;
            case 'Canceled':
                return 'canceled';
                break;
        }
    }
}

if (!function_exists('get_order_id')) {

    function get_order_id($orderid) {
    $text = '<a style="color: #2970a6; font-weight: bold;" href="' . Config::get('constants.BASE_URL_ADMIN') . '/order/detail/' . $orderid . '" >'.$orderid.'</a>';

	return $text;
    }
}

if (!function_exists('get_driver_id')) {

    function get_driver_id($driverid) {
    $text = '<a style="color: #2970a6; font-weight: bold;" href="' . Config::get('constants.BASE_URL_ADMIN') . '/driver/detail/' . $driverid . '" >'.$driverid.'</a>';

	return $text;
    }
}

if (!function_exists('hidden_id')) {

    function hidden_id($id) {
    $text = '<p hidden>'.$id.'</p>';

	return $text;
    }
}

if (!function_exists('get_formatted_date')) {

    function get_formatted_date($date) {
        return date('H:i, d/m/Y', strtotime($date));
    }
}

if (!function_exists('get_order_alert')) {

    function get_order_alert($order) {
        date_default_timezone_set('Asia/Bangkok');
        $now = time();
        switch ($order->status) {
            case 'Pending':
                $secDiff = $now - strtotime($order->orderdate);
                if ($secDiff >= 60 * 3) {
                    return 'danger';
                } else if ($secDiff >= 60 * 2) {
                    return 'warning';
                } else if ($secDiff >= 60 * 1) {
                    return 'info';
                }
                break;
            case 'In Progress':
                switch ($order->is_pickup) {
                    case 0:
                        if (!empty($order->rider_accept_time)) {
                            $secDiff = $now - strtotime($order->rider_accept_time);
                            if ($secDiff >= 60 * 15) {
                                return 'danger';
                            } else if ($secDiff >= 60 * 10) {
                                return 'warning';
                            } else if ($secDiff >= 60 * 5) {
                                return 'info';
                            }
                        }
                        break;
                    case 1:
                        if (!empty($order->rider_accept_time)) {
                            $secDiff = $now - strtotime($order->rider_accept_time);
                            if ($secDiff >= 60 * 20) {
                                return 'danger';
                            } else if ($secDiff >= 60 * 15) {
                                return 'warning';
                            } else if ($secDiff >= 60 * 10) {
                                return 'info';
                            }
                        }
                        break;
                    case 3:
                        switch ($order->is_dropoff) {
                            case 0:
                                if (!empty($order->pickup_time)) {
                                    $secDiff = $now - strtotime($order->pickup_time);
                                    if ($secDiff >= 60 * 20) {
                                        return 'danger';
                                    } else if ($secDiff >= 60 * 15) {
                                        return 'warning';
                                    } else if ($secDiff >= 60 * 10) {
                                        return 'info';
                                    }
                                }
                                break;
                            case 2:
                                break;
                        }
                        break;
                }
                break;
            case 'Complete':
                break;
            case 'Canceled':
                break;
            default:
                return '';
                break;
        }
    }
}

if (!function_exists('get_last_formatted_date')) {
    function get_last_formatted_date($date) {
        $text = '';
        if ($date) {
            date_default_timezone_set('Asia/Bangkok');
            $now = time();
            $secDiff = $now - strtotime($date);
            if ($secDiff > 86400) {
                $dayDiff = floor($secDiff / 86400);
                $text = trans('general.more_than_days', ["value" => $dayDiff]);
            } else if ($secDiff > 3600) {
                $hourDiff = floor($secDiff / 3600);
                $text = trans('general.more_than_hours', ["value" => $hourDiff]);
            } else if ($secDiff > 60) {
                $minuteDiff = floor($secDiff / 60);
                $text = trans('general.more_than_minutes', ["value" => $minuteDiff]);
            } else if ($secDiff > 0) {
                $text = trans('general.more_than_seconds', ["value" => $secDiff]);
            }
        }
        return $text;
    }
}

if (!function_exists('get_html_rider_info')) {
    function get_html_rider_info($rider) {
        $adminUrl = Config::get('constants.BASE_URL_ADMIN');
        $text = '<a href="' . $adminUrl . '/driver/detail/' . $rider->id . '" class="rider_info">';
        $defaultPic = Config::get('constant.BASE_URL') . '/vendor/crudbooster/noimage.png';
        $avatar = Config::get('constant.BASE_URL') . '/images/profile/' . $rider->profilepic;
        $text .= '<object class="avatar" data="' . $avatar . '" type="image/png"><img class="avatar" src="' . $defaultPic . '" /></object>';
        $text .= '<div><span>' . trans('general.fullname') . $rider->firstname .
            '</span><span>' . trans('general.phone') . $rider->contactno . '</span></div></a>';
        return $text;
    }
}
